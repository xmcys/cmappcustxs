//
//  CommonCode.m
//  CommonCode
//
//  Created by Pop Web on 15/1/8.
//  Copyright (c) 2015年 黄旺鑫. All rights reserved.
//

#import "CommonCode.h"
#import <CommonCrypto/CommonDigest.h>
#import <sys/utsname.h>

@implementation CommonCode

/**
 *  获取当前系统版本
 *  如果直接调用读取系统的方法， 可能出现一些问题
 *
 */
+ (NSString *)getCurrentSystemVersion {
    
    //静态全局变量  用来存系统版本，以便反复读取
    static NSString *currentVersion = nil;
    
    if (!currentVersion) {
        currentVersion = [User_Defaults objectForKey:@"currentSystemVersion"];
    }
    
    //当NSUserDefaults中没有值时，获取系统的版本并存进去
    if (!currentVersion) {
        currentVersion = [[UIDevice currentDevice] systemVersion];
        
        [User_Defaults setObject:currentVersion forKey:@"currentSystemVersion"];
        [User_Defaults synchronize];
    }
    
    return currentVersion;
}

/**
 *  区分IOS6以上的版本
 *
 *  @return 大于IOS6返回真
 */
+ (BOOL)greaterThanIOS6 {
    
    NSString *reqSysVer = @"7";
    NSString *currSysVer = [self getCurrentSystemVersion];
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending) {
        return YES;
    }
    
    return NO;
}

/**
 *  @brief 区分IOSx以上的版本
 *
 *  @return 大于IOSx返回真
 */
+ (BOOL)greaterThanIOSX:(NSString *)x {

    NSString *currSysVer = [self getCurrentSystemVersion];
    if ([currSysVer compare:x options:NSNumericSearch] != NSOrderedAscending) {
        return YES;
    }
    
    return NO;
}

/**
 *  判断是IOS8
 *
 */
+ (BOOL)isIOS8 {
    
    NSString *reqSysVer = @"8";
    NSString *currSysVer = [self getCurrentSystemVersion];
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending) {
        return YES;
    }
    
    return NO;
}

/**
 *  获取设备屏幕的大小
 *
 */
+ (CGRect)getDeviceBounds {
    
    static CGRect deviceBounds;
    if (CGRectGetWidth(deviceBounds) == 0) {
        //获取设备的屏幕大小
        deviceBounds = [[UIScreen mainScreen] bounds];
    }
    
    return deviceBounds;
}

/**
 *  @brief 计算文本所需的高度或宽度
 *
 *  @param string  文本内容
 *  @param font    显示的字体
 *  @param maxSize 最大宽度或者最大高度， 需要准确值  比如计算高度，CGSizeMake(280, 9999), 宽度准确值， 高度要比较大
 *
 */
+ (CGSize)calculateSizeWithString:(NSString *)string andFont:(UIFont *)font andMaxSize:(CGSize)maxSize {
    
    if (!string) {
        //当不存在string 返回0
        return CGSizeZero;
    }else if (!font){
        //字体为nil时，使用系统默认的字体
        font = [UIFont systemFontOfSize:17];
    }
    
    if (Greater_Than_IOS6) {
        
        //设置字体属性
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:string attributes:@{NSFontAttributeName: font}];
        NSRange range = NSMakeRange(0, attrString.length);
        NSDictionary *attributesDictionary = [attrString attributesAtIndex:0 effectiveRange:&range];
        
        //计算大小 此方法在ios7以上才能使用
        CGSize contentSize = [string boundingRectWithSize:maxSize //宽高限制
                                                  options:NSStringDrawingUsesLineFragmentOrigin //文本绘制时的附加选项
                                               attributes:attributesDictionary //文字的属性
                                                  context:nil].size; // context上下文。包括一些信息，例如如何调整字间距以及缩放。该对象包含的信息将用于文本绘制。该参数可为ni
        
        contentSize.width = ceil(contentSize.width);
        contentSize.height = ceil(contentSize.height);
        
        return contentSize;
        
    }

#pragma clang diagnostic push
    
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
    //ios6上运行
    CGSize contentSize = [string sizeWithFont:font constrainedToSize:maxSize lineBreakMode:NSLineBreakByCharWrapping];
#pragma clang diagnostic pop
    contentSize.width = ceil(contentSize.width);
    contentSize.height = ceil(contentSize.height);
    
    return contentSize;

}

/**
 *  @brief 获取设备型号
 *
 *  @return 返回如 iPhone 5s (A1453/A1533)
 */
+ (NSString *)deviceModelString {
    
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *deviceModelString = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    if ([deviceModelString isEqualToString:@"iPhone1,1"])   return @"iPhone 2G (A1203)";
    if ([deviceModelString isEqualToString:@"iPhone1,2"])   return @"iPhone 3G (A1241/A1324)";
    if ([deviceModelString isEqualToString:@"iPhone2,1"])   return @"iPhone 3GS (A1303/A1325)";
    if ([deviceModelString isEqualToString:@"iPhone3,1"])   return @"iPhone 4 (A1332)";
    if ([deviceModelString isEqualToString:@"iPhone3,2"])   return @"iPhone 4 (A1332)";
    if ([deviceModelString isEqualToString:@"iPhone3,3"])   return @"iPhone 4 (A1349)";
    if ([deviceModelString isEqualToString:@"iPhone4,1"])   return @"iPhone 4S (A1387/A1431)";
    if ([deviceModelString isEqualToString:@"iPhone5,1"])   return @"iPhone 5 (A1428)";
    if ([deviceModelString isEqualToString:@"iPhone5,2"])   return @"iPhone 5 (A1429/A1442)";
    if ([deviceModelString isEqualToString:@"iPhone5,3"])   return @"iPhone 5c (A1456/A1532)";
    if ([deviceModelString isEqualToString:@"iPhone5,4"])   return @"iPhone 5c (A1507/A1516/A1526/A1529)";
    if ([deviceModelString isEqualToString:@"iPhone6,1"])   return @"iPhone 5s (A1453/A1533)";
    if ([deviceModelString isEqualToString:@"iPhone6,2"])   return @"iPhone 5s (A1457/A1518/A1528/A1530)";
    if ([deviceModelString isEqualToString:@"iPhone7,1"])   return @"iPhone 6 Plus (A1522/A1524)";
    if ([deviceModelString isEqualToString:@"iPhone7,2"])   return @"iPhone 6 (A1549/A1586)";
    if ([deviceModelString isEqualToString:@"iPhone8,1"])   return @"iPhone 6s (A1700/A1688)";
    if ([deviceModelString isEqualToString:@"iPhone8,2"])   return @"iPhone 6s Plus (A1699/A1687)";
    if ([deviceModelString isEqualToString:@"iPhone9,1"])   return @"iPhone 7 ";
    if ([deviceModelString isEqualToString:@"iPhone9,2"])   return @"iPhone 7 Plus ";
    
    if ([deviceModelString isEqualToString:@"iPod1,1"])     return @"iPod Touch 1G (A1213)";
    if ([deviceModelString isEqualToString:@"iPod2,1"])     return @"iPod Touch 2G (A1288)";
    if ([deviceModelString isEqualToString:@"iPod3,1"])     return @"iPod Touch 3G (A1318)";
    if ([deviceModelString isEqualToString:@"iPod4,1"])     return @"iPod Touch 4G (A1367)";
    if ([deviceModelString isEqualToString:@"iPod5,1"])     return @"iPod Touch 5G (A1421/A1509)";
    
    if ([deviceModelString isEqualToString:@"iPad1,1"])     return @"iPad 1G (A1219/A1337)";
    if ([deviceModelString isEqualToString:@"iPad2,1"])     return @"iPad 2 (A1395)";
    if ([deviceModelString isEqualToString:@"iPad2,2"])     return @"iPad 2 (A1396)";
    if ([deviceModelString isEqualToString:@"iPad2,3"])     return @"iPad 2 (A1397)";
    if ([deviceModelString isEqualToString:@"iPad2,4"])     return @"iPad 2 (A1395+New Chip)";
    if ([deviceModelString isEqualToString:@"iPad2,5"])     return @"iPad Mini 1G (A1432)";
    if ([deviceModelString isEqualToString:@"iPad2,6"])     return @"iPad Mini 1G (A1454)";
    if ([deviceModelString isEqualToString:@"iPad2,7"])     return @"iPad Mini 1G (A1455)";
    
    if ([deviceModelString isEqualToString:@"iPad3,1"])     return @"iPad 3 (A1416)";
    if ([deviceModelString isEqualToString:@"iPad3,2"])     return @"iPad 3 (A1403)";
    if ([deviceModelString isEqualToString:@"iPad3,3"])     return @"iPad 3 (A1430)";
    if ([deviceModelString isEqualToString:@"iPad3,4"])     return @"iPad 4 (A1458)";
    if ([deviceModelString isEqualToString:@"iPad3,5"])     return @"iPad 4 (A1459)";
    if ([deviceModelString isEqualToString:@"iPad3,6"])     return @"iPad 4 (A1460)";
    
    if ([deviceModelString isEqualToString:@"iPad4,1"])     return @"iPad Air (A1474)";
    if ([deviceModelString isEqualToString:@"iPad4,2"])     return @"iPad Air (A1475)";
    if ([deviceModelString isEqualToString:@"iPad4,3"])     return @"iPad Air (A1476)";
    if ([deviceModelString isEqualToString:@"iPad4,4"])     return @"iPad Mini 2G (A1489)";
    if ([deviceModelString isEqualToString:@"iPad4,5"])     return @"iPad Mini 2G (A1490)";
    if ([deviceModelString isEqualToString:@"iPad4,6"])     return @"iPad Mini 2G (A1491)";
    
    if ([deviceModelString isEqualToString:@"i386"])         return @"Simulator";
    if ([deviceModelString isEqualToString:@"x86_64"])       return @"Simulator";
    
    
    NSLog(@"NOTE: Unknown device type: %@", deviceModelString);
    return deviceModelString;
    
    
}

/**
 *  Hex生成Color对象 支持Hex转换成UIColo对象
 *
 *  @param stringToConvert 传入如26BFFC或者#FFB25C
 *
 *  @return Color对象
 */
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert {
    
    NSString *cString = [[stringToConvert stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    if ([cString length] < 6)
        return [UIColor whiteColor];
    if ([cString hasPrefix:@"#"])
        cString = [cString substringFromIndex:1];
    if ([cString length] != 6)
        return [UIColor whiteColor];
    
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

/**
 *  绘制纯色图片
 *
 *  @param color 颜色对象
 *  @param size  图片大小
 *
 *  @return 纯色图片
 */
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size {
    
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

/**
 *  MD5加密
 *
 *  @param string 要加密的字符串
 *  @param len    加密位数
 *
 *  如需大写的只需修改02X
 */
+ (NSString *)md5WithStr:(NSString *)string andDigestLength:(NSInteger)len {
    
    const char *cStr = [string UTF8String];
    unsigned char result[len];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
    
    NSMutableString *md5Str = [NSMutableString string];
    
    for (int i = 0; i < len; i++)
    {
        [md5Str appendFormat:@"%02x", result[i]];
    }
    
    return md5Str;
}

/**
 *  清理tmp目录下的缓存
 */
+ (void)clearTempCache {
    //获取tmp目录
    NSString *tempPath = NSTemporaryDirectory();
    
    NSFileManager *_manager = [NSFileManager defaultManager];
    
    //文件列表
    NSArray *_cacheFileList;
    //枚举对象
    NSEnumerator *_cacheEnumerator;
    //单条的缓存地址
    NSString *_cacheFilePath;
    
    //获取列表
    _cacheFileList = [_manager subpathsAtPath:tempPath];
    //枚举
    _cacheEnumerator = [_cacheFileList objectEnumerator];
    //判断缓存时间是否超过两天 超过则删除掉
    while (_cacheFilePath = [_cacheEnumerator nextObject]){
        NSDictionary *attrs = [_manager attributesOfItemAtPath:[tempPath stringByAppendingPathComponent:_cacheFilePath] error:nil];
        if (fabs([[attrs fileCreationDate] timeIntervalSinceNow]) > 172800) {
            [_manager removeItemAtPath:[tempPath stringByAppendingPathComponent:_cacheFilePath] error:nil];
        }
        
    }
}

/**
 *  获取tmp目录的大小
 */
+ (NSUInteger)getTmpCacheSize {
    
    NSUInteger size = 0;
    //获取tmp目录
    NSString *tempPath = NSTemporaryDirectory();
    
    NSFileManager *_manager = [NSFileManager defaultManager];
    
    //文件列表
    NSArray *_cacheFileList;
    //枚举对象
    NSEnumerator *_cacheEnumerator;
    //单条的缓存地址
    NSString *_cacheFilePath;
    
    //获取列表
    _cacheFileList = [_manager subpathsAtPath:tempPath];
    //枚举
    _cacheEnumerator = [_cacheFileList objectEnumerator];
    //判断缓存时间是否超过两天 超过则删除掉
    while (_cacheFilePath = [_cacheEnumerator nextObject]){
        NSDictionary *attrs = [_manager attributesOfItemAtPath:[tempPath stringByAppendingPathComponent:_cacheFilePath] error:nil];
        size += [attrs fileSize];
    }
    
    return size;
}

+ (void)clearTempAllFile {
    
    //获取tmp目录
    NSString *tempPath = NSTemporaryDirectory();
    
    NSFileManager *_manager = [NSFileManager defaultManager];
    
    //文件列表
    NSArray *_cacheFileList;
    //枚举对象
    NSEnumerator *_cacheEnumerator;
    //单条的缓存地址
    NSString *_cacheFilePath;
    
    //获取列表
    _cacheFileList = [_manager subpathsAtPath:tempPath];
    //枚举
    _cacheEnumerator = [_cacheFileList objectEnumerator];
    
    while (_cacheFilePath = [_cacheEnumerator nextObject]) {
        [_manager removeItemAtPath:[tempPath stringByAppendingPathComponent:_cacheFilePath] error:nil];
        
    }
}


/**
 *  字数统计
 *
 *  @param string 要统计的字符串
 *
 *  @return 返回字数
 */
+ (int)countWord:(NSString *)string {
    int i,n=(int)[string length],l=0,a=0,b=0;
    unichar c;
    for(i=0;i<n;i++){
        c=[string characterAtIndex:i];
        if(isblank(c)){
            b++;
        }else if(isascii(c)){
            a++;
        }else{
            l++;
        }
    }
    if(a==0 && l==0) return 0;
    return l+(int)ceilf((float)(a+b)/2.0);
}

/**
 *  去掉一些字符，防止json解析不正确
 *
 *  @param jsonString json字符串
 *
 */
+ (NSString *)removeIllegalCharactersWithString:(NSString *)jsonString {
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\s" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\v" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\f" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\b" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\a" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\e" withString:@""];
    return jsonString;
}

// 获取日期显示
+ (NSString *)getTimeShowText:(NSString *)timeStr {
    // 昨天、前天date
    NSTimeInterval secondsPerDay = 24 * 60 * 60;
    NSDate *today = [[NSDate alloc] init];
    NSDate *yesterday, *yesterday1, *yesterday2;
    yesterday = [today dateByAddingTimeInterval: -secondsPerDay];
    yesterday1 = [today dateByAddingTimeInterval: -2*secondsPerDay];
    yesterday2 = [today dateByAddingTimeInterval: -3*secondsPerDay];
    
    // 前10个字符可以判断天
    NSString *todayString = [[today description] substringToIndex:10];
    NSString *yesterdayString = [[yesterday description] substringToIndex:10];
    NSString *yesterday1String = [[yesterday1 description] substringToIndex:10];
    NSString *yesterday2String = [[yesterday2 description] substringToIndex:10];
    NSString *comparedDay = [timeStr substringToIndex:10];
    
    // 订单date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *compareDate = [dateFormatter dateFromString:timeStr];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    if ([todayString isEqualToString:comparedDay]) {
        // 今天，具体时分
        [format setDateFormat:@"HH:mm"];
        return [format stringFromDate:compareDate];
    } else if ([yesterdayString isEqualToString:comparedDay]) {
        return @"一天前";
    } else if ([yesterday1String isEqualToString:comparedDay]) {
        return @"两天前";
    } else if ([yesterday2String isEqualToString:comparedDay]) {
        return @"三天前";
    } {
        // 具体日期
        [format setDateFormat:@"MM-dd"];
        return [format stringFromDate:compareDate];
    }
}

// 是否手机号
+ (BOOL)isPhoneNumber:(NSString *)tel {
    NSString *phoneRegex = @"^1\\d{10}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    if (![phoneTest evaluateWithObject:tel]) {
        return NO;
    }
    return YES;
}

// 是否由字母和数字混合组成
+ (BOOL)isMakeByCharAndNumber:(NSString *)str {
    // 字母或数字组成
    if (![CommonCode isMakeByCharOrNumber:str]) {
        return NO;
    }
    // 必须包含数字，纯字母不行
    if ([CommonCode isMakeByChar:str]) {
        return NO;
    }
    // 必须包含字母，纯数字不行
    if ([CommonCode isMakeByNumber:str]) {
        return NO;
    }
    return YES;
}

// 是否由字母组成
+ (BOOL)isMakeByChar:(NSString *)str {
    NSString *regex = @"^[a-zA-Z]*$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [predicate evaluateWithObject:str];
}

// 是否由数字组成
+ (BOOL)isMakeByNumber:(NSString *)str {
    NSString *regex = @"^[0-9]*$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [predicate evaluateWithObject:str];
}

// 是否是中文
+ (BOOL)isMakeByChinese:(NSString *)chinese {
    if (chinese.length <= 0) {
        return NO;
    }
    NSString *regex = @"^[\u4E00-\u9FA5]*$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [predicate evaluateWithObject:chinese];
}

// 是否是身份证号格式
+ (BOOL)isMakeByIdNumber:(NSString *)identityCard {
    BOOL flag;
    if (identityCard.length <= 0)
    {
        flag = NO;
        return flag;
    }
    
    NSString *regex2 = @"^(^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}$)|(^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])((\\d{4})|\\d{3}[Xx])$)$";
    NSPredicate *identityCardPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex2];
    flag = [identityCardPredicate evaluateWithObject:identityCard];
    
    
    //如果通过该验证，说明身份证格式正确，但准确性还需计算
    if(flag)
    {
        if(identityCard.length==18)
        {
            //将前17位加权因子保存在数组里
            NSArray * idCardWiArray = @[@"7", @"9", @"10", @"5", @"8", @"4", @"2", @"1", @"6", @"3", @"7", @"9", @"10", @"5", @"8", @"4", @"2"];
            
            //这是除以11后，可能产生的11位余数、验证码，也保存成数组
            NSArray * idCardYArray = @[@"1", @"0", @"10", @"9", @"8", @"7", @"6", @"5", @"4", @"3", @"2"];
            
            //用来保存前17位各自乖以加权因子后的总和
            
            NSInteger idCardWiSum = 0;
            for(int i = 0;i < 17;i++)
            {
                NSInteger subStrIndex = [[identityCard substringWithRange:NSMakeRange(i, 1)] integerValue];
                NSInteger idCardWiIndex = [[idCardWiArray objectAtIndex:i] integerValue];
                
                idCardWiSum+= subStrIndex * idCardWiIndex;
                
            }
            
            //计算出校验码所在数组的位置
            NSInteger idCardMod=idCardWiSum%11;
            
            //得到最后一位身份证号码
            NSString * idCardLast= [identityCard substringWithRange:NSMakeRange(17, 1)];
            
            //如果等于2，则说明校验码是10，身份证号码最后一位应该是X
            if(idCardMod==2)
            {
                if([idCardLast isEqualToString:@"X"]||[idCardLast isEqualToString:@"x"])
                {
                    return flag;
                }else
                {
                    flag =  NO;
                    return flag;
                }
            }else
            {
                //用计算出的验证码与最后一位身份证号码匹配，如果一致，说明通过，否则是无效的身份证号码
                if([idCardLast isEqualToString: [idCardYArray objectAtIndex:idCardMod]])
                {
                    return flag;
                }
                else
                {
                    flag =  NO;
                    return flag;
                }
            }
        }
        else
        {
            flag =  NO;
            return flag;
        }
    }
    else
    {
        return flag;
    }
}
// 是否由字母或数字组成
+ (BOOL)isMakeByCharOrNumber:(NSString *)str {
    NSString *regex = @"^[a-zA-Z0-9]*$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [predicate evaluateWithObject:str];
}

// 数据列表转成索引选择器所需数组
+ (NSMutableArray *)sortByFirstCharact:(NSArray *)dataArray {
    // 首字母－[数据、数据]
    NSMutableDictionary *keyDict = [NSMutableDictionary dictionary];
    // 遍历数据
    for (NSDictionary *dataDict in dataArray) {
        // 获取首字母
        NSString *key = dataDict[@"description"];
        key = [key uppercaseString];
        
        // 无此首字母则创建
        if (![[keyDict allKeys] containsObject:key]) {
            [keyDict setValue:[NSMutableArray array] forKey:key];
        }
        // 将数据添加到其首字母对应的数组
        NSMutableArray *keyInfoArray = [keyDict objectForKey:key];
        [keyInfoArray addObject:dataDict];
    }
    
    NSMutableArray *resultArray = [NSMutableArray array];
    for (NSString *key in @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", ]) {
        if ([[keyDict allKeys] containsObject:key]) {
            [resultArray addObject:@{@"indexTitle":key, @"data":[keyDict objectForKey:key]}];
        }
    }
    return resultArray;
}

// 判断字符串为空，剔除左右空格
+ (BOOL)isStringEmpty:(NSString *)str {
    NSString *result = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    return result.length <= 0;
}

@end

/*
 
 对应指令地址 http://fuckingclangwarnings.com
 防止警告
 #pragma clang diagnostic push
 #pragma clang diagnostic ignored "对应指令"
 self.completionBlock = ^ {
 ...
 };
 #pragma clang diagnostic pop
 弃用通知和保留周期误报是这个情况可能发生的常见情况。在这些罕见的，你绝对肯定一个特定的编译器或者稳态分析器警告需要被抑制时，#pragma就派上用场了
 Clang提供了一个方便的方法来解决这一切。通过使用#pragma clang diagnostic push/pop，你可以告诉编译器仅仅为某一特定部分的代码（最初的诊断设置在最后的pop被恢复）来忽视特定警告。
 常见用法
 
 1.方法弃用告警
 
 
 #pragma clang diagnostic push
 
 #pragma clang diagnostic ignored "-Wdeprecated-declarations"
 [TestFlight setDeviceIdentifier:[[UIDevice currentDevice] uniqueIdentifier]];
 
 #pragma clang diagnostic pop
 
 2.不兼容指针类型
 
 #pragma clang diagnostic push
 #pragma clang diagnostic ignored "-Wincompatible-pointer-types"
 //
 #pragma clang diagnostic pop
 
 3.循环引用
 // completionBlock is manually nilled out in AFURLConnectionOperation to break the retain cycle.
 #pragma clang diagnostic push
 #pragma clang diagnostic ignored "-Warc-retain-cycles"
 self.completionBlock = ^ {
 ...
 };
 #pragma clang diagnostic pop
 
 4.未使用变量
 #pragma clang diagnostic push
 #pragma clang diagnostic ignored "-Wunused-variable"
 int a;
 #pragma clang diagnostic pop
 
 */
