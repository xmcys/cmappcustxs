//
//  NcControl.h
//  newcustxm
//
//  Created by chyo on 16/11/10.
//  Copyright © 2016年 chyo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NcControl : UIControl

@property (nonatomic, strong) NSDictionary *userInfo;

@end


@interface NcButton : UIControl

@property (nonatomic, strong) NSDictionary *userInfo;

@end
