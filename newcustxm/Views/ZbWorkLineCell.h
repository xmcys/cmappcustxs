//
//  ZbWorkLineCell.h
//  newcustxm
//
//  Created by apple on 2017/7/26.
//  Copyright © 2017年 chyo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZbWorkLineCell : UITableViewCell

// 容器
@property (nonatomic, strong) IBOutlet UIView *viewBtn1;
@property (nonatomic, strong) IBOutlet UIView *viewBtn2;
@property (nonatomic, strong) IBOutlet UIView *viewBtn3;
@property (nonatomic, strong) IBOutlet UIView *viewBtn4;

// 按钮
@property (nonatomic, strong) IBOutlet NcButton *btn1;
@property (nonatomic, strong) IBOutlet NcButton *btn2;
@property (nonatomic, strong) IBOutlet NcButton *btn3;
@property (nonatomic, strong) IBOutlet NcButton *btn4;

// 图标
@property (nonatomic, strong) IBOutlet UIImageView *iv1;
@property (nonatomic, strong) IBOutlet UIImageView *iv2;
@property (nonatomic, strong) IBOutlet UIImageView *iv3;
@property (nonatomic, strong) IBOutlet UIImageView *iv4;

// label
@property (nonatomic, strong) IBOutlet UILabel *lab1;
@property (nonatomic, strong) IBOutlet UILabel *lab2;
@property (nonatomic, strong) IBOutlet UILabel *lab3;
@property (nonatomic, strong) IBOutlet UILabel *lab4;

// 点击block
@property (nonatomic, copy) void (^btnClickBlock)(NSDictionary *userInfo);

@end
