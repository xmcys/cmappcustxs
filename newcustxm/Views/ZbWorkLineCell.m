//
//  ZbWorkLineCell.m
//  newcustxm
//
//  Created by apple on 2017/7/26.
//  Copyright © 2017年 chyo. All rights reserved.
//

#import "ZbWorkLineCell.h"

@implementation ZbWorkLineCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - 点击事件
// 按钮点击
- (IBAction)btnOnClick:(NcButton *)sender {
    NSDictionary *xx = sender.userInfo;
    if (self.btnClickBlock) {
        self.btnClickBlock(xx);
    }
    return;
}

@end
