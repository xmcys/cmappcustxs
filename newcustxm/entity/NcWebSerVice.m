//
//  NcWebSerVice.m
//  newcustxm
//
//  Created by chyo on 16/11/8.
//  Copyright © 2016年 chyo. All rights reserved.
//

#import "NcWebSerVice.h"

static NSMutableDictionary *USER = nil;

@implementation NcWebSerVice

#pragma mark - 用户信息
+ (void)setUser:(NSMutableDictionary *)person {
    USER = person;
}

+ (NSMutableDictionary *)user {
    return USER;
}

+ (BOOL)isLogin {
    return USER != nil;
}

#pragma mark - 通用方法
// 获取网页完整地址
+ (NSString *)getHtmlFullUrl:(NSString *)url {
    if (url == nil || url.length <= 0) {
        return @"";
    }
    if([url rangeOfString:@"?"].location != NSNotFound) {
        return [NSString stringWithFormat:@"%@&userId=%@", url, USER[@"USER_ID"]];
    }
    return [NSString stringWithFormat:@"%@?userId=%@", url, USER[@"USER_ID"]];
}

// 获取图片完整地址
+ (NSString *)getImgFullUrl:(NSString *)url {
    if ([url hasPrefix:@"http"]) {
        return url;
    }
    return [NSString stringWithFormat:@"%@/Image/src/%@", WEBSERVICE_HOST, url];
}

// 获取图片完整地址不加src
+ (NSString *)getImgFullUrlNoScr:(NSString *)url {
    if ([url hasPrefix:@"http"]) {
        return url;
    }
    return [NSString stringWithFormat:@"%@/Image/%@", WEBSERVICE_HOST, url];
}

#pragma mark - 获取个人相关信息
+ (NSString *)getUserInfo {
    return [WEBSERVICE_HOST stringByAppendingString:@"/npWechat/userController/getUserInfo.do"];
}

#pragma mark - 登录
// 登录
+ (NSString *)verifyRetailerLogin {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/userController/verifyRetailerLogin.do"];
}
// 用户协议
+ (NSString *)getProtocol {
    return [WEBSERVICE_HOST stringByAppendingString:@"/npWechat/mobile/user/registerProtocol.jsp?type=12"];
}

#pragma mark - 检查版本
+ (NSString *)getLatestAppVersionInfo {
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/npWechat/appVersionManageController/getLatestAppVersionInfo.do?appType=%@&appId=%@", APP_TYPE, APP_ID]];
}

#pragma mark - 数据版本号
// 获取广告页、引导页、侧边栏、底部tab栏数据版本号
+ (NSString *)getDataVersions:(NSString *)appId
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/npWechat/api/getDataVersions/appId/%@/", appId]];
}

#pragma mark - 引导页、广告页
// 获取引导页、广告页数据
+ (NSString *)getAppPages:(NSString *)appId type:(NSString *)type versionNo:(NSString *)versionNo
{
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/npWechat/api/getAppPages/appId/%@/type/%@/versionNo/%@/", appId, type, versionNo]];
}

#pragma mark - 上传图片
+ (NSString *)uploadImageUrl {
    return [WEBSERVICE_HOST stringByAppendingString:@"/Image/servlet/appAction"];
}

#pragma mark - 接口-查询我的积分
+ (NSString *)queryIntegralByUserIdToAppUrl {
    return [WEBSERVICE_HOST stringByAppendingString:@"/npWechat/integralMgrController/queryIntegralByUserIdToApp.do"];
}

#pragma mark - 接口-查询工作台数据
+ (NSString *)getWorkNumUrl {
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/npWechat/userShoppingOrderController/getNum.do?merchantId=%@", USER[@"USER_ID"]]];
}

#pragma mark - 接口-查询快讯数据
+ (NSString *)getKuaiXunUrl {
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/npWechat/wxPubMsgListController/getKuaiXun.do"]];
}

#pragma mark - 接口-查询订单消息数量
+ (NSString *)getStautsNumUrl {
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/npWechat/userShoppingOrderController/getStautsNum.do?merchantId=%@", USER[@"USER_ID"]]];
}

#pragma mark - 接口-获取新品
+ (NSString *)getXinPinUrl {
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/npWechat/wxPubMsgListController/getXinPin.do?"]];
}

#pragma mark - 接口-菜单列表
+ (NSString *)getAppMenuNavListUrl:(NSString *)type {
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/npWechat/appMenuNavListController/getAppMenuNavList.do?navNo=%@", type]];
}

#pragma mark - 忘记密码
+ (NSString *)getForgetPwdUrl {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/user/forgetPwd.jsp"];
}

#pragma mark - tabHome-消息中心
+ (NSString *)msgUrl {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/msg/msg.jsp"];
}

#pragma mark - tabHome-到店购物
+ (NSString *)merchantShoppingUrl {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/shop/merchantShopping.jsp"];
}

#pragma mark - tabHome-移动支付
+ (NSString *)merchantPaySettingUrl {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/shop/merchantPaySetting.jsp"];
}

#pragma mark - tabHome-资讯服务
+ (NSString *)pubMsgrl {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/pubMsg/index.jsp"];
}

#pragma mark - tabHome-公告详情（快讯）
+ (NSString *)noticeDetailUrl:(NSString *)listId {
    return [WEBSERVICE_HOST stringByAppendingString:[NSString stringWithFormat:@"/npWechat/mobile/pubMsg/pubMsgDetail.jsp?url=%@", listId]];
}

#pragma mark - tab服务
+ (NSString *)tabServiceUrl {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/app/service.jsp"];
}

#pragma mark - tab工作台-交易管理
+ (NSString *)orderManageUrl {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/shop/service.jsp"];
}

#pragma mark - tab工作台-店铺管理
+ (NSString *)merchantManageUrl {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/shop/merchantManage.jsp"];
}

#pragma mark - tab工作台-商品管理
+ (NSString *)productManageUrl {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/merchantGoodsManager/mGManager.jsp"];
}

#pragma mark - tab工作台-交易管理-待付款
+ (NSString *)getNotPaidOrderUrl {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/shop/merchantOrder.jsp?orderStatus=20"];
}

// 待发货
+ (NSString *)getDfhPaidOrder {
    return [WEBSERVICE_HOST stringByAppendingString:@"/npWechat/mobile/shop/merchantOrder.jsp?orderStatus=40"];
}

#pragma mark - tab工作台-交易管理-待送货
+ (NSString *)getNotReceivingOrderUrl {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/shop/merchantOrder.jsp?orderStatus=50"];
}

#pragma mark - tab工作台-交易管理-待评价
+ (NSString *)getNotAppraiseOrderUrl {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/shop/merchantOrder.jsp?orderStatus=60"];
}
    
#pragma mark - tab工作台-交易管理-待退款
+ (NSString *)getMerchantNotRefundOrderUrl {
        return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/shop/merchantOrder.jsp?orderStatus=70"];
}

#pragma mark - tab工作台-交易管理-全部
+ (NSString *)getAllOrderUrl {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/shop/merchantOrder.jsp?orderStatus=all"];
}

#pragma mark - tab我的-营销活动
+ (NSString *)actListUrl {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/activity/actList.jsp"];
}

#pragma mark - tab我的-礼品管理
+ (NSString *)actGiftUrl {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/activity/actGift.jsp"];
}

#pragma mark - tab我的-店铺会员
+ (NSString *)merchantMemberUrl {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/shop/merchantMember.jsp"];
}

#pragma mark - tab我的-店铺资料
+ (NSString *)merchantInfoUrl {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/shop/merchantInfo.jsp"];
}

#pragma mark - tab我的-我的专卖证
+ (NSString *)dzzmzUrl {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/dzzmz/dzzmz.jsp"];
}

#pragma mark - tab我的-设置
+ (NSString *)retailerSettingsUrl {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/user/retailerSettings.jsp"];
}

#pragma mark - 店铺配送信息
+ (NSString *)merchantDeliveryInfoUrl {
    return [WEBSERVICE_HOST stringByAppendingPathComponent:@"npWechat/mobile/shop/merchantDeliveryInfo.jsp"];
}

@end
