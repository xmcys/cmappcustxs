//
//  ZbSaveManager.m
//  StoneTmall
//
//  Created by 张斌 on 15/8/29.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import "ZbSaveManager.h"

// 地图引导保存key
#define SAVE_KEY_MAP_GUIDE @"SAVE_KEY_MAP_GUIDE"

// 管理器实例
static ZbSaveManager *manager = nil;

// 版本保存key
static NSString * const VERSION_SAVE_KEY = @"VERSION_SAVE_KEY";
// 广告页保存key
static NSString * const AD_SAVE_KEY = @"AD_SAVE_KEY";
// 引导页保存key
static NSString * const GUIDE_SAVE_KEY = @"GUIDE_SAVE_KEY";

// 点击的闪屏广告
static NSDictionary *CLICK_AD_DICT = nil;

// 推送字典
static NSDictionary *PUSH_USER_INFO = nil;

// isSumitting
static BOOL isSumitting = NO;

@implementation ZbSaveManager

+ (instancetype)shareManager {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ZbSaveManager alloc] init];
        [manager onInit];
    });
    
    return manager;
}

// 初始化信息
- (void)onInit {
    return;
}

#pragma mark - 版本数据
// 设置版本数据
- (void)setVersionDict:(NSDictionary *)dict {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:dict forKey:VERSION_SAVE_KEY];
    [userDefault synchronize];
}
// 获取版本数据
- (NSDictionary *)getVersionDict {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault objectForKey:VERSION_SAVE_KEY];
}

#pragma mark - 广告页数据
// 设置广告页数据
- (void)setAdArray:(NSArray *)array {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:[NSArray arrayWithArray:array] forKey:AD_SAVE_KEY];
    [userDefault synchronize];
}
// 获取广告页数据
- (NSArray *)getAdArray {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault objectForKey:AD_SAVE_KEY];
}

#pragma mark - 引导页数据
// 设置引导页数据
- (void)setGuideArray:(NSArray *)array {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:[NSArray arrayWithArray:array] forKey:GUIDE_SAVE_KEY];
    [userDefault synchronize];
}
// 获取引导页数据
- (NSArray *)getGuideArray {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault objectForKey:GUIDE_SAVE_KEY];
}

#pragma mark - 推送字典
// 推送信息
+ (void)setPushUserInfo:(NSDictionary *)info {
    PUSH_USER_INFO = info;
}
+ (NSDictionary *)getPushUserInfo {
    return PUSH_USER_INFO;
}

#pragma mark - 点击的闪屏广告
+ (void)setClickAdDict:(NSDictionary *)adDict {
    CLICK_AD_DICT = adDict;
}
+ (NSDictionary *)getClickAdDict {
    return CLICK_AD_DICT;
}

#pragma mark - isSumitting
+ (BOOL)isSumitting {
    return isSumitting;
}
+ (void)setIsSumitting:(BOOL)isIng {
    isSumitting = isIng;
}

@end
