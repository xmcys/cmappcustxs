//
//  LocationServiceSingleton.m
//  AgreementRide
//
//  Created by qian on 15/5/9.
//  Copyright (c) 2015年 黄旺鑫. All rights reserved.
//

#import "LocationServiceManager.h"


static LocationServiceManager *manager = nil;

@interface LocationServiceManager ()<BMKLocationServiceDelegate>

///位置更新后执行的代码
@property (strong, nonatomic) LocationBlock locationBlock;
// 当前位置
@property (assign, nonatomic) BMKUserLocation *curLoc;

@end

@implementation LocationServiceManager

+ (instancetype)shareManager {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[LocationServiceManager alloc] init];
        [manager mapconfigure];
    });
    
    return manager;
}

- (void)mapconfigure {
    //初始化定位服务
    self.locationService = [[BMKLocationService alloc] init];
    self.locationService.delegate = self;
}

- (void)dealloc {
    self.locationService.delegate = nil;
}

/**
 *  打开定位服务
 */
- (void)startLocationService {
    
    [self.locationService startUserLocationService];
}

/**
 *  停止定位服务
 */
- (void)stopLocationService {
    [self.locationService stopUserLocationService];
}

/**
 *  用户更新调用此方法
 *
 *  @param locationBlock 位置更新后要执行的代码块
 */
- (void)updateLocationBlock:(LocationBlock)locationBlock {
    self.locationBlock = locationBlock;
}

// 获取当前位置经纬度
- (BMKUserLocation *)getCurLocation {
    return self.curLoc;
}

/**
 *用户位置更新后，会调用此函数
 *@param userLocation 新的用户位置
 */
- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation {
    
    // 不持续用定位，获取成功即可关闭
    [self stopLocationService];
    // 不重复更新位置
    if (self.curLoc && self.curLoc.location.coordinate.longitude == userLocation.location.coordinate.longitude && self.curLoc.location.coordinate.latitude == userLocation.location.coordinate.latitude) {
        return;
    }
    self.curLoc = userLocation;
    DLog(@"location:%@, %@", [NSNumber numberWithDouble:userLocation.location.coordinate.longitude], [NSNumber numberWithDouble:userLocation.location.coordinate.latitude]);
}

@end
