//
//  ZbSaveManager.h
//  StoneTmall
//
//  Created by 张斌 on 15/8/29.
//  Copyright (c) 2015年 stonetmall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZbSaveManager : NSObject

// 获取单例
+ (instancetype)shareManager;

// 设置版本数据
- (void)setVersionDict:(NSDictionary *)dict;
// 获取版本数据
- (NSDictionary *)getVersionDict;

// 设置广告页数据
- (void)setAdArray:(NSArray *)array;
// 获取广告页数据
- (NSArray *)getAdArray;

// 设置引导页数据
- (void)setGuideArray:(NSArray *)array;
// 获取引导页数据
- (NSArray *)getGuideArray;

#pragma mark - 推送字典
// 推送信息
+ (void)setPushUserInfo:(NSDictionary *)info;
+ (NSDictionary *)getPushUserInfo;

#pragma mark - 点击的闪屏广告
+ (void)setClickAdDict:(NSDictionary *)adDict;
+ (NSDictionary *)getClickAdDict;

#pragma mark - isSumitting
+ (BOOL)isSumitting;
+ (void)setIsSumitting:(BOOL)isIng;

@end
