//
//  LocationServiceSingleton.h
//  AgreementRide
//
//  Created by qian on 15/5/9.
//  Copyright (c) 2015年 黄旺鑫. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^LocationBlock)(void);

@interface LocationServiceManager : NSObject
///定位服务
@property (strong, nonatomic) BMKLocationService *locationService;

/**
 *  返回单例
 *
 */
+ (instancetype)shareManager;

/**
 *  打开定位服务
 */
- (void)startLocationService;

/**
 *  停止定位服务
 */
- (void)stopLocationService;

/**
 *  位置更新block设置
 *
 *  @param locationBlock 位置更新后要执行的代码块
 */
- (void)updateLocationBlock:(LocationBlock)locationBlock;

// 获取当前位置经纬度
- (BMKUserLocation *)getCurLocation;

@end
