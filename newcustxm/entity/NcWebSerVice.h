//
//  NcWebSerVice.h
//  newcustxm
//
//  Created by chyo on 16/11/8.
//  Copyright © 2016年 chyo. All rights reserved.
//

#import <Foundation/Foundation.h>


static NSString * APP_TYPE = @"1";                 // 安卓0，苹果1
static NSString * APP_ID = @"YX.WYX.001.XSBN";     // 多项目共用一份代码，以appId区分

// page类型广告页
#define PAGE_TYPE_AD @"0"
// page类型引导页
#define PAGE_TYPE_GUIDE @"1"

// 是否已查看引导页保存key
#define GUIDE_IS_SEE @"GUIDE_IS_SEE"

@interface NcWebSerVice : NSObject

// 接口地址
#define WEBSERVICE_HOST @"http://www.bn-tobacco.com.cn"
//#define WEBSERVICE_HOST @"http://192.168.253.1:8080"

// 包名
#define PACKAGE_NAME @"/npWechat"

#pragma mark - 用户信息
// 用户信息
+ (void)setUser:(NSMutableDictionary *)person;
+ (NSMutableDictionary *)user;
+ (BOOL)isLogin;

#pragma mark - 通用方法
// 获取网页完整地址
+ (NSString *)getHtmlFullUrl:(NSString *)url;
// 获取图片完整地址
+ (NSString *)getImgFullUrl:(NSString *)url;
// 获取图片完整地址不加src
+ (NSString *)getImgFullUrlNoScr:(NSString *)url;


#pragma mark - 获取个人相关信息
+ (NSString *)getUserInfo;

#pragma mark - 登录
// 登录
+ (NSString *)verifyRetailerLogin;
// 用户协议
+ (NSString *)getProtocol;


#pragma mark - 检查版本
+ (NSString *)getLatestAppVersionInfo;

#pragma mark - 数据版本号
// 获取广告页、引导页、侧边栏、底部tab栏数据版本号
+ (NSString *)getDataVersions:(NSString *)appId;

#pragma mark - 引导页、广告页
// 获取引导页、广告页数据
+ (NSString *)getAppPages:(NSString *)appId type:(NSString *)type versionNo:(NSString *)versionNo;

#pragma mark - 上传图片
+ (NSString *)uploadImageUrl;

#pragma mark - 接口-查询我的积分
+ (NSString *)queryIntegralByUserIdToAppUrl;

#pragma mark - 接口-查询工作台数据
+ (NSString *)getWorkNumUrl;

#pragma mark - 接口-查询快讯数据
+ (NSString *)getKuaiXunUrl;

#pragma mark - 接口-查询订单消息数量
+ (NSString *)getStautsNumUrl;

#pragma mark - 接口-获取新品
+ (NSString *)getXinPinUrl;

#pragma mark - 接口-菜单列表
+ (NSString *)getAppMenuNavListUrl:(NSString *)type;

#pragma mark - 忘记密码
+ (NSString *)getForgetPwdUrl;

#pragma mark - tabHome-消息中心
+ (NSString *)msgUrl;

#pragma mark - tabHome-到店购物
+ (NSString *)merchantShoppingUrl;

#pragma mark - tabHome-移动支付
+ (NSString *)merchantPaySettingUrl;

#pragma mark - tabHome-资讯服务
+ (NSString *)pubMsgrl;

#pragma mark - tabHome-公告详情（快讯）
+ (NSString *)noticeDetailUrl:(NSString *)listId;

#pragma mark - tab服务
+ (NSString *)tabServiceUrl;

#pragma mark - tab工作台-交易管理
+ (NSString *)orderManageUrl;

#pragma mark - tab工作台-店铺管理
+ (NSString *)merchantManageUrl;

#pragma mark - tab工作台-商品管理
+ (NSString *)productManageUrl;

#pragma mark - tab工作台-交易管理-待付款
+ (NSString *)getNotPaidOrderUrl;

// 待发货
+ (NSString *)getDfhPaidOrder;

#pragma mark - tab工作台-交易管理-待送货
+ (NSString *)getNotReceivingOrderUrl;

#pragma mark - tab工作台-交易管理-待评价
+ (NSString *)getNotAppraiseOrderUrl;

#pragma mark - tab工作台-交易管理-待退款
+ (NSString *)getMerchantNotRefundOrderUrl;

#pragma mark - tab工作台-交易管理-全部
+ (NSString *)getAllOrderUrl;

#pragma mark - tab我的-营销活动
+ (NSString *)actListUrl;

#pragma mark - tab我的-礼品管理
+ (NSString *)actGiftUrl;

#pragma mark - tab我的-店铺会员
+ (NSString *)merchantMemberUrl;

#pragma mark - tab我的-店铺资料
+ (NSString *)merchantInfoUrl;

#pragma mark - tab我的-我的专卖证
+ (NSString *)dzzmzUrl;

#pragma mark - tab我的-设置
+ (NSString *)retailerSettingsUrl;

#pragma mark - 店铺配送信息
+ (NSString *)merchantDeliveryInfoUrl;

@end
