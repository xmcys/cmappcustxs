//
//  AppDelegate.m
//  newcustxm
//
//  Created by chyo on 16/11/8.
//  Copyright © 2016年 chyo. All rights reserved.
//

// 与h5约定是否原生的标示
#define USER_AGENT_NATIVE_SIGN @"NativeObject"

#define UMSYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define _IPHONE80_ 80000

#import "AppDelegate.h"
#import "NcWebSerVice.h"
#import "ZbSaveManager.h"
#import "HsWebViewController.h"
// 仿系统推送
#import "LNNotificationsUI.h"
#import <AudioToolbox/AudioToolbox.h>

@interface AppDelegate ()

// 更新地址
@property (nonatomic, strong) NSString *targetUrl;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // 开启日志
//    [HttpRequest setNeedsLog:YES];
    
    // 友盟推送
    [UMessage startWithAppkey:@"5743b5ee67e58e0fa3000f81" launchOptions:launchOptions];
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= _IPHONE80_
    if(UMSYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        //register remoteNotification types （iOS 8.0及其以上版本）
        UIMutableUserNotificationAction *action1 = [[UIMutableUserNotificationAction alloc] init];
        action1.identifier = @"action1_identifier";
        action1.title=@"Accept";
        action1.activationMode = UIUserNotificationActivationModeForeground;//当点击的时候启动程序
        
        UIMutableUserNotificationAction *action2 = [[UIMutableUserNotificationAction alloc] init];  //第二按钮
        action2.identifier = @"action2_identifier";
        action2.title=@"Reject";
        action2.activationMode = UIUserNotificationActivationModeBackground;//当点击的时候不启动程序，在后台处理
        action2.authenticationRequired = YES;
        //需要解锁才能处理，如果action.activationMode = UIUserNotificationActivationModeForeground;则这个属性被忽略；
        action2.destructive = YES;
        
        UIMutableUserNotificationCategory *categorys = [[UIMutableUserNotificationCategory alloc] init];
        categorys.identifier = @"category1";//这组动作的唯一标示
        [categorys setActions:@[action1,action2] forContext:(UIUserNotificationActionContextDefault)];
        
        UIUserNotificationSettings *userSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge|UIUserNotificationTypeSound|UIUserNotificationTypeAlert
                                                                                     categories:[NSSet setWithObject:categorys]];
        [UMessage registerRemoteNotificationAndUserNotificationSettings:userSettings];
        
    } else{
        //register remoteNotification types (iOS 8.0以下)
        [UMessage registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge
         |UIRemoteNotificationTypeSound
         |UIRemoteNotificationTypeAlert];
    }
#else
    //register remoteNotification types (iOS 8.0以下)
    [UMessage registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge
     |UIRemoteNotificationTypeSound
     |UIRemoteNotificationTypeAlert];
    
#endif
    //for log
//    [UMessage setLogEnabled:NO];
    
    // UIWebView的UserAgent设置
    UIWebView* webView = [[UIWebView alloc] initWithFrame:CGRectZero];
    NSString *agent = [webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
    //    NSLog(@"agent=%@", agent);
    if ([agent rangeOfString:USER_AGENT_NATIVE_SIGN].location == NSNotFound) {
        // 往h5的UserAgent加入原生标示
        NSString *apiVer = [NSString stringWithFormat:@"IOS%.1f@%@", [[[UIDevice currentDevice] systemVersion] floatValue], [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
        NSString *idfv =[[[UIDevice currentDevice] identifierForVendor] UUIDString];
        NSDictionary*dictionnary = @{@"UserAgent":[NSString stringWithFormat:@"%@/%@/%@/%@", agent, USER_AGENT_NATIVE_SIGN, idfv, apiVer]};
        [[NSUserDefaults standardUserDefaults] registerDefaults:dictionnary];
    }
    
//    // 设置导航栏底部线条
//    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
//    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    // 设置状态栏为白色
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    // 设置tabbar 选中颜色
    [[UITabBar appearance] setTintColor:UIColorFromHexString(@"139460")];
    
    // 启动百度地图
    _mapManager = [[BMKMapManager alloc]init];
    BOOL ret = [_mapManager start:@"gmdlBfb08vGQ1BIDBrExiMwcoakLgme5"  generalDelegate:nil];
    if (!ret) {
        DLog(@"map manager start failed!");
    } else {
        DLog(@"map manager start success!");
    }
    // 启动定位服务
    [[LocationServiceManager shareManager] startLocationService];
    
    // 仿原生推送注册
    NSDictionary *appInfo = [[NSBundle mainBundle] infoDictionary];
    NSString *appName = [appInfo objectForKey:@"CFBundleDisplayName"];
    [[LNNotificationCenter defaultCenter] registerApplicationWithIdentifier:@"all-the-same" name:appName icon:[UIImage imageNamed:@"PushIcon"] defaultSettings:[LNNotificationAppSettings defaultNotificationAppSettings]];
    
    // 点击推送启动
    if (launchOptions) {
        NSDictionary*userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if(userInfo)
        {
            [self launchWithRemoteNotification:userInfo];
        }
    }
    
    // 获取app版本号
    [self loadAppVersionData];
    return YES;
}

#pragma mark - 推送
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(nonnull NSError *)error {
    NSString *error_str = [NSString stringWithFormat: @"%@", error];
    DLog(@"Failed to get token, error:%@", error_str);
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(nonnull NSData *)deviceToken {
    NSString *deviceTokenStr = [[[[deviceToken description] stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                 stringByReplacingOccurrencesOfString: @">" withString: @""]
                                stringByReplacingOccurrencesOfString: @" " withString: @""];
    DLog(@"%@", deviceTokenStr);
    [UMessage registerDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [UMessage didReceiveRemoteNotification:userInfo];
    DLog(@"APP运行中推送===%@", userInfo);
    
    if (application.applicationState == UIApplicationStateActive) {
        // 正在运行
        
        // 发本地推送，让通知栏有消息
        UILocalNotification *localNotif = [[UILocalNotification alloc] init];
        localNotif.fireDate = [NSDate date];
        localNotif.timeZone = [NSTimeZone defaultTimeZone];
        localNotif.alertBody = userInfo[@"aps"][@"alert"];
        localNotif.soundName = UILocalNotificationDefaultSoundName;
        localNotif.userInfo = userInfo;
        //  设置好本地推送后必须调用此方法启动此推送
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
        
        // 播放声音
        AudioServicesPlaySystemSound(1312);
        // 仿系统推送
        LNNotification* notification = [LNNotification notificationWithMessage:userInfo[@"aps"][@"alert"]];
        notification.title = @"";
        notification.defaultAction = [LNNotificationAction actionWithTitle:@"View" handler:^(LNNotificationAction *action) {
            UIApplication *application = [UIApplication sharedApplication];
            [application cancelLocalNotification:localNotif];
            
            NSDictionary *info = [NSDictionary dictionaryWithDictionary:userInfo];
            if (info[@"extras"] != nil) {
                info = userInfo[@"extras"];
            }
            NSString *pushType = info[@"TYPE"];
            if (!pushType || [pushType isKindOfClass:[NSNull class]]) {
                return;
            }
            
            if (![NcWebSerVice isLogin]) {
                // 未登录则不处理
                return;
            }
            [self handleRemoteNotification:info];
        }];
        [[LNNotificationCenter defaultCenter] presentNotification:notification forApplicationIdentifier:@"all-the-same"];
    } else {
        
        // 后台运行点击推送打开
        if (userInfo[@"extras"] != nil) {
            userInfo = userInfo[@"extras"];
        }
        NSString *pushType = userInfo[@"TYPE"];
        if (!pushType || [pushType isKindOfClass:[NSNull class]]) {
            return;
        }
        
        if (![NcWebSerVice isLogin]) {
            // 未登录则不处理
            return;
        }
        [self handleRemoteNotification:userInfo];
    }
}

// 收到本地推送
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    if (application.applicationState == UIApplicationStateActive) {
        // 正在运行，会弹出仿系统推送，所以不处理
        return;
    }
    
    NSDictionary *userInfo = notification.userInfo;
    if (userInfo[@"extras"] != nil) {
        userInfo = userInfo[@"extras"];
    }
    NSString *pushType = userInfo[@"TYPE"];
    if (!pushType || [pushType isKindOfClass:[NSNull class]]) {
        return;
    }
    
    // 需要登录的推送类型
    if (![NcWebSerVice isLogin]) {
        // 未登录则不处理
        return;
    }
    [self handleRemoteNotification:userInfo];
}

// 推送跳转逻辑
- (void)handleRemoteNotification:(NSDictionary *)userInfo {
    UIViewController *root = self.window.rootViewController;
    UINavigationController *rootNav = (UINavigationController *)root;
    UIStoryboard *mainSb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSString *pushType = userInfo[@"TYPE"];
    NSString *para1 = userInfo[@"URL"];
    Zb_Push_Type type = (Zb_Push_Type)(pushType.intValue);
    switch (type) {
        case Zb_Push_Order:
        case Zb_Push_Msg:
        {
            HsWebViewController *web = [mainSb instantiateViewControllerWithIdentifier:@"HsWebViewController"];
            web.url = para1;
            [rootNav pushViewController:web animated:YES];
            break;
        }
        default:
            break;
    }
    return;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

// 收到推送--启动时
- (void)launchWithRemoteNotification:(NSDictionary *)userInfo
{
    if (userInfo[@"extras"] != nil) {
        userInfo = userInfo[@"extras"];
    }
    NSString *pushType = userInfo[@"TYPE"];
    if (!pushType || [pushType isKindOfClass:[NSNull class]]) {
        return;
    }
    
    [ZbSaveManager setPushUserInfo:userInfo];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - 接口调用
// 请求app版本号
- (void)loadAppVersionData {
    [HttpRequest getWithURLString:[NcWebSerVice getLatestAppVersionInfo] parameters:nil success:^(id responseObject) {
        BOOL result = [responseObject[@"successful"] boolValue];
        if (!result) {
            return;
        }
        // 本地版本号
        NSDictionary *appInfo = [[NSBundle mainBundle] infoDictionary];
        NSString *curBuild = [appInfo objectForKey:@"CFBundleVersion"];
        
        // 更新信息
        NSDictionary *infoDict = responseObject[@"data"];
        NSString *tagBuild = infoDict[@"BUILD_CODE"];
        NSString *content = infoDict[@"CONTENT"];
        self.targetUrl = infoDict[@"TARGET_URL"];
        if (tagBuild.integerValue > curBuild.integerValue) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"应用更新" message:content delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"前往", nil];
            [alert show];
        }
        BOOL isSumitting = (curBuild.integerValue > tagBuild.integerValue);
        [ZbSaveManager setIsSumitting:isSumitting];
        return;
        
    } failure:^(NSError *error) {
        return;
    }];
}

#pragma mark UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == alertView.cancelButtonIndex) {
        return;
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.targetUrl]];
}

#pragma mark - 广告显示结束
- (void)onAdShowOk {
    [self enterLogin];
}

#pragma mark - 登录/注销
// 登录成功
- (void)loginSuccess {
    UIStoryboard *Main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *nav = [Main instantiateViewControllerWithIdentifier:@"NcNavViewController"];
    self.window.rootViewController = nav;
}

#pragma mark - private
// 进入登录界面
- (void)enterLogin {
    UIStoryboard *mainSb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *nav = [mainSb instantiateViewControllerWithIdentifier:@"LoginNav"];;
    self.window.rootViewController = nav;
}

#pragma mark - public
// 退出登录逻辑
- (void)logOut {
//    NSDictionary *userDict = [HsWebService user];
//    // 友盟推送设置别名，进行单个推送
//    [UMessage removeAlias:userDict[@"custLicenceCode"] type:kUMessageAliasTypeSina response:^(id responseObject, NSError *error) {
//        NSLog(@"removeAlias %@ - %@", responseObject, error);
//    }];
    [NcWebSerVice setUser:nil];
    // 清空用户密码，否则会自动登录
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:nil forKey:SAVE_KEY_PSW];
    [userDefaults synchronize];
    [self enterLogin];
}

@end
