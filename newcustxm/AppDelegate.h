//
//  AppDelegate.h
//  newcustxm
//
//  Created by chyo on 16/11/8.
//  Copyright © 2016年 chyo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) BMKMapManager *mapManager;

// 广告显示结束
- (void)onAdShowOk;
// 登录成功
- (void)loginSuccess;
// 退出登录逻辑
- (void)logOut;
// 推送跳转逻辑
- (void)handleRemoteNotification:(NSDictionary *)userInfo;

@end

