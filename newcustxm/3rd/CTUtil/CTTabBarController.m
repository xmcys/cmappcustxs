//
//  CTTabBarController.m
//  ChyoTools
//
//  Created by 陈 宏超 on 14-1-15.
//  Copyright (c) 2014年 Chyo. All rights reserved.
//

#import "CTTabBarController.h"

@interface CTTabBarController ()

- (void)back;

@end

@implementation CTTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBar.selectionIndicatorImage = [[UIImage alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1];;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
