//
//  CTViewController.m
//  ChyoTools
//
//  Created by 陈 宏超 on 13-12-31.
//  Copyright (c) 2013年 Chyo. All rights reserved.
//

#import "CTViewController.h"
#import "CTUtil.h"


@implementation ExtraNavView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.layer.borderWidth = 0.5;
        self.backgroundColor = [UIColor whiteColor];
        self.layer.borderColor = [UIColor lightGrayColor].CGColor;
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 44, self.frame.size.width, 44)];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.textColor = [UIColor blackColor];
        [self addSubview:self.titleLabel];
        
        self.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 44, self.bounds.size.width / 3, 44)];
        self.leftView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.leftView];
        
        self.rightView = [[UIView alloc] initWithFrame:CGRectMake(self.bounds.size.width / 3 * 2, self.frame.size.height - 44, self.bounds.size.width / 3, 44)];
        self.rightView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.rightView];
    }
    return self;
}

- (void)removeAllSubViewsInView:(UIView *)view
{
    for (UIView * v in view.subviews) {
        [v removeFromSuperview];
    }
}

@end

@interface CTViewController ()

// 重新调整视图大小
- (void)resizeViewFrame;
- (void)back;
- (void)initExtraNavBar;
- (void)addBackButtonInView:(UIView *)view;

@end

@implementation CTViewController

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // 跟改变任何视图状态的代码不能写在这边，至少要写在ViewDidLoad中，不然会出现一些奇怪的现象
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1];;
    [self resizeViewFrame];
    if (self.navigationController.viewControllers.count != 0 && self.navigationController.visibleViewController != [self.navigationController.viewControllers objectAtIndex:0]) {
        UIView * lv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth / 3, 44)];
        [self addBackButtonInView:lv];
        [lv sizeToFit];
        UIBarButtonItem * leftItem = [[UIBarButtonItem alloc] initWithCustomView:lv];
        self.navigationItem.leftBarButtonItem = leftItem;
    }
    
    [self initExtraNavBar];
}

- (void)back
{
    if (self.navigationController.childViewControllers.count <= 1) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (void)addBackButtonInView:(UIView *)view
{
    UIView * leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    leftView.backgroundColor = [UIColor clearColor];
    leftView.userInteractionEnabled = YES;
    UITapGestureRecognizer * tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(back)];
    [leftView addGestureRecognizer:tapGes];
    
    UIImageView * leftImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavBack"]];
    [leftImg sizeToFit];
    leftImg.center = CGPointMake(leftImg.frame.size.width * 0.75, leftView.frame.size.height / 2);
    [leftView addSubview:leftImg];
    
    [view addSubview:leftView];
}

- (void)initExtraNavBar
{
    if (VERSION_LESS_THAN_IOS7) {
        self.extraNavView = [[ExtraNavView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 44)];
    } else {
        self.extraNavView = [[ExtraNavView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 64)];
    }
    [self setExtraNavViewHidden:YES];
    [self.view addSubview:self.extraNavView];
}

// IOS7有效，需要在info.plist中先设置View Controller-bases status bar appearance = YES
- (BOOL)prefersStatusBarHidden
{
    return self.stautsBarHidden;
}

- (void)setStautsBarHidden:(BOOL)stautsBarHidden
{
    _stautsBarHidden = stautsBarHidden;
    
    // IOS6及以下有效
    [[UIApplication sharedApplication] setStatusBarHidden:self.stautsBarHidden];
    
    // IOS7有效
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
    int offset = 0;
    
    // IOS7状态不占用空间
    if (VERSION_LESS_THAN_IOS7) {
        offset = self.stautsBarHidden ? -20 : 20;
    }
    
    if (self.navigationController.navigationBar.frame.size.height == 0) {
        // 窗体位置上/下移offset像素，大小增加/减少offset像素
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + offset, self.view.frame.size.width, self.view.frame.size.height - offset);
    } else {
        // IOS6下视图上移时内容区会产生偏差（XCode5 bug)
        self.navigationController.view.frame = CGRectMake(self.navigationController.view.frame.origin.x, self.navigationController.view.frame.origin.y + offset, self.navigationController.view.frame.size.width, self.navigationController.view.frame.size.height - offset);
    }
    
}

- (void)resizeViewFrame
{
    CGRect frame = self.view.frame;
    int height = [UIScreen mainScreen].bounds.size.height;
    // IOS7以下状态栏占用20px
    if (VERSION_LESS_THAN_IOS7 && !self.stautsBarHidden) {
        height -= 20;
    }
    UINavigationBar * navBar = self.navigationController.navigationBar;
    // 状态栏高度不为0，代表控制器被包含在导航控制器中
    if (navBar.frame.size.height != 0) {
        // 导航栏未被隐藏，并且未被设置为透明（透明不占用空间）
        if (!navBar.hidden && !navBar.translucent) {
            // IOS7 以上导航栏和状态栏合并占用64
            height -= VERSION_LESS_THAN_IOS7 ? 44 : 64;
        }
    }
    
    // tab栏占位
    UITabBar * tabBar = self.tabBarController.tabBar;
    if (tabBar.frame.size.height != 0) {
        height -= tabBar.frame.size.height;
    }
    
    height -= self.bottomOffset;
    
    self.view.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, height);
    
//    NSLog(@"[CTViewController - resizeViewFrame] width: %.0f, height: %.0f", self.view.frame.size.width, self.view.frame.size.height);
}

- (void)setExtraNavViewHidden:(BOOL)hidden
{
    self.extraNavView.hidden = hidden;
    if (self.extraNavView.hidden) {
        self.navigationController.navigationBarHidden = NO;
    } else {
        self.navigationController.navigationBarHidden = YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setTabBarItemWithTitle:(NSString *)title titleColor:(UIColor *)color titleSelectedColor:(UIColor *)selectedColor image:(UIImage *)image selectedImage:(UIImage *)selectedImage
{
    UITabBarItem * item = nil;
    if (VERSION_LESS_THAN_IOS7) {
        item = [[UITabBarItem alloc] initWithTitle:title image:image tag:0];
    } else {
        image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        item = [[UITabBarItem alloc] initWithTitle:title image:image selectedImage:selectedImage];
    }
    [item setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:image];
    [item setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                  color, UITextAttributeTextColor,
                                  nil] forState:UIControlStateNormal];
    [item setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                  selectedColor, UITextAttributeTextColor,
                                  nil] forState:UIControlStateSelected];
    self.tabBarItem = item;
}

@end
