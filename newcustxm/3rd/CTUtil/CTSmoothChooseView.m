//
//  CTSmoothChooseView.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-21.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTSmoothChooseView.h"
#import <QuartzCore/QuartzCore.h>

@interface CTSmoothChooseView ()

@property (nonatomic, strong) UILabel * current;
@property (nonatomic) float itemWidth;
@property (nonatomic) float itemMargin;

@end

@implementation CTSmoothChooseView

- (id)init
{
    NSAssert(1 != 1, @"[CTSmoothChooseView - init] use initInView instead");
    return nil;
}

- (id)initWithFrame:(CGRect)frame
{
    NSAssert(1 != 1, @"[CTSmoothChooseView - initWithFrame] use initInView instead");
    return nil;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    NSAssert(1 != 1, @"[CTSmoothChooseView - initWithCoder] use initInView instead");
    return nil;
}

- (id)initInView:(UIView *)view
{
    self = [super initWithFrame:CGRectMake(view.frame.size.width - 25, 5, 25, view.frame.size.height - 10)];
    if (self) {
        self.topOffset = 5;
        self.bottomOffset = 5;
        self.rightOffset = 5;
        self.backgroundColor = [UIColor clearColor];
        self.layer.cornerRadius = 3.0f;
        [view addSubview:self];
        
        self.current = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 48, 48)];
        self.current.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.15];
        self.current.font = [UIFont boldSystemFontOfSize:20.0f];
        self.current.layer.cornerRadius = 4.0f;
        self.current.textColor = [UIColor colorWithRed:163.0/255.0 green:17.0/255.0 blue:121.0/255.0 alpha:1.0];;
        self.current.textAlignment = NSTextAlignmentCenter;
        self.current.hidden = YES;
        self.current.center = CGPointMake(view.frame.size.width / 2, view.frame.size.height / 2);
        [view addSubview:self.current];
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.1];
    [self touchesMoved:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint point = [[touches anyObject] locationInView:self];
    int index = point.y / self.itemWidth;
    if (index < self.words.count) {
        self.current.text = [self.words objectAtIndex:index];
        self.current.hidden = NO;
        if ([self.delegate respondsToSelector:@selector(ctSmoothChooseView:didTouchedAtIndex:)]) {
            [self.delegate ctSmoothChooseView:self didTouchedAtIndex:index];
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.backgroundColor = [UIColor clearColor];
    [self touchesMoved:touches withEvent:event];
    self.current.hidden = YES;
}

- (void)setWords:(NSArray *)words
{
    _words = words;
    [self reset];
}

- (void)reset
{
    self.frame = CGRectMake(self.superview.frame.size.width - self.frame.size.width - self.rightOffset, self.topOffset, self.frame.size.width, self.superview.frame.size.height - self.topOffset - self.bottomOffset);
    self.itemWidth = self.frame.size.width;
    self.itemMargin = (self.frame.size.width - self.itemWidth) / 2;
    if (self.frame.size.height / self.words.count < self.itemWidth) {
        self.itemWidth = self.frame.size.height / self.words.count;
        self.itemMargin = 0;
    }
    for (UIView * v in self.subviews) {
        [v removeFromSuperview];
    }
    for (int i = 0; i < self.words.count; i++) {
        UILabel * item = [[UILabel alloc] initWithFrame:CGRectMake((self.frame.size.width - self.itemWidth) / 2, self.itemMargin * (i + 1) + self.itemWidth * i, self.itemWidth, self.itemWidth)];
        item.backgroundColor = [UIColor clearColor];
        item.text = [self.words objectAtIndex:i];
        item.font = [UIFont systemFontOfSize:13.0f];
        item.textAlignment = NSTextAlignmentCenter;
        item.textColor = [UIColor colorWithRed:163.0/255.0 green:17.0/255.0 blue:121.0/255.0 alpha:1.0];
        [self addSubview:item];
    }
}

@end
