//
//  CTImageView.m
//  ChyoTools
//
//  Created by 陈 宏超 on 14-1-7.
//  Copyright (c) 2014年 Chyo. All rights reserved.
//

#import "CTImageView.h"
#import <QuartzCore/QuartzCore.h>

#define CONNECT_TIME_OUT 10
#define IMAGE_CACHE_PATH ([NSString stringWithFormat:@"%@/images", [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0]])

@interface CTImageLoadingView : UIView

@property (nonatomic, strong) UILabel * textLabel;
@property (nonatomic, strong) UIImageView * loadingImage;

@end

@implementation CTImageLoadingView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.layer.cornerRadius = 5.0f;
        self.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1];
        
        self.loadingImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
        self.loadingImage.image = [UIImage imageNamed:@"IndicateLoading"];
        
        // 增加旋转动画
        CABasicAnimation * loadingAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        loadingAnimation.toValue = [NSNumber numberWithFloat:M_PI * 2.0];
        loadingAnimation.duration = 1.0;
        loadingAnimation.repeatCount = HUGE_VALF;  // 无限循环
        loadingAnimation.cumulative = YES; // 记录动画结束后，图片的状态，NO的时候，图片会恢复初始状态
        [self.loadingImage.layer addAnimation:loadingAnimation forKey:@"loadingImageAnimation"];
        
        [self addSubview:self.loadingImage];
        
        self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        self.textLabel.text = @"加载中...";
        self.textLabel.font = [UIFont systemFontOfSize:11.0f];
        self.textLabel.textColor = [UIColor lightGrayColor];
        self.textLabel.backgroundColor = [UIColor clearColor];
        [self.textLabel sizeToFit];
        [self addSubview:self.textLabel];
        
        // 如果图的宽度比加载指示图还小时，重定义加载指示图的大小，并且不显示文字
        if (self.frame.size.width < self.loadingImage.frame.size.width) {
            self.loadingImage.frame = CGRectMake(0, 0, self.frame.size.width - 2, self.frame.size.width - 2);
            self.textLabel.hidden = YES;
            self.loadingImage.center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
        }
        // 如果图的宽度比加载指示图还小时，重定义加载指示图的大小，并且不显示文字
        else if (self.frame.size.height < self.loadingImage.frame.size.height) {
            self.loadingImage.frame = CGRectMake(0, 0, self.frame.size.height - 2, self.frame.size.height - 2);
            self.textLabel.hidden = YES;
            self.loadingImage.center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
        }
        // 如果图的宽度比文字小，或者图的高度不足以同时容纳加载指示图和文字时，隐藏文字
        else if (self.frame.size.width < self.textLabel.frame.size.width + 5 || self.frame.size.height < self.loadingImage.frame.size.height + self.textLabel.frame.size.height + 10){
            self.textLabel.hidden = YES;
            self.loadingImage.center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
        }
        else {
            int height = self.textLabel.frame.size.height + self.loadingImage.frame.size.height;
            int startY = (self.frame.size.height - height) / 2;
            self.loadingImage.center = CGPointMake(self.frame.size.width / 2, startY + self.loadingImage.frame.size.height / 2);
            self.textLabel.center = CGPointMake(self.frame.size.width / 2, startY + self.loadingImage.frame.size.height + 2 + self.textLabel.frame.size.height / 2);
        }
        
        self.contentMode = UIViewContentModeScaleAspectFit;
        self.clipsToBounds = YES;
    }
    return self;
}

@end

@interface CTImageView () <NSURLConnectionDelegate, NSURLConnectionDataDelegate>

@property (nonatomic, strong) CTImageLoadingView * loadingView;
@property (nonatomic, strong) NSURLConnection * conn;
@property (nonatomic, strong) NSMutableData * data;
@property (nonatomic, strong) UIView * highlightedView;

- (void)setDefault;
- (void)downloadFromUrl;
- (void)readFromCache;
- (void)singleTap;

@end

@implementation CTImageView

- (id)init
{
    NSAssert(1 != 1, @"[CTImageView-init] 请使用其他实例化方法");
    return nil;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setDefault];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setDefault];
    }
    return self;
}

- (id)initWithImage:(UIImage *)image
{
    self = [super initWithImage:image];
    if (self) {
        [self setDefault];
    }
    return self;
}

- (id)initWithImage:(UIImage *)image highlightedImage:(UIImage *)highlightedImage
{
    self = [super initWithImage:image highlightedImage:highlightedImage];
    if (self) {
        [self setDefault];
    }
    return self;
}


- (void)setDefault
{
    // 创建缓存文件夹 /library/caches/images
    NSError * err = nil;
    NSFileManager * fileMgr = [NSFileManager defaultManager];
    if (![fileMgr fileExistsAtPath:IMAGE_CACHE_PATH]) {
        [fileMgr createDirectoryAtPath:IMAGE_CACHE_PATH withIntermediateDirectories:YES attributes:nil error:&err];
        if (err) {
            NSLog(@"图片缓存文件夹创建失败：%@", err);
        }
    }
    self.fileCacheEnabled = YES;
    self.loadingViewEnabled = YES;
    self.loadingView = [[CTImageLoadingView alloc] initWithFrame:self.bounds];
    self.loadingView.hidden = YES;
    [self addSubview:self.loadingView];
    [self stopAnimating];
    
    // 点击高亮视图
    self.highlightedView = [[UIView alloc] initWithFrame:self.bounds];
    self.highlightedView.backgroundColor = [UIColor blackColor];
    self.highlightedView.alpha = 0.0;
    self.highlightedView.hidden = YES;
    [self addSubview:self.highlightedView];
    
    // 添加tap手势，用于点击事件
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:tap];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // 显示高亮视图，将图片变暗
    if ([self.delegate respondsToSelector:@selector(ctImageViewDidClicked:)]) {
        self.highlightedView.hidden = NO;
        [UIView animateWithDuration:0.15 animations:^{
            self.highlightedView.alpha = 0.3;
        }];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    // 隐藏高亮视图，图片恢复
    if ([self.delegate respondsToSelector:@selector(ctImageViewDidClicked:)]) {
        [UIView animateWithDuration:0.15 animations:^{
            self.highlightedView.alpha = 0.0;
        } completion:^(BOOL isFinished){
            if (isFinished) {
                self.highlightedView.hidden = YES;
            }
        }];
    }
}

- (void)singleTap
{
    // 触发点击事件
    if ([self.delegate respondsToSelector:@selector(ctImageViewDidClicked:)]) {
        [self.delegate ctImageViewDidClicked:self];
        [UIView animateWithDuration:0.2 animations:^{
            self.highlightedView.alpha = 0.0;
        } completion:^(BOOL isFinished){
            if (isFinished) {
                self.highlightedView.hidden = YES;
            }
        }];
    }
}

- (void)setLoadingViewEnabled:(BOOL)loadingViewEnabled
{
    // 设置禁用时，先停止动画再赋值
    if (_loadingViewEnabled && !loadingViewEnabled) {
        [self stopAnimating];
    }
    _loadingViewEnabled = loadingViewEnabled;
    if (loadingViewEnabled) {
        self.loadingView.hidden = NO;
        [self startAnimating];
    } else {
        self.loadingView.hidden = YES;
    }
}

- (void)startAnimating
{
    CALayer * layer = self.loadingView.loadingImage.layer;
    // 判断是否启用，并且是不是暂停状态
    if (self.loadingViewEnabled && layer.speed == 0.0) {
        CFTimeInterval timeOffset = [layer timeOffset];
        layer.speed = 1.0;
        layer.timeOffset = 0.0;
        layer.beginTime = 0.0;
        CFTimeInterval beginTime = [layer convertTime:CACurrentMediaTime() fromLayer:nil] - timeOffset;
        layer.beginTime = beginTime;
        self.loadingView.hidden = NO;
    }
    [super startAnimating];
}

- (void)stopAnimating
{
    CALayer * layer = self.loadingView.loadingImage.layer;
    // 判断是否启用，并且是不是动画状态
    if (self.loadingViewEnabled && layer.speed != 0.0) {
        CFTimeInterval pause = [layer convertTime:CACurrentMediaTime() fromLayer:nil];
        layer.speed = 0.0;
        layer.timeOffset = pause;
    }
    [super stopAnimating];
}

- (void)loadImage
{
    if (self.url == nil || self.url.length == 0 || self.isLoading) {
        return;
    }
    self.data = nil;
    _isLoading = YES;
    self.loadingView.textLabel.text = @"加载中...";
    [self startAnimating];
    // 判断是否启用文件缓存，并且文件名不为空
    if (self.fileCacheEnabled && self.specailName != nil && self.specailName.length != 0) {
        [NSThread detachNewThreadSelector:@selector(readFromCache) toTarget:self withObject:nil];
    } else {
        [self downloadFromUrl];
    }
    
}

- (void)setUrl:(NSString *)url
{
    _url = url;
    if (self.specailName == nil || self.specailName.length == 0) {
        // 默认设置文件名为图片路径，去除http://，并且替换 / 为___
        self.specailName = [[self.url stringByReplacingOccurrencesOfString:@"http://" withString:@""] stringByReplacingOccurrencesOfString:@"/" withString:@"___"];
    }
}

- (void)readFromCache
{
    NSData * data = [NSData dataWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", IMAGE_CACHE_PATH, self.specailName]];
    // 图片缓存不为空
    if (data != nil && data.length != 0) {
        self.data = [[NSMutableData alloc] initWithData:data];
        [self performSelectorOnMainThread:@selector(connectionDidFinishLoading:) withObject:nil waitUntilDone:NO];
    } else {
        [self performSelectorOnMainThread:@selector(downloadFromUrl) withObject:nil waitUntilDone:NO];
    }
}

// 下载图片
- (void)downloadFromUrl
{
    self.conn = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:CONNECT_TIME_OUT] delegate:self startImmediately:NO];
    [self.conn start];
}

#pragma mark -
#pragma mark NSURLConnection Delegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"图片加载失败：%@", [error description]);
    _isLoading = NO;
    self.loadingView.textLabel.text = @"加载失败";
    [self stopAnimating];
    [self.loadingView.textLabel sizeToFit];
    if ([self.delegate respondsToSelector:@selector(ctImageViewLoadDone:withResult:)]) {
        [self.delegate ctImageViewLoadDone:self withResult:CTImageViewStateFailure];
    }
    [self.conn cancel];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse * httpURLResponse = (NSHTTPURLResponse *) response;
    if (httpURLResponse.statusCode != 200) {
        _isLoading = NO;
        self.loadingView.textLabel.text = @"加载失败";
        [self stopAnimating];
        [self.loadingView.textLabel sizeToFit];
        if ([self.delegate respondsToSelector:@selector(ctImageViewLoadDone:withResult:)]) {
            [self.delegate ctImageViewLoadDone:self withResult:CTImageViewStateFailure];
        }
        NSLog(@"图片加载失败：%d, %@", (int)(httpURLResponse.statusCode), [[httpURLResponse allHeaderFields] description]);
        [self.conn cancel];
    }
    else {
        self.data = [[NSMutableData alloc] init];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.data appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    _isLoading = NO;
    self.image = [UIImage imageWithData:self.data];
    self.loadingView.hidden = YES;
    [self stopAnimating];
    if ([self.delegate respondsToSelector:@selector(ctImageViewLoadDone:withResult:)]) {
        if (connection != nil) {
            [self.delegate ctImageViewLoadDone:self withResult:CTImageViewStateLoadFromUrl];
        } else {
            [self.delegate ctImageViewLoadDone:self withResult:CTImageViewStateLoadFromFileCache];
        }
    }
    // 当图片从缓存文件读取时，connection为nil
    if (connection != nil && self.fileCacheEnabled && self.data != nil && self.data.length != 0 && self.specailName != nil && self.specailName.length != 0) {
        NSLog(@"%d", [self.data writeToFile:[NSString stringWithFormat:@"%@/%@", IMAGE_CACHE_PATH, self.specailName] atomically:YES]);
    }
    
}

@end
