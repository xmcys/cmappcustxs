//
//  CTNavigationController.m
//  ChyoTools
//
//  Created by 陈 宏超 on 13-12-31.
//  Copyright (c) 2013年 Chyo. All rights reserved.
//

#import "CTNavigationController.h"
#import "CTUtil.h"

@interface CTNavigationController ()

@end

@implementation CTNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // 跟改变任何视图状态的代码不能写在这边，至少要写在ViewDidLoad中，不然会出现一些奇怪的现象
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1];;
    // 将导航栏的透明默认设置未NO（IOS7默认YES），占用64px空间
    self.navigationBar.translucent = NO;
    self.view.userInteractionEnabled = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setNavigationBarBackgroundImage:(NSString *)imgName44 imgName64:(NSString *)imgName64
{
    NSAssert(imgName44 != nil, @"imgName44 不能为空");
    UIImage * bg = nil;
    // IOS7以下使用44px导航栏
    if (VERSION_LESS_THAN_IOS7) {
        bg = [UIImage imageNamed:imgName44];
    }
    // IOS7以上使用64px导航栏
    else if (imgName64 != nil) {
        bg = [UIImage imageNamed:imgName64];
    }
    else {
        // IOS7如果状态栏使用44px图片，则导航栏背景会自动置为黑色
        bg = [UIImage imageNamed:imgName44];
    }
    [self.navigationBar setBackgroundImage:bg forBarMetrics:UIBarMetricsDefault];
}

@end
