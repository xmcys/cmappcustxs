//
//  CTTabBar.m
//  ChyoTools
//
//  Created by 陈 宏超 on 14-1-20.
//  Copyright (c) 2014年 Chyo. All rights reserved.
//

#import "CTTabBar.h"

@interface CTTabBarItem ()

@property (nonatomic, strong) UIImageView * imageView;

@end

@implementation CTTabBarItem

- (id)init
{
    if (self = [super init]) {
        self.imageView = [[UIImageView alloc] init];
        [self addSubview:self.imageView];
    }
    return self;
}

- (void)setImage:(UIImage *)image
{
    _image = image;
    self.imageView.image = image;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    if (frame.size.width > self.image.size.width) {
        self.imageView.frame = CGRectMake(0, 0, self.image.size.width, self.image.size.height);
    } else {
        self.imageView.frame = self.bounds;
    }
    self.imageView.center = CGPointMake(frame.size.width / 2, frame.size.height / 2);
}

@end

@interface CTTabBar ()

@property (nonatomic, strong) NSMutableArray * mTabBarItems;
@property (nonatomic, strong) NSMutableArray * mControllers;
@property (nonatomic, weak) UIViewController * mRootViewController;

- (void)setSelectedIndex:(NSUInteger)index;

@end

@implementation CTTabBar

- (id)init
{
    NSAssert(1 != 1, @"[CTTabBar - init] use initWithFrame:andRootViewController instead");
    return nil;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    NSAssert(1 != 1, @"[CTTabBar - initWithCoder] use initWithFrame:andRootViewController instead");
    return nil;
}

- (id)initWithFrame:(CGRect)frame
{
    NSAssert(1 != 1, @"[CTTabBar - initWithFrame] use initWithFrame:andRootViewController instead");
    return nil;
}

- (id)initWithFrame:(CGRect)frame andRootViewController:(UIViewController *)rootViewController
{
    self = [super initWithFrame:frame];
    if (self) {
        self.mControllers = [[NSMutableArray alloc] init];
        self.mTabBarItems = [[NSMutableArray alloc] init];
        self.mRootViewController = rootViewController;
    }
    return self;
}

//- (void)drawRect:(CGRect)rect
//{
//
//}

- (NSArray *)controllers
{
    return self.mControllers;
}

- (NSArray *)tabBarItems
{
    return self.mTabBarItems;
}

- (UIViewController *)rootViewController
{
    return self.mRootViewController;
}

- (void)ctTabBarItemDidClicked:(CTTabBarItem *)item
{
    [self setSelectedIndex:[self.mTabBarItems indexOfObject:item]];
    
    if ([self.delegate respondsToSelector:@selector(ctTabBar:didSelectedAtIndex:)]) {
        [self.delegate ctTabBar:self didSelectedAtIndex:[self.mTabBarItems indexOfObject:item]];
    }
}

- (void)addTabBarItem:(CTTabBarItem *)tabBarItem withController:(UIViewController *)controller
{
    [tabBarItem addTarget:self action:@selector(ctTabBarItemDidClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.mTabBarItems addObject:tabBarItem];
    [self addSubview:tabBarItem];
    int width = self.frame.size.width / self.mTabBarItems.count;
    for (int i = 0; i < self.mTabBarItems.count; i++) {
        CTTabBarItem * item = [self.mTabBarItems objectAtIndex:i];
        item.frame = CGRectMake(i * width, 0, width, self.bounds.size.height);
    }
    [self.mControllers addObject:controller];
    [self setSelectedIndex:0];
}

- (void)setSelectedIndex:(NSUInteger)index
{
    for (CTTabBarItem * item in self.mTabBarItems) {
        item.imageView.image = item.image;
    }
    CTTabBarItem * item = [self.mTabBarItems objectAtIndex:index];
    item.imageView.image = item.selectedImage;
    for (UIViewController * controller in self.mControllers) {
        controller.view.hidden = YES;
    }
    
    UIViewController * controller = [self.mControllers objectAtIndex:[self.mTabBarItems indexOfObject:item]];
    if ([self.rootViewController.view.subviews indexOfObject:controller.view] == NSNotFound) {
        [self.rootViewController.view addSubview:controller.view];
    }
    
    controller.view.hidden = NO;
}

@end
