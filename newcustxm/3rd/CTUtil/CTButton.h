//
//  CTButton.h
//  CmAppCustIos
//
//  Created by hsit on 14-11-24.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CTButton : UIButton

@property (nonatomic) NSInteger index;
@property (nonatomic, strong) NSString *specialId;

@end
