//
//  CTBannerView.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-25.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTBannerView.h"

#define AUTO_SCROLL_TNTERVAL 2

@interface CTBannerView () <UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView * bannerScroll;
@property (nonatomic, strong) UIPageControl * pageControl;
@property (nonatomic) int mPage;
@property (nonatomic) int mMaxPage;
@property (nonatomic, strong) NSTimer * timer;

- (void)autoScrolling;

@end

@implementation CTBannerView

- (id)init
{
    NSAssert(1 != 1, @"[CTBannerView - init] use initWithFrame instead");
    return nil;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    NSAssert(1 != 1, @"[CTBannerView - initWithCoder] use initWithFrame instead");
    return nil;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.autoScroll = YES;
        self.backgroundColor = [UIColor clearColor];
        
        self.bannerScroll = [[UIScrollView alloc] initWithFrame:self.bounds];
        self.bannerScroll.backgroundColor = [UIColor clearColor];
        self.bannerScroll.pagingEnabled = YES;
        self.bannerScroll.delegate = self;
        self.bannerScroll.showsHorizontalScrollIndicator = NO;
        self.bannerScroll.showsVerticalScrollIndicator = NO;
        [self addSubview:self.bannerScroll];
        
        self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, frame.size.height - 37, frame.size.width, 37)];
        self.pageControl.backgroundColor = [UIColor clearColor];
        self.pageControl.numberOfPages = 0;
        self.pageControl.currentPage = 0;
        
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 6.0) {
            self.pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:175.0/255.0 green:136.0/255.0 blue:187.0/255.0 alpha:1];
            self.pageControl.pageIndicatorTintColor = [UIColor colorWithRed:229.0/255.0 green:220.0/255.0 blue:235.0/255.0 alpha:1];
        }
        [self addSubview:self.pageControl];
    }
    return self;
}

- (void)setViewArray:(NSMutableArray *)viewArray
{
    _viewArray = viewArray;
    [self.timer invalidate];
    self.mMaxPage = self.viewArray.count;
    self.mPage = 0;
    self.pageControl.numberOfPages = self.viewArray.count;
    self.pageControl.currentPage = 0;
    [self.bannerScroll scrollRectToVisible:self.bannerScroll.bounds animated:YES];
    self.bannerScroll.contentSize = CGSizeMake(self.bannerScroll.frame.size.width * self.viewArray.count, self.bannerScroll.frame.size.height);
    if (self.autoScroll) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:AUTO_SCROLL_TNTERVAL target:self selector:@selector(autoScrolling) userInfo:nil repeats:YES];
    }
    for (UIView * v in self.bannerScroll.subviews) {
        [v removeFromSuperview];
    }
    for (int i = 0; i < self.viewArray.count; i++) {
        UIView * v = [self.viewArray objectAtIndex:i];
        v.frame = CGRectMake(i * self.bannerScroll.frame.size.width, 0, self.bannerScroll.frame.size.width, self.bannerScroll.frame.size.height);
        [self.bannerScroll addSubview:v];
    }
}

- (int)currentPage
{
    return self.mPage;
}

- (void)setCurrentPage:(NSUInteger)page
{
    self.mPage = page;
    if (self.mPage > self.mMaxPage) {
        self.mPage = 0;
    }
    [self.bannerScroll setContentOffset:CGPointMake(self.mPage * self.bannerScroll.frame.size.width, 0) animated:YES];
}

- (void)autoScrolling
{
    if (self.autoScroll) {
        if (self.mPage + 1 > self.viewArray.count) {
            self.mPage = 0;
        }
        [self.bannerScroll setContentOffset:CGPointMake(self.mPage * self.bannerScroll.frame.size.width, 0) animated:(self.mPage == 0? NO : YES)];
        self.mPage++;
    }
}

- (void)invalidate
{
    [self.bannerScroll scrollRectToVisible:self.bannerScroll.bounds animated:YES];
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.timer invalidate];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int offsetX = scrollView.contentOffset.x;
    int index = offsetX / scrollView.frame.size.width;
    self.mPage = index;
    self.pageControl.currentPage = index;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (self.autoScroll) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:AUTO_SCROLL_TNTERVAL target:self selector:@selector(autoScrolling) userInfo:nil repeats:YES];
    }
}

@end
