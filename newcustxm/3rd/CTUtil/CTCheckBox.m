//
//  CTCheckBox.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-21.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTCheckBox.h"

@interface CTCheckBox ()

@property (nonatomic, strong) UILabel * textLabel;
@property (nonatomic, strong) UIImageView * imageView;
@property (nonatomic) BOOL mChecked;
@property (nonatomic) BOOL mTextHidden;

- (void)checkBoxDidClicked;

@end

@implementation CTCheckBox

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.textHidden = YES;
        self.mChecked = NO;
        self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        self.imageView.contentMode = UIViewContentModeCenter;
        [self addSubview:self.imageView];
        [self addTarget:self action:@selector(checkBoxDidClicked) forControlEvents:UIControlEventTouchUpInside];
        self.image = [UIImage imageNamed:@"CheckBoxNormal"];
        self.checkedImage = [UIImage imageNamed:@"CheckBoxChecked"];
        
        self.textLabel = [[UILabel alloc] init];
        self.textLabel.backgroundColor = [UIColor clearColor];
        self.textLabel.font = [UIFont systemFontOfSize:15.0f];
        self.textLabel.textColor = [UIColor blackColor];
        self.textLabel.hidden = YES;
        [self.textLabel sizeToFit];
        [self addSubview:self.textLabel];
    }
    return self;
}

- (void)setImage:(UIImage *)image
{
    _image = image;
    self.imageView.image = image;
}

- (void)setChecked:(BOOL)checked
{
    self.mChecked = checked;
    if (self.mChecked) {
        self.imageView.image = self.checkedImage;
    } else {
        self.imageView.image = self.image;
    }
}

- (BOOL)checked
{
    return self.mChecked;
}

- (void)setText:(NSString *)text
{
    _text = text;
    self.textLabel.text = text;
    self.textLabel.center = CGPointMake(self.imageView.frame.size.width + self.textLabel.frame.size.width / 2 + 5, self.frame.size.height / 2);
}

- (void)setTextHidden:(BOOL)hidden
{
    self.mTextHidden = hidden;
    if (self.hidden) {
        self.imageView.frame = self.frame;
        self.imageView.center = CGPointMake(self.frame.size.width  / 2, self.frame.size.height / 2);
        self.textLabel.hidden = YES;
    } else {
        self.imageView.frame = CGRectMake(0, 0, self.image.size.width, self.image.size.height);
        self.imageView.center = CGPointMake(self.imageView.frame.size.width / 2, self.frame.size.height / 2);
        self.textLabel.hidden = NO;
        self.textLabel.frame = CGRectMake(self.imageView.frame.size.width + 5, 0, self.frame.size.width - self.imageView.frame.size.width - self.imageView.frame.size.width - 5, self.frame.size.height);
    }
}

- (BOOL)textHidden
{
    return self.mTextHidden;
}

- (void)checkBoxDidClicked
{
    if (!self.enabled) {
        return;
    }
    [self setChecked:!self.mChecked];
    if ([self.delegate respondsToSelector:@selector(ctCheckBox:didValueChanged:)]) {
        [self.delegate ctCheckBox:self didValueChanged:self.mChecked];
    }
}
@end
