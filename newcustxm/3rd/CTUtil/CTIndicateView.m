//
//  CTIndicateView.m
//  ChyoTools
//
//  Created by 陈 宏超 on 14-1-9.
//  Copyright (c) 2014年 Chyo. All rights reserved.
//

#import "CTIndicateView.h"
#import <QuartzCore/QuartzCore.h>

#define CTINDICATE_DISMISS_INTERVAL 0.3
#define CTINDICATE_IMG_WIDTH 32
#define CTINDICATE_MIN_WIDTH 84

@interface CTIndicateView ()

@property (nonatomic, strong) UIView * indicateFrame;
@property (nonatomic, strong) UIImageView * img;
@property (nonatomic, strong) UILabel * textLabel; // 提示文本
@property (nonatomic, strong) UIView * cover;

- (void)setDefault;
- (void)resetFrameLayout;
- (void)startAnimating;
- (void)stopAnimating;
- (void)switchState:(CTIndicateState)state;
- (void)viewDidResume;

@end

@implementation CTIndicateView

- (id)init
{
    NSAssert(1 != 1, @"[CTIndicateView-init] 请使用其他实例化方法");
    return nil;
}

- (id)initWithFrame:(CGRect)frame
{
    NSAssert(1 != 1, @"[CTIndicateView-init] 请使用其他实例化方法");
    return nil;
}

 -(id)initWithCoder:(NSCoder *)aDecoder
{
    NSAssert(1 != 1, @"[CTIndicateView-init] 请使用其他实例化方法");
    return nil;
}

- (id)initInView:(UIView *)view
{
    if (self = [super initWithFrame:view.bounds]) {
        self.cover = [[UIView alloc] initWithFrame:view.bounds];
        self.cover.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        [view addSubview:self.cover];
        [view addSubview:self];
        [self setDefault];
    }
    return self;
}

- (void)setDefault
{
    self.backgroundTouchable = YES;
    self.cover.hidden = YES;
    self.showing = NO;
    _currentState = CTIndicateStateLoading;
    self.backgroundColor = [UIColor clearColor];
    
    self.indicateFrame = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CTINDICATE_MIN_WIDTH, CTINDICATE_MIN_WIDTH)];
    self.indicateFrame.layer.cornerRadius = 5.0;
    self.indicateFrame.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    
    self.img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"IndicateWarnning"]];
    self.img.frame = CGRectMake(0, 0, CTINDICATE_IMG_WIDTH, CTINDICATE_IMG_WIDTH);
    [self.indicateFrame addSubview:self.img];
    
    self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    self.textLabel.textColor = [UIColor whiteColor];
    self.textLabel.font = [UIFont boldSystemFontOfSize:12.0f];
    self.textLabel.text = @"";
    self.textLabel.backgroundColor = [UIColor clearColor];
    [self.indicateFrame addSubview:self.textLabel];
    
    [self addSubview:self.indicateFrame];
    
    self.text = @"加载中...";
    
    self.alpha = 0.0;
    
    // 注册应用状态监听
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewDidResume) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)resetFrameLayout
{
    [self.textLabel sizeToFit];
    
    int totalHeight = self.textLabel.frame.size.height + self.img.frame.size.height;
    int totalWidth = self.textLabel.frame.size.width + 16;
    
    int frameWidth = totalWidth > CTINDICATE_MIN_WIDTH ? totalWidth : CTINDICATE_MIN_WIDTH;
    self.frame = CGRectMake(0, 0, frameWidth, CTINDICATE_MIN_WIDTH);
    self.center = CGPointMake(self.superview.frame.size.width / 2, self.superview.frame.size.height / 2 - 20);
    self.indicateFrame.frame = self.bounds;
    
    self.img.center = CGPointMake(self.indicateFrame.frame.size.width / 2, (self.indicateFrame.frame.size.height - totalHeight) / 2 + CTINDICATE_IMG_WIDTH / 2 - self.textLabel.frame.size.height / 3);
    self.textLabel.center = CGPointMake(self.indicateFrame.frame.size.width / 2, (self.indicateFrame.frame.size.height - totalHeight) / 2 + + CTINDICATE_IMG_WIDTH + self.textLabel.frame.size.height / 2);
}

- (void)startAnimating
{
    if (self.img.layer.animationKeys.count == 0) {
        [self.img.layer removeAnimationForKey:@"transform.z"];
        
        CABasicAnimation * animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        animation.duration = 1.0;
        animation.speed = 1.0;
        animation.cumulative = NO;
        animation.toValue = [NSNumber numberWithFloat:M_PI * 2];
        animation.repeatCount = HUGE_VALF;
        
        [self.img.layer addAnimation:animation forKey:@"transform.z"];
    }
}

- (void)setText:(NSString *)text
{
    _text = text;
    self.textLabel.text = _text;
    [self resetFrameLayout];
}

- (void)stopAnimating
{
    [self.img.layer removeAnimationForKey:@"transform.z"];
}

- (void)show
{
    [self showWithState:CTIndicateStateLoading];
}

- (void)showWithState:(CTIndicateState)state
{
    if (self.showing) {
        return;
    }
    [self.superview bringSubviewToFront:self.cover];
    [self.superview bringSubviewToFront:self];
    self.showing = YES;
    self.cover.hidden = self.backgroundTouchable;
    [self switchState:state];
    [UIView animateWithDuration:CTINDICATE_DISMISS_INTERVAL animations:^{
        self.alpha = 1.0;
    }];
}

- (void)hide
{
    [self hideWithSate:self.currentState afterDelay:0];
}

- (void)hideWithSate:(CTIndicateState)state afterDelay:(float)timeInterval
{
    [self switchState:state];
    
    [UIView animateWithDuration:CTINDICATE_DISMISS_INTERVAL delay:timeInterval options:UIViewAnimationOptionAllowAnimatedContent animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL isFinished){
        self.showing = NO;
        self.cover.hidden = YES;
        [self stopAnimating];
    }];
}

- (void)switchState:(CTIndicateState)state
{
    _currentState = state;
    switch (state) {
        case CTIndicateStateLoading:
            self.img.image = [UIImage imageNamed:@"IndicateLoading"];
            [self startAnimating];
            break;
            
        case CTIndicateStateDone:
            self.img.image = [UIImage imageNamed:@"IndicateDone"];
            [self stopAnimating];
            break;
            
        case CTIndicateStateWarning:
            self.img.image = [UIImage imageNamed:@"IndicateWarnning"];
            [self stopAnimating];
            break;
            
        default:
            break;
    }
}

- (void)autoHide:(CTIndicateState)state
{
    [self autoHide:state afterDelay:1.2];
}

- (void)autoHide:(CTIndicateState)state afterDelay:(float)timeInterval
{
    [self.superview bringSubviewToFront:self.cover];
    [self.superview bringSubviewToFront:self];
    [self switchState:state];
    self.alpha = 1.0;
    self.showing = YES;
    [UIView animateWithDuration:CTINDICATE_DISMISS_INTERVAL delay:timeInterval options:UIViewAnimationOptionAllowUserInteraction animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL isFinished){
        self.showing = NO;
        [self stopAnimating];
    }];
}

- (void)setBackgroundTouchable:(BOOL)backgroundTouchable
{
    _backgroundTouchable = backgroundTouchable;
    if (self.showing) {
        self.cover.hidden = _backgroundTouchable;
    } else {
        self.cover.hidden = YES;
    }
}

- (void)viewDidResume
{
    // controller不可见时，动画会停止，且不会自动恢复，所以要做监听设置
    if (self.currentState == CTIndicateStateLoading && self.showing) {
        [self startAnimating];
    }
}

- (void)removeFromSuperview
{
    // 移除监听
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super removeFromSuperview];
}

@end
