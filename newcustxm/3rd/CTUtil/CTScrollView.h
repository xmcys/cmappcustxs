//
//  CTScrollView.h  下拉刷新、拖动加载
//  ChyoTools
//
//  如果无需重写UIScrollView Delegate中的方法，那么只要实现CTScrollView Delegate协议即可。
//  如果重写了UIScrollView Delegate中的方法，那么需要将ctScrollViewDidScroll及ctScrollViewDidEndDragging分别写在相应方法中，如：
//  #pragma mark -
//  #pragma mark UIScrollView Delegate
//  - (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//  {
//      [ctTableView ctScrollViewDidEndDragging];
//  }
//
//  - (void)scrollViewDidScroll:(UIScrollView *)scrollView
//  {
//      [ctTableView ctScrollViewDidScroll];
//  }
//
//  另外，横向滚动模式不建议使用下拉刷新和上拖加载视图
//
//  Created by 陈 宏超 on 14-1-3.
//  Copyright (c) 2014年 Chyo. All rights reserved.
//

#import <UIKit/UIKit.h>

// 视图状态
typedef NS_ENUM(NSInteger, CTScrollViewStatus)
{
    CTScrollViewStatusNormal,    // 0 正常
    CTScrollViewStatusPullDown,  // 1 下拉
    CTScrollViewStatusPullUp,    // 2 上拉
    CTScrollViewStatusLoading,   // 3 加载中
    CTScrollViewStatusAllLoaded  // 4 数据全部加载
};

@class CTScrollView;

@protocol CTScrollViewDelegate <NSObject>

// 将下拉刷新数据的代码写在此方法里，返回YES则视图维持在刷新状态
- (BOOL)ctScrollViewDidPullDownToRefresh:(CTScrollView *)ctScrollView;
// 将上拉加载数据的代码写在此方法里，返回YES则视图维持在加载状态
- (BOOL)ctScrollViewDidPullUpToLoad:(CTScrollView *)ctScrollView;

@end

@interface CTScrollView : UIScrollView

@property (nonatomic) BOOL pullDownViewEnabled; // 设置是否使用下拉刷新视图
@property (nonatomic) BOOL pullUpViewEnabled;      // 设置是否使用上拉加载视图
@property (nonatomic) CTScrollViewStatus status;      // 视图状态
@property (nonatomic) NSInteger bottomInset;    // 底部缩进大小（当内容区大小大于窗体大小时）
@property (nonatomic, weak) id<CTScrollViewDelegate> ctScrollViewDelegate;  // 委托

// 如果您重新定义了UIScrollViewDelegate的方法，请将此方法写在scrollViewDidScroll方法中
- (void)ctScrollViewDidScroll;
// 如果您重新定义了UIScrollViewDelegate的方法，请将此方法写在scrollViewDidEndDragging方法中
- (void)ctScrollViewDidEndDragging;
// 标识数据已全部加载（此时上拖不会再执行相关方法）
- (void)dataAllLoaded;
// 设置最后更新时间 yyyy-MM-dd HH:mm
- (void)setLastestUpdatedTime:(NSString *)time;
// 重置下拉刷新及上拖加载视图
- (void)reload;
// 自动计算内容大小，bottomInset默认缩进10
- (void)contentToFit;
// 清空所有子视图
- (void)removeAllSubViews;

@end
