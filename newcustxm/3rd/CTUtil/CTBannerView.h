//
//  CTBannerView.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-25.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CTBannerView : UIView

@property (nonatomic) BOOL autoScroll;
@property (nonatomic, strong) NSArray * viewArray;

- (int)currentPage;
- (void)setCurrentPage:(NSUInteger)page;
- (void)invalidate;

@end
