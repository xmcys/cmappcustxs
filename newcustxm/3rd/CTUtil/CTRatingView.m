//
//  CTRatingView.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-24.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTRatingView.h"

@interface CTRatingView ()

@property (nonatomic) float imgWidth;
@property (nonatomic) float mRate;

@end


@implementation CTRatingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.maxRate = 5;
        self.perRate = 1;
        self.mRate = 0;
        self.image = [UIImage imageNamed:@"Star"];
        self.highlightedImage = [UIImage imageNamed:@"StarHighlighted"];
        self.ratingViewEnabled = YES;
        [self invalidate];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame image:(NSString *)img highlightedImage:(NSString *)selectimg
{
    self = [super initWithFrame:frame];
    if (self) {
        self.maxRate = 5;
        self.perRate = 1;
        self.mRate = 0;
        self.image = [UIImage imageNamed:img];
        self.highlightedImage = [UIImage imageNamed:selectimg];
        self.ratingViewEnabled = YES;
        [self invalidate];
    }
    return self;
}

- (void)invalidate
{
    if (self.perRate <= 0) {
        self.perRate = 0;
    }
    if (self.maxRate < 0) {
        self.maxRate = 0;
    }
    int imgCount = self.maxRate / self.perRate;
    if (imgCount < 0) {
        imgCount = 0;
    }
    self.imgWidth = self.frame.size.width / imgCount;
    float imgHeight = self.frame.size.height;
    if (self.imgWidth >= imgHeight) {
        self.imgWidth = imgHeight;
    } else {
        imgHeight = self.imgWidth;
    }
    
    for (UIView * v in self.subviews) {
        [v removeFromSuperview];
    }
    for (int i = 0; i < imgCount; i++) {
        UIImageView * img = [[UIImageView alloc] initWithFrame:CGRectMake(i * self.imgWidth, (self.frame.size.height - imgHeight) / 2, self.imgWidth, imgHeight)];
        img.contentMode = UIViewContentModeCenter;
        if (i < self.mRate) {
            img.image = self.highlightedImage;
        } else {
            img.image = self.image;
        }
        [self addSubview:img];
    }
}


- (void)setRate:(float)rate
{
    self.mRate = rate;
    if (self.mRate < 0) {
        self.mRate = 0;
    }
    if (self.mRate > self.maxRate) {
        self.mRate = self.maxRate;
    }
    [self invalidate];
}

- (float)rate
{
    return self.mRate;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesMoved:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!self.ratingViewEnabled) {
        return;
    }
    CGPoint point = [[touches anyObject] locationInView:self];
    self.mRate = point.x / self.imgWidth;
    if (self.mRate < 0) {
        self.mRate = 0;
    }
    if (self.mRate > self.maxRate) {
        self.mRate = self.maxRate;
    }
    if ([self.delegate respondsToSelector:@selector(ctRatingViewDidRating:withmRate:)]) {
        [self.delegate ctRatingViewDidRating:self withmRate:self.mRate];
    }
    [self invalidate];
}


@end
