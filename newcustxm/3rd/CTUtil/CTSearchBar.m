//
//  CTSearchBar.m
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-21.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTSearchBar.h"

@interface CTSearchBar () <UITextFieldDelegate>

@property (nonatomic, strong) UIButton * btnCancel;

- (void)cancel;

@end

@implementation CTSearchBar

- (id)init
{
    NSAssert(1 != 1, @"[CTSearchBar - init] use initWithFrame instead");
    return nil;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    NSAssert(1 != 1, @"[CTSearchBar - init] use initWithFrame instead");
    return nil;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor lightGrayColor];
        
        self.inputBoxBg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 6, self.frame.size.width - 20, self.frame.size.height - 12)];
        self.inputBoxBg.image = [[UIImage imageNamed:@"InputBoxBg"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 5.5, 14, 5.5)];
        [self addSubview:self.inputBoxBg];
        
        UIImageView * searchIcon = [[UIImageView alloc] init];
        searchIcon.image = [UIImage imageNamed:@"SearchIcon"];
        [searchIcon sizeToFit];
        searchIcon.center = CGPointMake(self.inputBoxBg.frame.origin.x + 10 + searchIcon.frame.size.width / 2, (self.inputBoxBg.frame.size.height - searchIcon.frame.size.height) / 2 + self.inputBoxBg.frame.origin.y + searchIcon.frame.size.height / 2);
        [self addSubview:searchIcon];
        
        self.textField = [[UITextField alloc] initWithFrame:CGRectMake(searchIcon.frame.size.width + searchIcon.frame.origin.x + 10, self.inputBoxBg.frame.origin.y, self.inputBoxBg.frame.size.width - searchIcon.frame.size.width - searchIcon.frame.origin.x - 20, self.inputBoxBg.frame.size.height)];
        self.textField.font = [UIFont systemFontOfSize:14.0f];
        self.textField.placeholder = @"请输入卷烟名称关键字";
        self.textField.returnKeyType = UIReturnKeySearch;
        self.textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.textField.textColor = [UIColor colorWithRed:88.0/255.0 green:88.0/255.0 blue:88.0/255.0 alpha:1];
        self.textField.text = @"";
        self.textField.delegate = self;
        [self addSubview:self.textField];
        
        self.btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btnCancel.titleLabel.font = [UIFont systemFontOfSize:16.0f];
        [self.btnCancel setTitle:@"  取消  " forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"  取消  " forState:UIControlStateHighlighted];
        self.btnCancel.backgroundColor = [UIColor clearColor];
        [self.btnCancel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.btnCancel setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        [self.btnCancel addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
        [self.btnCancel sizeToFit];
        self.btnCancel.center = CGPointMake(self.inputBoxBg.frame.origin.x + self.inputBoxBg.frame.size.width - self.btnCancel.frame.size.width / 2, self.frame.size.height / 2);
        [self addSubview:self.btnCancel];
        self.btnCancel.hidden = YES;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)cancel
{
    self.btnCancel.hidden = YES;
    [self.textField resignFirstResponder];
}

#pragma mark -
#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    self.btnCancel.hidden = YES;
    [self.textField resignFirstResponder];
    if ([self.delegate respondsToSelector:@selector(ctSearchBar:didSearchWithContent:)]) {
        [self.delegate ctSearchBar:self didSearchWithContent:self.textField.text];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.btnCancel.hidden = NO;
}

@end
