//
//  CTScrollView.m
//  ChyoTools
//
//  Created by 陈 宏超 on 14-1-3.
//  Copyright (c) 2014年 Chyo. All rights reserved.
//

#import "CTScrollView.h"
#import <QuartzCore/QuartzCore.h>

#define CTSVHFView_HEIGHT 50
#define EXCLUDE_VIEW_TAG -9

// 声明一个Header和Footer状态视图
@interface CTSVHFView : UIView

@property (nonatomic, strong) UILabel * title;
@property (nonatomic, strong) UILabel * lastUpdatedTime;
@property (nonatomic, strong) UIImageView * arrow;
@property (nonatomic, strong) UIActivityIndicatorView * indicator;
@property (nonatomic) int status;

@end

@implementation CTSVHFView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.tag = EXCLUDE_VIEW_TAG;
        self.backgroundColor = [UIColor clearColor];
        
        // indicator默认为20x20大小
        self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.indicator.center = CGPointMake(CTSVHFView_HEIGHT, CTSVHFView_HEIGHT / 2);
        self.indicator.hidesWhenStopped = YES;
        self.indicator.tag = EXCLUDE_VIEW_TAG;
        [self addSubview:self.indicator];
        
        // 初始化箭头
        self.arrow = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 40)];
        self.arrow.center = CGPointMake(CTSVHFView_HEIGHT, CTSVHFView_HEIGHT / 2);
        self.arrow.image = [UIImage imageNamed:@"CTPullRefreshArrow"];
        self.arrow.tag = EXCLUDE_VIEW_TAG;
        [self addSubview:self.arrow];
        
        // 初始化标题
        self.title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 160, 18)];
        self.title.center = CGPointMake(self.frame.size.width / 2, CTSVHFView_HEIGHT / 2 - 9);
        self.title.font = [UIFont boldSystemFontOfSize:12.0f];
        self.title.textColor = [UIColor darkGrayColor];
        self.title.backgroundColor = [UIColor clearColor];
        self.title.textAlignment = NSTextAlignmentCenter;
        self.title.tag = EXCLUDE_VIEW_TAG;
        [self addSubview:self.title];
        
        // 初始化更新时间
        self.lastUpdatedTime = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 160, 18)];
        self.lastUpdatedTime.center = CGPointMake(self.frame.size.width / 2, CTSVHFView_HEIGHT / 2 + 9);
        self.lastUpdatedTime.font = [UIFont boldSystemFontOfSize:12.0f];
        self.lastUpdatedTime.textColor = [UIColor lightGrayColor];
        self.lastUpdatedTime.backgroundColor = [UIColor clearColor];
        self.lastUpdatedTime.textAlignment = NSTextAlignmentCenter;
        self.lastUpdatedTime.tag = EXCLUDE_VIEW_TAG;
        [self addSubview:self.lastUpdatedTime];
    }
    return  self;
}

@end

@interface CTScrollView () <UIScrollViewDelegate>

@property (nonatomic, strong) CTSVHFView * pullDownView;
@property (nonatomic, strong) CTSVHFView * pullUpView;
@property (nonatomic, strong) NSDateFormatter * dateFmt;

// 设置一些默认值
- (void)setDefault;

@end

@implementation CTScrollView

-(id)init
{
    NSAssert(1 != 1, @"[CTScrollView-init] 请使用其他实例化方法");
    return nil;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self setDefault];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setDefault];
    }
    return self;
}

- (void)setContentSize:(CGSize)contentSize
{
    [super setContentSize:contentSize];
    
    // 动态改变 上拖加载 视图的位置
    self.pullUpView.center = CGPointMake(self.frame.size.width / 2, self.contentSize.height + CTSVHFView_HEIGHT / 2);
}

- (void)drawRect:(CGRect)rect
{
    [self addSubview:self.pullDownView];
    [self addSubview:self.pullUpView];
    
    // IOS6以后，内容区小于窗体区时，无法拖动
    if (self.contentSize.height < self.frame.size.height) {
        self.contentSize = CGSizeMake(self.frame.size.width, self.frame.size.height + 1);
    }
}

- (void)setDefault
{
    self.bounces = YES;
    self.showsHorizontalScrollIndicator = NO;
    self.showsVerticalScrollIndicator = YES;
    self.delegate = self;
    self.bottomInset = 10;
    
    self.dateFmt = [[NSDateFormatter alloc] init];
    [self.dateFmt setDateFormat:@"yyyy-MM-dd hh:mm"];
    
    self.pullDownView = [[CTSVHFView alloc] initWithFrame:CGRectMake(0, -CTSVHFView_HEIGHT, self.frame.size.width, CTSVHFView_HEIGHT)];
    self.pullDownView.title.text = @"下拉可以刷新...";
    self.pullDownView.lastUpdatedTime.text = @"最后更新：从未刷新";
    
    self.pullUpView = [[CTSVHFView alloc] initWithFrame:CGRectMake(0, self.contentSize.height, self.frame.size.width, CTSVHFView_HEIGHT)];
    self.pullUpView.title.text = @"更多...";
    self.pullUpView.lastUpdatedTime.text = @"最后加载：从未加载";
    self.pullUpView.arrow.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
    
    self.status = CTScrollViewStatusNormal;

    self.pullDownViewEnabled = NO;
    self.pullUpViewEnabled = NO;
}

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    [super setBackgroundColor:backgroundColor];
}

- (void)ctScrollViewDidScroll
{
    if (self.status == CTScrollViewStatusLoading) {
        return;
    }
    
    CGFloat offsetY = self.contentOffset.y;
    
    // 下拉超过CTSVHFView_HEIGHT
    if (self.pullDownViewEnabled && offsetY < -CTSVHFView_HEIGHT) {
        [UIView animateWithDuration:0.2 animations:^{
            self.pullDownView.title.text = @"松开即可刷新...";
            self.pullDownView.arrow.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            self.pullDownView.status = CTScrollViewStatusPullDown;
        }];
        return;
    }
    
    if (self.pullDownView.status == CTScrollViewStatusPullDown) {
        // 恢复下拉视图
        [UIView animateWithDuration:0.2 animations:^{
            self.pullDownView.title.text = @"下拉可以刷新...";
            self.pullDownView.arrow.layer.transform = CATransform3DMakeRotation(0, 0, 0, 1);
            self.pullDownView.status = CTScrollViewStatusNormal;
        }];
    }
    
    // 如果上拖视图未被启用，或者被隐藏，或者被标识为“已加载全部数据”，则不进行动画
    if (!self.pullUpViewEnabled || self.pullUpView.hidden || self.pullUpView.status == CTScrollViewStatusAllLoaded) {
        return;
    }
    
    int diffY = self.contentSize.height > self.frame.size.height ? (self.contentSize.height - self.frame.size.height) + CTSVHFView_HEIGHT : CTSVHFView_HEIGHT;
    
    if (offsetY > diffY) {
        [UIView animateWithDuration:0.2 animations:^{
            self.pullUpView.title.text = @"松开即可加载...";
            self.pullUpView.arrow.layer.transform = CATransform3DMakeRotation(0, 0, 0, 1);
            self.pullUpView.status = CTScrollViewStatusPullUp;
        }];
        return;
    }
    
    if (self.pullUpView.status == CTScrollViewStatusPullUp) {
        [UIView animateWithDuration:0.2 animations:^{
            self.pullUpView.title.text = @"更多...";
            self.pullUpView.arrow.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            self.pullUpView.status = CTScrollViewStatusNormal;
        }];
    }
    
}

- (void)ctScrollViewDidEndDragging
{
    if (self.status == CTScrollViewStatusLoading) {
        return;
    }
    
    CGFloat offsetY = self.contentOffset.y;
    
    // 如果下拉视图被启用，并且下拉超过CTSVHFView_HEIGHT
    if (self.pullDownViewEnabled && offsetY < -CTSVHFView_HEIGHT) {
        if ([self.ctScrollViewDelegate respondsToSelector:@selector(ctScrollViewDidPullDownToRefresh:)]) {
            if ([self.ctScrollViewDelegate ctScrollViewDidPullDownToRefresh:self]) {
                self.status = CTScrollViewStatusLoading;
                self.pullDownView.status = CTScrollViewStatusLoading;
                self.pullDownView.title.text = @"正在刷新...";
                self.pullDownView.arrow.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
                [self.pullDownView.indicator startAnimating];
                self.pullDownView.arrow.hidden = YES;
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
                [UIView animateWithDuration:0.2 animations:^{
                    self.contentInset = UIEdgeInsetsMake(CTSVHFView_HEIGHT * 1.2, 0, 0, 0);
                }];
            }
        }
        return;
    }
    
    // 如果上拖视图未被启用，或者被隐藏，或者被标识为“已加载全部数据”，则不进行动画
    if (!self.pullUpViewEnabled || self.pullUpView.hidden || self.pullUpView.status == CTScrollViewStatusAllLoaded) {
        return;
    }
    
    // 当Scroll中内容小于1屏时，设置偏差判断为CTSVHFView_HEIGHT，当内容超过1屏时，先扣去1屏的大小再设置偏差判断为剩余大小加CTSVHFView_HEIGHT
    int diffY = self.contentSize.height > self.frame.size.height ? (self.contentSize.height - self.frame.size.height) + CTSVHFView_HEIGHT: CTSVHFView_HEIGHT;
    
    if (offsetY > diffY) {
        if ([self.ctScrollViewDelegate respondsToSelector:@selector(ctScrollViewDidPullUpToLoad:)]) {
            if ([self.ctScrollViewDelegate ctScrollViewDidPullUpToLoad:self]) {
                self.status = CTScrollViewStatusLoading;
                self.pullUpView.status = CTScrollViewStatusLoading;
                self.pullUpView.title.text = @"正在加载...";
                self.pullUpView.arrow.layer.transform = CATransform3DMakeRotation(0, 0, 0, 1);
                [self.pullUpView.indicator startAnimating];
                self.pullUpView.arrow.hidden = YES;
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
                [UIView animateWithDuration:0.2 animations:^{
                    self.contentInset = UIEdgeInsetsMake(0, 0, CTSVHFView_HEIGHT * 1.2, 0);
                }];
            }
        }
        return;
    }
}

- (void)reload
{
    self.status = CTScrollViewStatusNormal;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    // 重置 下拉刷新 视图
    if (self.pullDownView.status != CTScrollViewStatusNormal) {
        self.pullDownView.status = CTScrollViewStatusNormal;
        self.pullDownView.title.text = @"下拉可以刷新...";
        self.pullDownView.lastUpdatedTime.text = [NSString stringWithFormat:@"最后更新：%@", [self.dateFmt stringFromDate:[NSDate date]]];
        self.pullUpView.lastUpdatedTime.text = [NSString stringWithFormat:@"最后加载：%@", [self.dateFmt stringFromDate:[NSDate date]]];
        [self.pullDownView.indicator stopAnimating];
        self.pullDownView.arrow.hidden = NO;
        [UIView animateWithDuration:0.4 animations:^{
            self.pullDownView.arrow.layer.transform = CATransform3DMakeRotation(0, 0, 0, 1);
            self.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }];
    }
    
    // 重置 上拖加载 视图
    if (self.pullUpView.status != CTScrollViewStatusNormal) {
        self.pullUpView.status = CTScrollViewStatusNormal;
        self.pullUpView.title.text = @"更多...";
        self.pullUpView.lastUpdatedTime.text = [NSString stringWithFormat:@"最后加载：%@", [self.dateFmt stringFromDate:[NSDate date]]];
        [self.pullUpView.indicator stopAnimating];
        self.pullUpView.arrow.hidden = NO;
        [UIView animateWithDuration:0.2 animations:^{
            self.pullUpView.arrow.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            self.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }];
    }
    
    [self contentToFit];
}

- (void)setPullDownViewEnabled:(BOOL)pullDownViewEnabled
{
    _pullDownViewEnabled = pullDownViewEnabled;
    if (_pullDownViewEnabled) {
        self.pullDownView.hidden = NO;
    } else {
        self.pullDownView.hidden = YES;
    }
}

- (void)setPullUpViewEnabled:(BOOL)pullUpViewEnabled
{
    _pullUpViewEnabled = pullUpViewEnabled;
    if (_pullUpViewEnabled) {
        self.pullUpView.hidden = NO;
    } else {
        self.pullUpView.hidden = YES;
    }
}

// 标识数据已全部加载（此时上拖不会再执行相关方法）
- (void)dataAllLoaded
{
    self.pullUpView.status = CTScrollViewStatusAllLoaded;
    self.pullUpView.title.text = @"已加载全部数据";
    [self.pullUpView.indicator stopAnimating];
    self.pullUpView.arrow.hidden = YES;
    self.pullUpView.arrow.layer.transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
}

// 设置最后更新时间 yyyy-MM-dd HH:mm
- (void)setLastestUpdatedTime:(NSString *)time
{
    self.pullDownView.lastUpdatedTime.text = [NSString stringWithFormat:@"最后更新：%@", time];
    self.pullUpView.lastUpdatedTime.text = [NSString stringWithFormat:@"最后加载：%@", time];
}

// 自动计算内容大小；
- (void)contentToFit
{
    int cw = self.frame.size.width;
    int ch = self.frame.size.height;
    for (UIView * v in self.subviews) {
        if (v.tag == EXCLUDE_VIEW_TAG || v.alpha == 0 || v.hidden) {
            // 把下拉刷新、上拖加载及不可见的视图排除在外
            continue;
        } else if (v.frame.origin.x + v.frame.size.width > cw) {
            cw = v.frame.origin.x + v.frame.size.width;
        }
        if (v.frame.origin.y + v.frame.size.height > ch){
            ch = v.frame.origin.y + v.frame.size.height;
        }
    }
    if (cw <= self.frame.size.width) {
        cw = self.frame.size.width;
    } else {
        cw += self.bottomInset;
    }
    if (ch <= self.frame.size.height) {
        ch = self.frame.size.height + 1;
    } else {
        ch += self.bottomInset;
    }
    [self setContentSize:CGSizeMake(cw, ch)];
}

- (void)removeAllSubViews
{
    for (UIView * v in self.subviews) {
        if (v.tag != EXCLUDE_VIEW_TAG) {
            [v removeFromSuperview];
        }
    }
    [self reload];
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self ctScrollViewDidEndDragging];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self ctScrollViewDidScroll];
}


@end
