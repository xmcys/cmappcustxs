//
//  CTURLConnection.m
//  CM_APP
//
//  Created by 陈 宏超 on 13-4-1.
//  Copyright (c) 2013年 Xiamen Icss-Haisheng Information Technology Co., Ltd. All rights reserved.
//

#import "CTURLConnection.h"

#define CTURL_TIME_OUT_TERMINAL 60

Class object_getClass(id object);

@interface CTURLConnection()
@property (nonatomic, strong) NSMutableData * data;
@property (nonatomic, strong) NSURLConnection * conn;
@property (nonatomic, weak) Class originalClass;

@end
@implementation CTURLConnection

- (void)dealloc
{
    self.delegate = nil;
    [self.conn cancel];
    self.conn = nil;
}

- (id)initWithGetMethodUrl:(NSString *)url  delegate:(id<CTURLConnectionDelegate>)delegate
{
    self = [super init];
    if (self) {
        self.url = url;
//        self.url = [[url stringByReplacingOccurrencesOfString:@"ussWebservice" withString:@"ussWebservice-test"] stringByReplacingOccurrencesOfString:@"inteOrder" withString:@"inteOrder-test"];
        self.delegate = delegate;
        _originalClass = object_getClass(delegate);
    }
    return self;
}

- (id)initWithPutMethodUrl:(NSString *)url params:(NSString *)params  delegate:(id<CTURLConnectionDelegate>)delegate
{
//    NSLog(@"%@", params);
    self = [super init];
    if (self) {
        self.url = url;
//        self.url = [[url stringByReplacingOccurrencesOfString:@"ussWebservice" withString:@"ussWebservice-test"] stringByReplacingOccurrencesOfString:@"inteOrder" withString:@"inteOrder-test"];
        self.params = params;
        self.delegate = delegate;
        _originalClass = object_getClass(delegate);
    }
    return self;
}

- (void)start
{
//    NSLog(@"%@", self.url);
    if (self.conn == nil && self.params == nil) {
        NSURL * mUrl = [[NSURL alloc] initWithString:self.url];
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:mUrl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:CTURL_TIME_OUT_TERMINAL];
        NSURLConnection * conn = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
        self.conn = conn;
    } else if (self.conn == nil) {
        NSURL * mUrl = [[NSURL alloc] initWithString:self.url];
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:mUrl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:CTURL_TIME_OUT_TERMINAL];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:[self.params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES]];
        NSURLConnection * conn = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
        self.conn = conn;
    }
    
    if (self.data == nil) {
        NSMutableData * data = [[NSMutableData alloc] init];
        self.data = data;
    }
    [self.conn start];
}

- (void)cancel
{
    self.delegate = nil;
    [self.conn cancel];
    self.conn = nil;
}

- (void)setConn:(NSURLConnection *)conn
{
    if (_conn != nil) {
        [self setDelegate:nil];
    }
    if (conn == nil) {
        [self setDelegate:nil];
    }
    _conn = conn;
}

#pragma mark -
#pragma mark NSURLConnection Delegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
//    NSLog(@"%@", [error description]);
    Class currentClass = object_getClass(_delegate);
    if (currentClass == _originalClass) {
        if (_delegate != nil && [_delegate respondsToSelector:@selector(connection:didFailWithError:)]) {
            [_delegate connection:self didFailWithError:error];
        } else {
//            NSLog(@"delegate release");
        }
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse * httpURLResponse = (NSHTTPURLResponse *) response;
    if((httpURLResponse.statusCode / 100) != 2){
//        NSLog(@"网络响应异常: %@", [[httpURLResponse allHeaderFields] description]);
        Class currentClass = object_getClass(_delegate);
        if (currentClass == _originalClass) {
            if (_delegate != nil && [_delegate respondsToSelector:@selector(connection:didFailWithError:)]) {
                [_delegate connection:self didFailWithError:nil];
            }
        } else {
//            NSLog(@"delegate release");
        }
        
        [self.conn cancel];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.data appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
//    NSLog(@"%@", [[NSString alloc] initWithData:self.data encoding:NSUTF8StringEncoding]);
    Class currentClass = object_getClass(_delegate);
    if (currentClass == _originalClass) {
        if (_delegate != nil && [_delegate respondsToSelector:@selector(connection:didFinishLoading:)]) {
            [_delegate connection:self didFinishLoading:self.data];
        } else {
//            NSLog(@"delegate release");
        }
    }
     
}
@end
