//
//  CTRatingView.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-24.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CTRatingView;

@protocol CTRatingViewDelegate <NSObject>

@optional
// 手指触摸时
- (void)ctRatingViewDidRating:(CTRatingView *)ratingView;
- (void)ctRatingViewDidRating:(CTRatingView *)ratingView withmRate:(float)mRate;
@end


@interface CTRatingView : UIControl

@property (nonatomic) float maxRate; // 最大分值
@property (nonatomic) float perRate; // 每个图片的分值
@property (nonatomic, strong) UIImage * image;  // 正常图
@property (nonatomic, strong) UIImage * highlightedImage;  // 高亮图
@property (nonatomic) BOOL ratingViewEnabled;  // 标识是否可用
@property (nonatomic, weak) id<CTRatingViewDelegate> delegate;

// 设置当前分数，会自动刷新视图
- (void)setRate:(float)rate;
- (float)rate;
// 自定义图片
- (id)initWithFrame:(CGRect)frame image:(NSString *)img highlightedImage:(NSString *)selectimg;

// 更新视图
- (void)invalidate;

@end
