//
//  LNNotificationsAppSettingsController.h
//  LNNotificationsUI
//
//  Created by Leo Natan on 9/19/14.
//  Copyright (c) 2014 Leo Natan. All rights reserved.
//

@interface LNNotificationsAppSettingsController : UITableViewController

- (instancetype)initWithAppIdentifier:(NSString*)identifier;

@end
