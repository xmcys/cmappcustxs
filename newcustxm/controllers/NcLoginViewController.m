//
//  ViewController.m
//  newcustxm
//
//  Created by chyo on 16/11/8.
//  Copyright © 2016年 chyo. All rights reserved.
//

#import "NcLoginViewController.h"
#import "HsWebViewController.h"
#import "AppDelegate.h"
#import "ZbSaveManager.h"

@interface NcLoginViewController ()

@property (nonatomic, strong) IBOutlet UITextField *userCode;
@property (nonatomic, strong) IBOutletCollection(UIView) NSArray *circleView;
@property (nonatomic, strong) IBOutlet UITextField *pwd;
@property (nonatomic, strong) IBOutlet UIButton *btnSignIn;

// 最新数据版本字典
@property (nonatomic, strong) NSMutableDictionary *curVersionDict;
// 请求的详情http返回数据字典
@property (nonatomic, strong) NSMutableDictionary *requestResultDict;
// 下载图片列表
@property (nonatomic, strong) NSMutableArray *imageArray;


@end

@implementation NcLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.curVersionDict = [NSMutableDictionary dictionary];
    self.requestResultDict = [NSMutableDictionary dictionary];
    self.imageArray = [NSMutableArray array];
    
    //圆角
    for (UIView *cview in self.circleView) {
        cview.layer.cornerRadius = 3.0f;
        cview.layer.masksToBounds = YES;
        cview.layer.borderWidth = 1;
        cview.layer.borderColor = [UIColor colorWithRed:101.0/255.0 green:101.0/255.0 blue:101.0/255.0 alpha:0.5].CGColor;
    }
    
    self.btnSignIn.layer.cornerRadius = 3.0f;
    self.btnSignIn.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    // 已保存的用户名
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *acc = [userDefaults valueForKey:SAVE_KEY_ACCOUNT];
    if (acc && acc.length > 0) {
        self.userCode.text = acc;
    }
    
    // 自动登录
    NSString *psw = [userDefaults valueForKey:SAVE_KEY_PSW];
    if (acc && acc.length > 0 && psw && psw.length > 0) {
        self.pwd.text = psw;
        self.hud.labelText = @"自动登录中..";
        [self.hud show:YES];
        [self requestLogin];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // 恢复导航栏显示
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

#pragma mark - 接口调用
// 请求数据版本号
- (void)requestVersion {
    self.hud.labelText = @"登录中..";
    [self.hud show:NO];
    
    [self.requestResultDict removeAllObjects];
    [self.curVersionDict removeAllObjects];
    __weak typeof(self) weakSelf = self;
    NSDictionary *parameters = @{};
    [HttpRequest getWithURLString:[NcWebSerVice getDataVersions:APP_ID] parameters:parameters success:^(id responseObject) {
        NSString *code = responseObject[@"code"];
        if (code.integerValue == 1) {
            // 获取成功
            NSArray *versionArray = responseObject[@"value"];
            // 新版本数据
            for (NSDictionary *curDict in versionArray) {
                NSString *type = curDict[@"PAGES_TYPE"];
                [weakSelf.curVersionDict setValue:curDict[@"VERSION_NO"] forKey:type];
            }
            
            // 取旧版本数据
            NSDictionary *lastVersionDict = [[ZbSaveManager shareManager] getVersionDict];
            
            BOOL isAllNew = YES;
            // 广告数据版本
            NSString *lastAdVer = lastVersionDict[PAGE_TYPE_AD];
            NSString *curAdVer = weakSelf.curVersionDict[PAGE_TYPE_AD];
            // 引导页版本
            NSString *lastGuideVer = lastVersionDict[PAGE_TYPE_GUIDE];
            NSString *curGuideVer = weakSelf.curVersionDict[PAGE_TYPE_GUIDE];
            
            BOOL isAdOld = curAdVer && ![curAdVer isEqualToString:lastAdVer];
            BOOL isGuideOld = curGuideVer && ![curGuideVer isEqualToString:lastGuideVer];
            if (isAdOld || isGuideOld) {
                isAllNew = NO;
            }
            
            // 所有数据都是最新的
            if (isAllNew) {
                [weakSelf checkImages];
                return;
            }
            
            if (isAdOld) {
                // 广告页版本不同，获取新数据
                [weakSelf requestPageInfo:PAGE_TYPE_AD versionNo:curAdVer successBlock:^(void) {
                    if (isGuideOld) {
                        // 继续请求引导页数据
                        [weakSelf requestPageInfo:PAGE_TYPE_GUIDE versionNo:curGuideVer successBlock:^(void) {
                            [weakSelf savePageInfo];
                        }];
                    } else {
                        [weakSelf savePageInfo];
                    }
                }];
                
            } else {
                // 只有引导页版本不同，获取新数据
                [weakSelf requestPageInfo:PAGE_TYPE_GUIDE versionNo:curGuideVer successBlock:^(void) {
                    [weakSelf savePageInfo];
                }];
            }
            
        } else {
            weakSelf.hud.labelText = @"登录失败，请稍后重试";
            [weakSelf.hud autoHideWithType:Zb_Indicator_type_None delay:1.0f complete:nil];
        }
        
    } failure:^(NSError *error) {
        weakSelf.hud.labelText = @"登录失败，请稍后重试";
        [weakSelf.hud autoHideWithType:Zb_Indicator_type_None delay:1.0f complete:nil];
    }];
}

// 请求页面内容
- (void)requestPageInfo:(NSString *)pageType versionNo:(NSString *)versionNo successBlock:(void (^)(void))block {
    __weak typeof(self) weakSelf = self;
    NSDictionary *parameters = @{};
    // 页面内容接口
    [HttpRequest getWithURLString:[NcWebSerVice getAppPages:APP_ID type:pageType versionNo:versionNo] parameters:parameters success:^(id responseObject) {
        NSString *code = responseObject[@"code"];
        if (code.integerValue == 1) {
            NSArray *array = responseObject[@"value"];
            [weakSelf.requestResultDict setObject:array forKey:pageType];
            
            block();
        } else {
            weakSelf.hud.labelText = @"登录失败，请稍后重试";
            [weakSelf.hud autoHideWithType:Zb_Indicator_type_None delay:1.0f complete:nil];
        }
        
    } failure:^(NSError *error) {
        weakSelf.hud.labelText = @"登录失败，请稍后重试";
        [weakSelf.hud autoHideWithType:Zb_Indicator_type_None delay:1.0f complete:nil];
    }];
    return;
}

// 页面数据保存
- (void)savePageInfo {
    // 保存最新数据版本
    [[ZbSaveManager shareManager] setVersionDict:self.curVersionDict];
    // 保存广告数据
    NSArray *adArray = self.requestResultDict[PAGE_TYPE_AD];
    if (adArray != nil) {
        [[ZbSaveManager shareManager] setAdArray:adArray];
    }
    // 保存引导页数据
    NSArray *guideArray = self.requestResultDict[PAGE_TYPE_GUIDE];
    if (guideArray != nil) {
        [[ZbSaveManager shareManager] setGuideArray:guideArray];
        
        // 重置引导页为未播放
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        [userDefault setValue:nil forKey:GUIDE_IS_SEE];
        [userDefault synchronize];
    }
    // 下载新资源
    [self checkImages];
    return;
}

// 登录
- (void)requestLogin {
    
    __weak typeof(self) weakSelf = self;
    // 先检查app版本号
    [HttpRequest getWithURLString:[NcWebSerVice getLatestAppVersionInfo] parameters:nil success:^(id responseObject) {
        BOOL result = [responseObject[@"successful"] boolValue];
        if (!result) {
            weakSelf.hud.labelText = @"登录失败，请稍后重试";
            [weakSelf.hud autoHideWithType:Zb_Indicator_type_None delay:1.0f complete:nil];
            return;
        }
        // 本地版本号
        NSDictionary *appInfo = [[NSBundle mainBundle] infoDictionary];
        NSString *curBuild = [appInfo objectForKey:@"CFBundleVersion"];
        
        // 更新信息
        NSDictionary *infoDict = responseObject[@"data"];
        NSString *tagBuild = infoDict[@"BUILD_CODE"];
        BOOL isSumitting = (curBuild.integerValue > tagBuild.integerValue);
        [ZbSaveManager setIsSumitting:isSumitting];
        
        NSDictionary *parameters = @{@"custLicenceCode":weakSelf.userCode.text, @"userPwd":weakSelf.pwd.text};
        [HttpRequest postWithURLString:[NcWebSerVice verifyRetailerLogin] parameters:parameters success:^(id responseObject) {
            NSString *result = responseObject[@"successful"];
            if (result.boolValue) {
                NSMutableDictionary *userDict = responseObject[@"data"];
                [weakSelf requestLoginSuccess:userDict];
                
            } else {
                weakSelf.hud.labelText = responseObject[@"msg"];
                [weakSelf.hud autoHideWithType:Zb_Indicator_type_None delay:1.0f complete:^{
                }];
            }
            
        } failure:^(NSError *error) {
            weakSelf.hud.labelText = @"登录失败，请稍后重试";
            [weakSelf.hud autoHideWithType:Zb_Indicator_type_None delay:1.0f complete:nil];
        }];
        return;
        
    } failure:^(NSError *error) {
        self.hud.labelText = @"登录失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_None delay:1.0f complete:nil];
        return;
    }];
}

// 登录接口调用成功处理逻辑
- (void)requestLoginSuccess:(NSMutableDictionary *)userDict {
    // 保存用户数据
    [NcWebSerVice setUser:userDict];
    // 友盟推送设置别名，进行单个推送
    [UMessage setAlias:userDict[@"USER_ID"] type:kUMessageAliasTypeSina response:^(id responseObject, NSError *error) {
        DLog(@"%@ - %@", responseObject, error);
    }];
    
    [self requestVersion];
}

#pragma mark - 点击事件
// 界面点击
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

//登陆
- (IBAction)loginOnClick:(UIButton *)sender {
    [self.view endEditing:YES];
    if (self.userCode.text.length <= 0) {
        self.hud.labelText = @"请输入账号";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    if (self.pwd.text.length <= 0) {
        self.hud.labelText = @"请输入密码";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        return;
    }
    
    self.hud.labelText = @"登录中..";
    [self.hud show:YES];
    [self requestLogin];
    return;
}

// 用户协议
- (IBAction)agreementOnClick:(id)sender {
    HsWebViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    controller.url = [NcWebSerVice getProtocol];
    controller.isShowNav = YES;
    [self.navigationController pushViewController:controller animated:YES];
}

// 忘记密码
- (IBAction)forgetPwd:(id)sender {
    HsWebViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    controller.url = [NcWebSerVice getForgetPwdUrl];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - 下载图片
// 检查所需下载图片
- (void)checkImages {
    [self.imageArray removeAllObjects];
    // 获取需下载的图片列表
    NSArray *adArray = [[ZbSaveManager shareManager] getAdArray];
    NSArray *guideArray = [[ZbSaveManager shareManager] getGuideArray];
    for (NSDictionary *curDict in adArray) {
        [self.imageArray addObject:curDict[@"IMAGE_URL"]];
    }
    for (NSDictionary *curDict in guideArray) {
        [self.imageArray addObject:curDict[@"IMAGE_URL"]];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.02 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // 下载图片
        [self downloadImage];
    });
}

// 下载图片
- (void)downloadImage {
    if (self.imageArray.count <= 0) {
        // 保存用户名
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setValue:self.userCode.text forKey:SAVE_KEY_ACCOUNT];
        [userDefaults setValue:self.pwd.text forKey:SAVE_KEY_PSW];
        [userDefaults synchronize];
        
        self.hud.labelText = @"登录成功";
        [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0f complete:^(void) {
            //  进入主界面
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate loginSuccess];
        }];
        return;
    }
    
    NSString *imageUrl = [self.imageArray lastObject];
//        DLog(@"正在下载：%@", imageUrl);
    BOOL isExists = [[SDWebImageManager sharedManager] cachedImageExistsForURL:[NSURL URLWithString:imageUrl]];
    if (isExists) {
//        DLog(@"图片已存在");
        [self.imageArray removeLastObject];
        [self performSelector:@selector(downloadImage) withObject:self];
        return;
    }
    
    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:imageUrl] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
//        DLog(@"下载结束，是否成功=%d", (error == nil));
        [self.imageArray removeLastObject];
        [self performSelector:@selector(downloadImage) withObject:self];
    }];
}

@end
