//
//  NcJyViewController.m
//  newcustxm
//
//  Created by 张斌 on 16/12/19.
//  Copyright © 2016年 chyo. All rights reserved.
//

#import "NcJyViewController.h"
#import "HsWebViewController.h"
#import "UIButton+Badge.h"

@interface NcJyViewController ()

// 红点
@property (nonatomic, strong) IBOutlet UIButton *btnBadgeWaitPay;
@property (nonatomic, strong) IBOutlet UIButton *btnBadgeWaitDelivery;
@property (nonatomic, strong) IBOutlet UIButton *btnBadgeWaitComment;
@property (nonatomic, strong) IBOutlet UIButton *btnBadgeWaitTuikuan;
@property (nonatomic, strong) IBOutlet UIButton *btnBadgeWaitRecv;

@end

@implementation NcJyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // 加载红点信息
    [self loadOrderStatusNumInfo];
    
    // 恢复导航栏显示
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

#pragma mark - 接口调用
// 查询订单状态数量
- (void)loadOrderStatusNumInfo {
    [HttpRequest getWithURLString:[NcWebSerVice getStautsNumUrl] parameters:nil
                          success:^(id responseObject) {
                              
                              NSString *result = responseObject[@"successful"];
                              if (result.boolValue) {
                                  NSDictionary *data = responseObject[@"data"];
                                  if (data && ![data isKindOfClass:[NSNull class]]) {
                                      NSNumber *num1 = data[@"num1"];
                                      NSNumber *num2 = data[@"num2"];
                                      NSNumber *num3 = data[@"num3"];
                                      NSNumber *num4 = data[@"num4"];
                                      NSNumber *num5 = data[@"num5"];
                                      if (num1 && ![num1 isKindOfClass:[NSNull class]]) {
                                          NSString *num1Str = [NSString stringWithFormat:@"%@", num1];
                                          num1Str = [num1Str isEqualToString:@"0"] ? @"":num1Str;
                                          self.btnBadgeWaitPay.badgeValue = num1Str;
                                      }
                                      if (num2 && ![num2 isKindOfClass:[NSNull class]]) {
                                          NSString *num2Str = [NSString stringWithFormat:@"%@", num2];
                                          num2Str = [num2Str isEqualToString:@"0"] ? @"":num2Str;
                                          self.btnBadgeWaitRecv.badgeValue = num2Str;
                                      }
                                      if (num3 && ![num3 isKindOfClass:[NSNull class]]) {
                                          NSString *num3Str = [NSString stringWithFormat:@"%@", num3];
                                          num3Str = [num3Str isEqualToString:@"0"] ? @"":num3Str;
                                          self.btnBadgeWaitComment.badgeValue = num3Str;
                                      }
                                      if (num4 && ![num4 isKindOfClass:[NSNull class]]) {
                                          NSString *num4Str = [NSString stringWithFormat:@"%@", num4];
                                          num4Str = [num4Str isEqualToString:@"0"] ? @"":num4Str;
                                          self.btnBadgeWaitTuikuan.badgeValue = num4Str;
                                      }
                                      if (num5 && ![num5 isKindOfClass:[NSNull class]]) {
                                          NSString *num5Str = [NSString stringWithFormat:@"%@", num5];
                                          num5Str = [num5Str isEqualToString:@"0"] ? @"":num5Str;
                                          self.btnBadgeWaitDelivery.badgeValue = num5Str;
                                      }
                                  }
                              }
                          } failure:^(NSError *error) {
                          }];
    return;
}

#pragma mark - 点击事件
// 待付款点击
- (IBAction)btnWaitPayOnClick:(id)sender {
    HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    web.url = [NcWebSerVice getNotPaidOrderUrl];
    [self.navigationController pushViewController:web animated:YES];
    return;
}

// 待送货点击
- (IBAction)btnWaitDeliveryOnClick:(id)sender {
    HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    web.url = [NcWebSerVice getDfhPaidOrder];
    [self.navigationController pushViewController:web animated:YES];
    return;
}

// 待收货点击
- (IBAction)btnWaitRectOnClick:(id)sender {
    HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    web.url = [NcWebSerVice getNotReceivingOrderUrl];
    [self.navigationController pushViewController:web animated:YES];
    return;
}

// 待评价点击
- (IBAction)btnWaitCommentOnClick:(id)sender {
    HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    web.url = [NcWebSerVice getNotAppraiseOrderUrl];
    [self.navigationController pushViewController:web animated:YES];
    return;
}

// 待退款点击
- (IBAction)btnWaitTuikuanOnClick:(id)sender {
    HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    web.url = [NcWebSerVice getMerchantNotRefundOrderUrl];
    [self.navigationController pushViewController:web animated:YES];
    return;
}

// 全部订单点击
- (IBAction)btnAllOnClick:(id)sender {
    HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    web.url = [NcWebSerVice getAllOrderUrl];
    [self.navigationController pushViewController:web animated:YES];
    return;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
