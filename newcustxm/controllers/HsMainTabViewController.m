//
//  HsMainTabViewController.m
//  newcustxm
//
//  Created by 张斌 on 16/12/15.
//  Copyright © 2016年 chyo. All rights reserved.
//

#import "HsMainTabViewController.h"
#import "AppDelegate.h"
#import "ZbSaveManager.h"
#import "HsWebViewController.h"
#import "HsGuideViewController.h"

@interface HsMainTabViewController () <UITabBarControllerDelegate>

@end

@implementation HsMainTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.delegate = self;
    
    // 加载引导页面
    NSArray *guideArray = [[ZbSaveManager shareManager] getGuideArray];
    // 有数据
    if (guideArray != nil && guideArray.count > 0) {
        
        // 是否播放过引导页
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSNumber *guideSeen = [userDefault valueForKey:GUIDE_IS_SEE];
        if (guideSeen == nil) {
            // 播放引导页
            HsGuideViewController *guide = [self.storyboard instantiateViewControllerWithIdentifier:@"HsGuideViewController"];
            [self.navigationController presentViewController:guide animated:YES completion:nil];
            
            // 引导页已播放
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setValue:@1 forKey:GUIDE_IS_SEE];
            [userDefault synchronize];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // 显示点击的闪屏广告内容
    NSDictionary *adDict = [ZbSaveManager getClickAdDict];
    
    NSDictionary *userInfo = [ZbSaveManager getPushUserInfo];
    if (userInfo) {
        // 优先展示点击的推送
        [ZbSaveManager setPushUserInfo:nil];
        [ZbSaveManager setClickAdDict:nil];
        AppDelegate *applegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [applegate handleRemoteNotification:userInfo];
    } else if (adDict != nil) {
        // 展示点击的广告
        HsWebViewController *controller;
        controller = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
        controller.url = adDict[@"TARGET_URL"];
        controller.isShowNav = YES;
        [self.navigationController pushViewController:controller animated:YES];
        [ZbSaveManager setClickAdDict:nil];
    } else {
        // 查看启动是否有需要打开链接
        NSMutableDictionary *userDict = [NcWebSerVice user];
        NSString *URL = userDict[@"URL"];
        if (URL != nil && URL.length > 0) {
            [userDict removeObjectForKey:@"URL"];
            HsWebViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
            controller.url = URL;
//            controller.isShowNav = YES;
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}

#pragma mark - UITabBarController delegate
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    NSArray *array = tabBarController.viewControllers;
    NSInteger index = [array indexOfObject:viewController];
    switch (index) {
        case 1:
        {
            // 服务tab
            [self.navigationController setNavigationBarHidden:YES animated:NO];
            self.navigationController.navigationBar.translucent = YES;
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
            break;
        }
        case 0:
        {
        }
        case 2:
        {
        }
        case 3:
        {
            [self.navigationController setNavigationBarHidden:NO animated:NO];
            self.navigationController.navigationBar.translucent = NO;
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            break;
        }
        default:
            break;
    }
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
