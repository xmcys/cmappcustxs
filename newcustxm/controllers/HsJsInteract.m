//
//  HsJsInteract.m
//  newconsumerxm
//
//  Created by 张斌 on 17/2/10.
//  Copyright © 2017年 chyo. All rights reserved.
//

#import "HsJsInteract.h"
#import "HsWebViewController.h"
#import "AppDelegate.h"
#import "CTScannerController.h"

#pragma mark - 与js交互的处理类
@interface HsJsInteract ()

@end

@implementation HsJsInteract

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - js delegate
// 返回
- (void)back {
    // 返回通知上一页刷新
    if (self.controller.backBlock) {
        self.controller.backBlock();
    }
    [self.controller.navigationController popViewControllerAnimated:YES];
}

// 显示Toast提醒
- (void)showToast:(NSString *)msg {
    [MBProgressHUD showTextHudInView:self.controller.view withText:msg lastTime:1.0 completeBlock:nil];
    return;
}

// 跳转新页面，返回执行js
- (void)open:(NSString *)targetUlr WebView:(NSString *)jsStr BackJs:(NSInteger)closeCnt {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        HsWebViewController *webVc = weakSelf.controller;
        if ([webVc respondsToSelector:@selector(h5_open:WebView:BackJs:)]) {
            [webVc h5_open:targetUlr WebView:jsStr BackJs:closeCnt];
        }
    });
    return;
}

// 跳转新页面
- (void)open:(NSString *)targetUlr WebView:(NSString *)isRefreshOnBack {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        HsWebViewController *webVc = weakSelf.controller;
        if ([webVc respondsToSelector:@selector(h5_open:WebView:)]) {
            [webVc h5_open:targetUlr WebView:isRefreshOnBack];
        }
    });
    return;
}

// 获取二维码
// 回调getBarCodeResult(code)
- (void)getBarCode {
    __weak typeof(self) weakSelf = self;
    CTScannerController *scanController = [[CTScannerController alloc] initWithNibName:nil bundle:nil];
    scanController.scanResultBlock = ^(NSString *barCode) {
        // 扫描结果
        [weakSelf.controller.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"onBarCodeResult('%@')", barCode]];
        [weakSelf.controller.navigationController popViewControllerAnimated:NO];
    };
    // 跳转扫码
    [self.controller.navigationController pushViewController:scanController animated:YES];
    return;
}

// 拍照
- (void)getImage {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        HsWebViewController *webVc = weakSelf.controller;
        if ([webVc respondsToSelector:@selector(h5_getImage)]) {
            [webVc h5_getImage];
        }
    });
    return;
}

// 退出登录
- (void)logOut {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate logOut];
    return;
}

// 获取定位信息
- (void)getLocation {
    BMKUserLocation *loc = [[LocationServiceManager shareManager] getCurLocation];
    if (loc) {
        NSString *js = [NSString stringWithFormat:@"onGetLocationResult(%@, %@)", [NSNumber numberWithDouble:loc.location.coordinate.longitude], [NSNumber numberWithDouble:loc.location.coordinate.latitude]];
        [self.controller.webView stringByEvaluatingJavaScriptFromString:js];
    } else {
        NSString *js = [NSString stringWithFormat:@"onGetLocationResult(0, 0)"];
        [self.controller.webView stringByEvaluatingJavaScriptFromString:js];
    }
    return;
}

// 显示导航栏
- (void)showNav {
    self.controller.navigationController.navigationBar.translucent = NO;
    [self.controller.navigationController setNavigationBarHidden:NO animated:NO];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    self.controller.isShowNav = YES;
    self.controller.viewColor.hidden = YES;
    self.controller.alcWebViewTop.constant = 0;
    self.controller.title = [self.controller.webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    return;
}

// 注册页面自动登录成功
- (void)login:(NSString *)info {
    // h5传给原生的登录成功信息
    NSError *error;
    NSMutableDictionary *tagDict = [NSJSONSerialization JSONObjectWithData:[info dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    DLog(@"login() %@", tagDict);
    
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        HsWebViewController *webVc = weakSelf.controller;
        if ([webVc respondsToSelector:@selector(h5_login:)]) {
            [webVc h5_login:tagDict];
        }
    });
    return;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
