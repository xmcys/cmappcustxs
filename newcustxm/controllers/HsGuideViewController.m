//
//  HsGuideViewController.m
//  CmAppCustXs
//
//  Created by 张斌 on 15/9/26.
//  Copyright (c) 2015年 chyo. All rights reserved.
//

#import "HsGuideViewController.h"
#import "AppDelegate.h"

@interface HsGuideViewController () <UIScrollViewDelegate>

// 滚动层
@property (nonatomic, strong) IBOutlet UIScrollView *sv;
// 跳过按钮
@property (nonatomic, strong) IBOutlet UIButton *btnJump;
// 进入按钮
@property (nonatomic, strong) IBOutlet UIButton *btnEnter;

// 当前选择页面
@property (assign, nonatomic) NSInteger curPageIndex;
// 总页面数
@property (assign, nonatomic) NSInteger totalPageCount;

@end

@implementation HsGuideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 边框
    self.btnJump.layer.borderWidth = 1;
    self.btnJump.layer.borderColor = UIColorFromHexString(@"E5E5E5").CGColor;
    self.btnEnter.layer.borderWidth = 1;
    self.btnEnter.layer.borderColor = UIColorFromHexString(@"E5E5E5").CGColor;
    
    // 获取引导页数据
    NSArray *guideArray = [[ZbSaveManager shareManager] getGuideArray];
    
    // 创建图片
    self.totalPageCount = guideArray.count;
    for (NSInteger i = 0; i < guideArray.count; i++) {
        UIImageView *ivImage = [[UIImageView alloc] initWithFrame:CGRectMake(i * DeviceWidth, 0, DeviceWidth, DeviceHeight)];
        NSDictionary *curDict = [guideArray objectAtIndex:i];
        [ivImage sd_setImageWithURL:[NSURL URLWithString:curDict[@"IMAGE_URL"]]];
        ivImage.contentMode = UIViewContentModeScaleAspectFill;
        ivImage.clipsToBounds = YES;
        
        [self.sv addSubview:ivImage];
    }
    self.sv.contentSize = CGSizeMake(DeviceWidth * guideArray.count, DeviceHeight);
    
//    // 如果只有一张，x秒后关闭
//    if (guideArray.count == 1) {
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
//            [appDelegate onAdShowOk];
//        });
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 初始化UI时机
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    [self pageChange];
}

#pragma mark - 点击事件
// 跳过按钮点击
- (IBAction)btnJumpOnClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    return;
}

// 进入按钮点击
- (IBAction)btnEnterOnClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    return;
}

#pragma mark - 切页逻辑
- (void)pageChange
{
    if (self.curPageIndex == 0) {
        // 第一页显示跳过
        self.btnJump.hidden = NO;
        self.btnEnter.hidden = YES;
    } else if (self.curPageIndex == (self.totalPageCount - 1)) {
        // 最后一页显示进入
        self.btnJump.hidden = YES;
        self.btnEnter.hidden = NO;
    } else {
        self.btnJump.hidden = YES;
        self.btnEnter.hidden = YES;
    }
    return;
}

# pragma mark - UIScrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offsetX = scrollView.contentOffset.x;
    
    // 判断是否切页
    if (fmod(offsetX, scrollView.frame.size.width)  == 0) {
        int index = offsetX / scrollView.frame.size.width;
        self.curPageIndex = index;
        [self pageChange];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
