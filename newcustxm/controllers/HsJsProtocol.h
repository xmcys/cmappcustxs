//
//  HsJsProtocol.h
//  newconsumerxm
//
//  Created by 张斌 on 17/2/10.
//  Copyright © 2017年 chyo. All rights reserved.
//

#import <Foundation/Foundation.h>

//首先创建一个实现了JSExport协议的协议
@protocol HsJsProtocol <JSExport>

// 返回上一页
- (void)back;
// 显示Toast提醒
- (void)showToast:(NSString *)msg;
// 跳转新页面，返回执行js
- (void)open:(NSString *)targetUlr WebView:(NSString *)jsStr BackJs:(NSInteger)closeCnt;
// 跳转新页面
- (void)open:(NSString *)targetUlr WebView:(NSString *)isRefreshOnBack;
// 获取二维码
// 回调getBarCodeResult(code)
- (void)getBarCode;
// 拍照
- (void)getImage;
// 退出登录
- (void)logOut;
// 获取定位信息
- (void)getLocation;
// 显示导航栏
- (void)showNav;

// 注册页面自动登录成功
- (void)login:(NSString *)info;

@end
