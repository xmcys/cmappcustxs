//
//  NcTabHomeViewController.m
//  newcustxm
//
//  Created by chyo on 16/11/8.
//  Copyright © 2016年 chyo. All rights reserved.
//

#import "NcTabHomeViewController.h"
#import "HsWebViewController.h"
#import "SDCycleScrollView.h"
#import "UIImageView+WebCache.h"

#define TOP_BANNER_HEIGHTRATE 0.608755

@interface NcTabHomeViewController ()<YKuImageInLoopScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, SDCycleScrollViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tv;
@property (nonatomic, strong) IBOutlet UIView *tvHeader;
// 快讯走马灯容器
@property (nonatomic, strong) IBOutlet UIView *viewTextScroll;

// 消息中心按钮
@property (nonatomic, strong) IBOutlet UIBarButtonItem *btnMsg;

// 滚动广告
@property (nonatomic, strong) YKuImageInLoopScrollView *imageLoopView;
// 快讯走马灯容器
@property (nonatomic, strong) IBOutlet UIView *viewKuaixun;
// 快讯走马灯
@property (nonatomic, strong) SDCycleScrollView *textScrollView;
// 子菜单数组
@property (nonatomic, strong) NSMutableArray *array;
// 快讯数组
@property (nonatomic, strong) NSMutableArray *kuaixunArray;
// 新品数组
@property (nonatomic, strong) NSMutableArray *xinpinArray;

@end

@implementation NcTabHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.kuaixunArray = [[NSMutableArray alloc] init];
    self.xinpinArray = [[NSMutableArray alloc] init];
    self.view.backgroundColor = UIColorFromHexString(@"f0f0f0");
    
    self.array = [NSMutableArray new];
    
    [self.navigationController.navigationBar setBackgroundImage:[self imageWithColor:self.navigationController.navigationBar.barTintColor size:CGSizeMake(self.navigationController.navigationBar.frame.size.width, 64)] forBarMetrics:UIBarMetricsDefault];
//    [self.navigationController.navigationBar.layer setMasksToBounds:YES];
    [self.navigationController.navigationBar setShadowImage:[self imageWithColor:[UIColor clearColor] size:CGSizeMake(1, 1)]];
}

- (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size

{
    @autoreleasepool {
        
        CGRect rect = CGRectMake(0, 0, size.width, size.height);
        
        UIGraphicsBeginImageContext(rect.size);
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGContextSetFillColorWithColor(context,
                                       
                                       color.CGColor);
        
        CGContextFillRect(context, rect);
        
        UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        return img;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    CGFloat bannerHeight = DeviceWidth * TOP_BANNER_HEIGHTRATE;
    
    // 设置tableview头部高度
    CGRect tvHeaderFrame = self.tvHeader.frame;
    tvHeaderFrame.size.height = tvHeaderFrame.size.height + bannerHeight;
    self.tvHeader.frame = tvHeaderFrame;
    [self.tv setTableHeaderView:self.tvHeader];
    
    // 滚动广告
    self.imageLoopView = [[YKuImageInLoopScrollView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, bannerHeight)];
    self.imageLoopView.delegate = self;
    // 设置yKuImageInLoopScrollView显示类型
    self.imageLoopView.scrollViewType = ScrollViewDefault;
    // 设置styledPageControl位置
    [self.imageLoopView.styledPageControl setPageControlSite:PageControlSiteMiddle];
    [self.imageLoopView.styledPageControl setBottomDistance:8];
    // 设置styledPageControl已选中下的内心圆颜色
    [self.imageLoopView.styledPageControl setCoreNormalColor:[UIColor colorWithRed:253/255.0 green:253/255.0 blue:253/255.0 alpha:0.5]];
    [self.imageLoopView.styledPageControl setCoreSelectedColor:[UIColor whiteColor]];
    [self.tvHeader addSubview:self.imageLoopView];
    
    // 快讯走马灯
    self.textScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, DeviceWidth - CGRectGetMinX(self.viewTextScroll.frame), CGRectGetHeight(self.viewTextScroll.frame)) delegate:self placeholderImage:nil];
    self.textScrollView.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.textScrollView.onlyDisplayText = YES;
    self.textScrollView.titleLabelTextColor = UIColorFromHexString(@"333333");
    self.textScrollView.titleLabelBackgroundColor = [UIColor clearColor];
    self.textScrollView.titleLabelTextFont = [UIFont systemFontOfSize:15];
    self.textScrollView.titlesGroup = [NSMutableArray new];
    [self.viewTextScroll addSubview:self.textScrollView];
    [self.tv setTableHeaderView:self.tvHeader];
    
    BOOL isSumitting = [ZbSaveManager isSumitting];
    if (isSumitting) {
        self.imageLoopView.hidden = YES;
        self.viewKuaixun.hidden = YES;
        // 设置tableview头部高度
        CGRect tvHeaderFrame = self.tvHeader.frame;
        tvHeaderFrame.size.height = 45;
        self.tvHeader.frame = tvHeaderFrame;
        [self.tv setTableHeaderView:self.tvHeader];
    }
    // 加载快讯数据
    [self loadKuaiXunData];
    [self loadXinPinData];
    // 加载首页菜单
    [self loadAppEasyMenuList];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.navigationItem.title = @"首页";
    self.tabBarController.navigationItem.rightBarButtonItem = self.btnMsg;
    [self.tv reloadData];
    [self.imageLoopView reloadData];
    
    // 恢复导航栏显示
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

- (void)drawTvHeader {
    BOOL isSumitting = [ZbSaveManager isSumitting];
    if (isSumitting) {
        return;
    }
    
    CGFloat bannerHeight = DeviceWidth * TOP_BANNER_HEIGHTRATE;
    CGFloat textScrollHeight = CGRectGetHeight(self.viewTextScroll.frame);
    CGFloat toolHeight = 45 + 10;
    
    CGFloat tvHeight = toolHeight;
    if (self.xinpinArray.count > 0) {
        tvHeight += bannerHeight;
        self.imageLoopView.hidden = NO;
    } else {
        self.imageLoopView.hidden = YES;
    }
    
    if (self.kuaixunArray.count > 0) {
        tvHeight += textScrollHeight;
        self.viewTextScroll.hidden = NO;
    } else {
        self.viewTextScroll.hidden = YES;
    }
    
    // 设置tableview头部高度
    CGRect tvHeaderFrame = self.tvHeader.frame;
    tvHeaderFrame.size.height = tvHeight;
    self.tvHeader.frame = tvHeaderFrame;
    [self.tv setTableHeaderView:self.tvHeader];
    return;
}

#pragma mark - 接口调用
// 加载新品数据
- (void)loadXinPinData {
    NSDictionary *parameters = @{};
    [HttpRequest postWithURLString:[NcWebSerVice getXinPinUrl] parameters:parameters success:^(id responseObject) {
        NSString *result = responseObject[@"status"];
        if ([result isEqualToString:@"success"]) {
            NSArray *tagArray = responseObject[@"data"];
            // 无新品
            if (tagArray == nil || ![tagArray isKindOfClass:[NSArray class]]) {
                [self drawTvHeader];
                return;
            }
            // 新品数据
            [self.xinpinArray removeAllObjects];
            [self.xinpinArray addObjectsFromArray:tagArray];
            [self.imageLoopView reloadData];
            [self drawTvHeader];
        }
    } failure:^(NSError *error) {
        self.hud.labelText = @"加载失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    }];
    return;
}

// 加载快讯数据
- (void)loadKuaiXunData {
    NSDictionary *parameters = @{};
    [HttpRequest postWithURLString:[NcWebSerVice getKuaiXunUrl] parameters:parameters success:^(id responseObject) {
        NSString *result = responseObject[@"successful"];
        if (result.boolValue) {
            NSArray *tagArray = responseObject[@"data"];
            // 无快讯
            if (tagArray == nil || ![tagArray isKindOfClass:[NSArray class]]) {
                [self drawTvHeader];
                return;
            }
            // 快讯数据
            [self.kuaixunArray removeAllObjects];
            [self.kuaixunArray addObjectsFromArray:tagArray];
            // 快讯文本
            NSMutableArray *textArray = [NSMutableArray new];
            for (NSDictionary *curDict in tagArray) {
                [textArray addObject:curDict[@"listTitle"]];
            }
            self.textScrollView.titlesGroup = textArray;
            [self drawTvHeader];
        }
    } failure:^(NSError *error) {
        self.hud.labelText = @"加载快讯失败，请稍后重试";
        [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    }];
    return;
}

// 加载首页菜单
- (void)loadAppEasyMenuList {
    NSDictionary *parameters = @{};
    NSString *url = [NcWebSerVice getAppMenuNavListUrl:@"NAV_TOOL"];
    [HttpRequest getWithURLString:url parameters:parameters success:^(id responseObject) {
        NSString *result = responseObject[@"status"];
        if ([result isEqualToString:@"success"]) {
            NSArray *tagArray = responseObject[@"data"];
            // 无数据
            if (tagArray == nil || ![tagArray isKindOfClass:[NSArray class]]) {
                return;
            }
            // 菜单数据
            [self.array removeAllObjects];
            [self.array addObjectsFromArray:tagArray];
            [self.tv reloadData];
        }
    } failure:^(NSError *error) {
    }];
    return;
}

#pragma mark - 点击事件
- (void)itemOnClick:(NcControl *)control {
    NSString *TARGET_URL = control.userInfo[@"TARGET_URL"];
    HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    web.url = TARGET_URL;
    [self.navigationController pushViewController:web animated:YES];
}

// 消息中心点击
- (IBAction)btnMsgOnClick:(id)sender {
    HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    web.url = [NcWebSerVice msgUrl];
    [self.navigationController pushViewController:web animated:YES];
    return;
}

#pragma mark - YKuImageInLoopScrollViewDelegate
- (int)numOfPageForScrollView:(YKuImageInLoopScrollView*)ascrollView {
    return (int)self.xinpinArray.count;
}

- (int)widthForScrollView:(YKuImageInLoopScrollView*)ascrollView {
    return self.view.frame.size.width;
}

- (UIView *)scrollView:(YKuImageInLoopScrollView*)ascrollView viewAtPageIndex:(int)apageIndex {
    CGFloat bannerHeight = DeviceWidth * TOP_BANNER_HEIGHTRATE;
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, bannerHeight)];
    UIImageView *curImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, bannerHeight)];
    
    NSDictionary *curXinPin = self.xinpinArray[apageIndex];
    NSString *listImg01 = [NcWebSerVice getImgFullUrlNoScr:curXinPin[@"listImg01"]];
    [curImageView sd_setImageWithURL:[NSURL URLWithString:listImg01]];
    //    curImageView.contentMode = UIViewContentModeScaleAspectFill;
    [bgView addSubview:curImageView];
    return bgView;
}

- (void)scrollView:(YKuImageInLoopScrollView*)ascrollView didTapIndex:(int)apageIndex {
    NSDictionary *curXinPin = self.xinpinArray[apageIndex];
    NSString *listUrl = curXinPin[@"listUrl"];
    if (!listUrl || listUrl.length <= 0) {
        return;
    }
    
    HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    if ([listUrl hasPrefix:@"http"]) {
        web.url = [NcWebSerVice noticeDetailUrl:listUrl];
    } else {
        web.url = [NSString stringWithFormat:@"%@%@%@", WEBSERVICE_HOST, PACKAGE_NAME, listUrl];
    }
    [self.navigationController pushViewController:web animated:YES];
}

/*
 选中第几页
 @param didSelectedPageIndex 选中的第几项，[0-numOfPageForScrollView];
 */
-(void)scrollView:(YKuImageInLoopScrollView*)ascrollView didSelectedPageIndex:(int) apageIndex
{
    //    NSLog(@"didSelectedPageIndex:%d",apageIndex);
}

#pragma mark - SDCycleScrollViewDelegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    NSDictionary *curKuaiXun = self.kuaixunArray[index];
    NSString *listUrl = curKuaiXun[@"listUrl"];
    HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    if ([listUrl hasPrefix:@"http"]) {
        web.url = [NcWebSerVice noticeDetailUrl:listUrl];
    } else {
        web.url = [NSString stringWithFormat:@"%@%@%@", WEBSERVICE_HOST, PACKAGE_NAME, listUrl];;
    }
    [self.navigationController pushViewController:web animated:YES];
}

#pragma mark - UITableView delegate && DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.array.count % 4 == 0) {
        return self.array.count / 4;
    } else {
        return self.array.count / 4 + 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 85;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"CELL";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.backgroundColor = [UIColor clearColor];
    CGFloat width = DeviceWidth / 4;
    for (int i = 0; i < 4; i ++) {
        if (indexPath.row * 4 + i >= self.array.count) {
            break;
        }
        
        NSDictionary * item = [self.array objectAtIndex:(indexPath.row * 4 + i)];
        
        NcControl *itemControl = [[NcControl alloc] initWithFrame:CGRectMake(width * i, 0, width, 85)];
        itemControl.backgroundColor = [UIColor whiteColor];
        itemControl.layer.borderWidth = 0.5;
        itemControl.layer.borderColor = [UIColorFromHexString(@"E8E8E8") CGColor];
        itemControl.tag = indexPath.row * 4 + i + 100;
        itemControl.userInfo = item;
        [itemControl addTarget:self action:@selector(itemOnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        UIImageView *itemImg = [[UIImageView alloc] initWithFrame:CGRectMake((width - 30) / 2, 17, 30, 30)];
        NSString *imageUrl = [NcWebSerVice getImgFullUrl:item[@"ITEM_ICON"]];
        [itemImg sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
//        itemImg.image = [UIImage imageNamed:item[@"itemImg"]];
        [itemControl addSubview:itemImg];
        
        UILabel *itemMenuLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 52, CGRectGetWidth(itemControl.frame), 21)];
        itemMenuLab.textAlignment = NSTextAlignmentCenter;
        [itemMenuLab setFont:[UIFont systemFontOfSize:14.0f]];
        [itemControl addSubview:itemMenuLab];
        itemMenuLab.text = [NSString stringWithFormat:@"%@",item[@"ITEM_NAME"]];
        [cell addSubview:itemControl];
        
    }
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
