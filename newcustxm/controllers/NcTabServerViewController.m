//
//  NcTabServerViewController.m
//  newcustxm
//
//  Created by chyo on 16/11/8.
//  Copyright © 2016年 chyo. All rights reserved.
//

#import "NcTabServerViewController.h"
#import "HsWebViewController.h"

static NSString *const cellIdentifier = @"itemCollection";

@interface NcTabServerViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation NcTabServerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.array = [[NSMutableArray alloc] init];
    
    self.array = [NSMutableArray arrayWithObjects:@{@"itemkey":@"专卖服务", @"itemImg":@"zm"}, @{@"itemkey":@"营销服务", @"itemImg":@"yx"}, @{@"itemkey":@"品牌服务", @"itemImg":@"pp"}, @{@"itemkey":@"店铺经营", @"itemImg":@"dp"}, @{@"itemkey":@"积分商城", @"itemImg":@"jf"},  nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.navigationItem.title = @"服务";
    
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:cellIdentifier];
    
    // 恢复导航栏显示
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

#pragma mark ---- UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.array.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:@"itemCollection" forIndexPath:indexPath];
    cell.layer.borderWidth = 0.5;
    cell.layer.borderColor = [UIColorFromHexString(@"E8E8E8") CGColor];
    
    NSDictionary *item = [self.array objectAtIndex:indexPath.row];
    UIImageView *itemImg = [[UIImageView alloc] initWithFrame:CGRectMake((CGRectGetWidth(cell.frame) - 50) / 2, 10, 50, 50)];
    itemImg.image = [UIImage imageNamed:item[@"itemImg"]];
    
    UILabel *itemMenuLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 65, CGRectGetWidth(cell.frame), 21)];
    itemMenuLab.textAlignment = NSTextAlignmentCenter;
    [itemMenuLab setFont:[UIFont systemFontOfSize:14.0f]];
    itemMenuLab.text = [NSString stringWithFormat:@"%@",item[@"itemkey"]];

    [cell addSubview:itemImg];
    [cell addSubview:itemMenuLab];
    return cell;
}

#pragma mark ---- UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return (CGSize){DeviceWidth / 3, 100};
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.f;
}

#pragma mark ---- UICollectionViewDelegate
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

// 点击高亮
- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
//    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
//    cell.backgroundColor = [UIColor greenColor];
}

// 选中某item
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *item = [self.array objectAtIndex:indexPath.row];
    NSString *itemKey = item[@"itemkey"];
    DLog(@"onClick %@", itemKey);
    if ([itemKey isEqualToString:@"专卖服务"]) {
//        HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
//        web.url = [NcWebSerVice monopolyUrl];
//        [self.navigationController pushViewController:web animated:YES];
    } else if ([itemKey isEqualToString:@"营销服务"]) {
//        HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
//        web.url = [NcWebSerVice giftLabelUrl];
//        [self.navigationController pushViewController:web animated:YES];
    } else if ([itemKey isEqualToString:@"品牌服务"]) {
//        HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
//        web.url = [NcWebSerVice giftLabelUrl];
//        [self.navigationController pushViewController:web animated:YES];
    } else if ([itemKey isEqualToString:@"店铺经营"]) {
        HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
        web.url = [NcWebSerVice merchantDeliveryInfoUrl];
        [self.navigationController pushViewController:web animated:YES];
    } else if ([itemKey isEqualToString:@"积分商城"]) {
//        HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
//        web.url = [NcWebSerVice giftLabelUrl];
//        [self.navigationController pushViewController:web animated:YES];
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
