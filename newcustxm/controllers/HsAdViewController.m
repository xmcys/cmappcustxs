//
//  HsAdViewController.m
//  CmAppCustXs
//
//  Created by chyo on 15/9/24.
//  Copyright (c) 2015年 chyo. All rights reserved.
//

#import "HsAdViewController.h"
#import "AppDelegate.h"
#import "YKuImageInLoopScrollView.h"

@interface HsAdViewController () <YKuImageInLoopScrollViewDelegate>

// 广告滚动控件
@property (nonatomic, strong) IBOutlet YKuImageInLoopScrollView *imageLoopView;

// 当前广告内容数组
@property (nonatomic, strong) NSArray *curAdArray;
// 是否浏览过一遍
@property (nonatomic, assign) BOOL isAlreadyLook;

@end

@implementation HsAdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate onAdShowOk];
    return;
    // 获取广告数据
    NSArray *adArray = [[ZbSaveManager shareManager] getAdArray];
    self.curAdArray = adArray;
    
    // 无数据
    if (adArray == nil || adArray.count <= 0) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate onAdShowOk];
        return;
    }
    
//    self.imageLoopView.delegate = self;
    // 设置yKuImageInLoopScrollView显示类型
    self.imageLoopView.scrollViewType = ScrollViewDefault;
    // 设置styledPageControl位置
    [self.imageLoopView.styledPageControl setPageControlSite:PageControlSiteMiddle];
    [self.imageLoopView.styledPageControl setBottomDistance:12];
    // 设置styledPageControl已选中下的内心圆颜色
    [self.imageLoopView.styledPageControl setCoreSelectedColor:[UIColor whiteColor]];
    [self.imageLoopView.styledPageControl setCoreNormalColor:UIColorFromHexString(@"888888")];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 初始化UI时机
- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.imageLoopView reloadData];
}

#pragma mark - YKuImageInLoopScrollViewDelegate
- (int)numOfPageForScrollView:(YKuImageInLoopScrollView *)ascrollView{
    return (int)self.curAdArray.count;
}

- (int)widthForScrollView:(YKuImageInLoopScrollView *)ascrollView{
    int width = DeviceWidth;
    return width;
}

- (UIView *)scrollView:(YKuImageInLoopScrollView *)ascrollView viewAtPageIndex:(int)apageIndex
{
    UIView *bgView = [[UIView alloc]initWithFrame:ascrollView.bounds];
    UIImageView *curImageView = [[UIImageView alloc] initWithFrame:ascrollView.bounds];
    NSDictionary *adDict = [self.curAdArray objectAtIndex:apageIndex];
    NSString *imgUrl = adDict[@"IMAGE_URL"];
    [curImageView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
    curImageView.contentMode = UIViewContentModeScaleAspectFill;
    curImageView.clipsToBounds = YES;
    [bgView addSubview:curImageView];
    return bgView;
}

- (void)scrollView:(YKuImageInLoopScrollView*)ascrollView didTapIndex:(int)apageIndex{
    //    DLog(@"Clicked page%d",apageIndex);
    NSDictionary *adDict = [self.curAdArray objectAtIndex:apageIndex];
    [ZbSaveManager setClickAdDict:adDict];
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate onAdShowOk];
    
}

/*
 选中第几页
 @param didSelectedPageIndex 选中的第几项，[0-numOfPageForScrollView];
 */
-(void)scrollView:(YKuImageInLoopScrollView*)ascrollView didSelectedPageIndex:(int) apageIndex isNewPage:(BOOL)isNewPage
{
//    NSLog(@"didSelectedPageIndex:%d isNewPage=%@",apageIndex, [NSNumber numberWithBool:isNewPage]);
    if (self.curAdArray.count == 1) {
        // 只有一张
        if (!self.isAlreadyLook) {
            self.isAlreadyLook = YES;
            return;
        }
        
        // 已浏览
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate onAdShowOk];
    } else {
        // 多张
        if (!self.isAlreadyLook) {
            if (apageIndex == (self.curAdArray.count - 1)) {
                // 浏览类库的问题兼容，每一页会走到2次
                self.isAlreadyLook = YES;
            }
            return;
        }
        
        // 已浏览一遍
        if (apageIndex == (self.curAdArray.count - 1)) {
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate onAdShowOk];
        }
    }
}

/*
 开始拖动
 */
- (void)scrollViewWillBeginDragging:(YKuImageInLoopScrollView *)ascrollView {
    if (ascrollView.pageIndex == self.curAdArray.count - 1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate onAdShowOk];
        });
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
