//
//  BsController.h
//  newcustxm
//
//  Created by chyo on 16/11/8.
//  Copyright © 2016年 chyo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonHeader.h"

@interface BsViewController : UIViewController

@property (nonatomic, assign) BOOL isInit;                   // 是否已初始化UI
@property (nonatomic, strong) MBProgressHUD *hud;            // 提示器
@property (nonatomic, strong) NSString *myName;              // 友盟统计用

// 初始化UI时机，只执行一次
- (void)viewWillAppearOnce;

// 返回点击
- (IBAction)leftBarItemOnClick:(id)sender;


@end
