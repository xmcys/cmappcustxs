//
//  NcTabWorkViewController.m
//  newcustxm
//
//  Created by chyo on 16/11/8.
//  Copyright © 2016年 chyo. All rights reserved.
//

#import "NcTabWorkViewController.h"
#import "HsWebViewController.h"
#import "NcJyViewController.h"
#import "ZbWorkLineCell.h"

@interface NcTabWorkViewController () <UITableViewDelegate, UITableViewDataSource>

// 头像
@property (nonatomic, strong) IBOutlet UIImageView *ivHead;
// 名称
@property (nonatomic, strong) IBOutlet UILabel *labName;
// 许可证号
@property (nonatomic, strong) IBOutlet UILabel *labCode;
// UITableView
@property (nonatomic, strong) IBOutlet UITableView *tv;
// tv header
@property (nonatomic, strong) IBOutlet UIView *tvHeader;

// 工作台项目
@property (nonatomic, strong) IBOutlet UILabel *labTotalAmt;
@property (nonatomic, strong) IBOutlet UILabel *labTotalNum;
@property (nonatomic, strong) IBOutlet UILabel *labTotalUser;
@property (nonatomic, strong) IBOutlet UILabel *labDdAmt;
@property (nonatomic, strong) IBOutlet UILabel *labDdNum;
@property (nonatomic, strong) IBOutlet UILabel *labDdUser;
@property (nonatomic, strong) IBOutlet UILabel *labZxAmt;
@property (nonatomic, strong) IBOutlet UILabel *labZxNum;
@property (nonatomic, strong) IBOutlet UILabel *labZxUser;
@property (nonatomic, strong) IBOutlet UILabel *labOneAmt;
@property (nonatomic, strong) IBOutlet UILabel *labHistoryNum;
@property (nonatomic, strong) IBOutlet UILabel *labHistoryUser;

@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation NcTabWorkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.array = [NSMutableArray new];
    self.automaticallyAdjustsScrollViewInsets = NO;
    // 用户信息
    self.labName.text = [NcWebSerVice user][@"USER_NAME"];
//    self.labName.text = @"便利店";
    self.labCode.text = [NcWebSerVice user][@"CUST_LICENCE_CODE"];
//    self.labCode.text = @"5328****0254";
    // 用户头像
    self.ivHead.contentMode = UIViewContentModeScaleAspectFill;
    self.ivHead.layer.cornerRadius = self.ivHead.frame.size.width / 2;
    self.ivHead.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    if (!self.isInit) {
    } else {
        // 加载工作台数据
        [self loadWordNumInfo];
        // 加载用户头像信息
        [self loadUserInfo];
    }
    [super viewWillAppear:animated];
    self.tabBarController.navigationItem.title = nil;
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
    
    // 恢复导航栏显示
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    [self calcTvHeader];
    
    self.hud.labelText = @"加载中..";
    [self.hud show:NO];
    // 加载工作台数据
    [self loadWordNumInfo];
    // 加载用户头像信息
    [self loadUserInfo];
    // 加载菜单
    [self loadAppEasyMenuList];
}

#pragma mark - 界面绘制
// 重算header高度
- (void)calcTvHeader {
    // header高度
    CGFloat tagHeaderHeight = [self.tvHeader systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    CGRect headerFrame = CGRectMake(0, 0, DeviceWidth, tagHeaderHeight);
    self.tvHeader.frame = headerFrame;
    [self.tv setTableHeaderView:self.tvHeader];
}

#pragma mark - 接口调用
// 加载工作台数据
- (void)loadWordNumInfo {
    [HttpRequest getWithURLString:[NcWebSerVice getWorkNumUrl] parameters:nil
                          success:^(id responseObject) {
                              [self.hud hide:NO];
                              
                              NSString *result = responseObject[@"successful"];
                              if (result.boolValue) {
                                  self.labTotalAmt.text = [NSString stringWithFormat:@"%@", responseObject[@"data"][@"TOTAL_AMT"]];
                                  self.labTotalUser.text = [NSString stringWithFormat:@"%@", responseObject[@"data"][@"TOTAL_USER"]];
                                  self.labTotalNum.text = [NSString stringWithFormat:@"%@", responseObject[@"data"][@"TOTAL_NUM"]];
                                  self.labDdAmt.text = [NSString stringWithFormat:@"%@", responseObject[@"data"][@"DD_AMT"]];
                                  self.labDdUser.text = [NSString stringWithFormat:@"%@", responseObject[@"data"][@"DD_USER"]];
                                  self.labDdNum.text = [NSString stringWithFormat:@"%@", responseObject[@"data"][@"DD_NUM"]];
                                  self.labZxAmt.text = [NSString stringWithFormat:@"%@", responseObject[@"data"][@"ZX_AMT"]];
                                  self.labZxUser.text = [NSString stringWithFormat:@"%@", responseObject[@"data"][@"ZX_USER"]];
                                  self.labZxNum.text = [NSString stringWithFormat:@"%@", responseObject[@"data"][@"ZX_NUM"]];
                                  self.labOneAmt.text = [NSString stringWithFormat:@"%@", responseObject[@"data"][@"ONE_AMT"]];
                                  self.labHistoryUser.text = [NSString stringWithFormat:@"%@", responseObject[@"data"][@"HISTORY_USER"]];
                                  self.labHistoryNum.text = [NSString stringWithFormat:@"%@", responseObject[@"data"][@"HISTORY_NUM"]];
                              }
                          } failure:^(NSError *error) {
                              self.hud.labelText = @"加载失败，请稍后重试";
                              [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                          }];
    return;
}

- (void)loadUserInfo {
    NSString *userId = [NcWebSerVice user][@"USER_ID"];
    [HttpRequest postWithURLString:[NcWebSerVice getUserInfo] parameters:@{@"userId":userId} success:^(id responseObject) {
        NSString *result = responseObject[@"statusCode"];
        if ([result isEqualToString:@"200"]) {
            NSString *HEAD_PICTURE = responseObject[@"data"][@"HEAD_PICTURE"];
            if (!HEAD_PICTURE || (![HEAD_PICTURE isKindOfClass:[NSString class]]) || HEAD_PICTURE.length <= 0) {
                self.ivHead.image = [UIImage imageNamed:@"mine_head"];
            } else {
                NSString *urlString = [NcWebSerVice getImgFullUrl:HEAD_PICTURE];
                [self.ivHead sd_setImageWithURL:[NSURL URLWithString:urlString]];
            }
        } else {
            self.ivHead.image = [UIImage imageNamed:@"mine_head"];
        }
        
    } failure:^(NSError *error) {
        return;
    }];
}

// 加载首页菜单
- (void)loadAppEasyMenuList {
    NSDictionary *parameters = @{};
    NSString *url = [NcWebSerVice getAppMenuNavListUrl:@"NAV_WORK"];
    [HttpRequest getWithURLString:url parameters:parameters success:^(id responseObject) {
        NSString *result = responseObject[@"status"];
        if ([result isEqualToString:@"success"]) {
            NSArray *tagArray = responseObject[@"data"];
            // 无数据
            if (tagArray == nil || ![tagArray isKindOfClass:[NSArray class]]) {
                return;
            }
            // 菜单数据
            [self.array removeAllObjects];
            [self.array addObjectsFromArray:tagArray];
            [self.tv reloadData];
        }
    } failure:^(NSError *error) {
    }];
    return;
}

#pragma mark - 点击事件
// 扫描点击
- (IBAction)btnScanCodeOnClick:(id)sender {
    return;
}

// 交易管理点击
- (IBAction)btnJyglOnClick:(id)sender {
    NcJyViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"NcJyViewController"];
    [self.navigationController pushViewController:web animated:YES];
    return;
}

#pragma mark - UITableView delegate && DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.array.count % 4 == 0) {
        return self.array.count / 4;
    } else {
        return self.array.count / 4 + 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 84;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    ZbWorkLineCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    NSArray *viewArray = @[cell.viewBtn1, cell.viewBtn2, cell.viewBtn3, cell.viewBtn4, ];
    NSArray *labArray = @[cell.lab1, cell.lab2, cell.lab3, cell.lab4, ];
    NSArray *btnArray = @[cell.btn1, cell.btn2, cell.btn3, cell.btn4, ];
    NSArray *ivArray = @[cell.iv1, cell.iv2, cell.iv3, cell.iv4, ];
    for (int i = 0; i < 4; i ++) {
        UIView *viewBtn = viewArray[i];
        if (indexPath.row * 4 + i >= self.array.count) {
            viewBtn.hidden = YES;
            continue;
        }
        viewBtn.hidden = NO;
        UILabel *lab = labArray[i];
        NcButton *btn = btnArray[i];
        UIImageView *iv = ivArray[i];
        
        NSDictionary * item = [self.array objectAtIndex:(indexPath.row * 4 + i)];
        
        btn.userInfo = item;
        NSString *imageUrl = [NcWebSerVice getImgFullUrl:item[@"ITEM_ICON"]];
        [iv sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
        lab.text = [NSString stringWithFormat:@"%@",item[@"ITEM_NAME"]];
    }
    
    // 按钮点击block
    __weak typeof(self) weakSelf = self;
    cell.btnClickBlock = ^(NSDictionary *info) {
        NSString *TARGET_URL = info[@"TARGET_URL"];
        HsWebViewController *web = [weakSelf.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
        web.url = TARGET_URL;
        [weakSelf.navigationController pushViewController:web animated:YES];
    };
    return cell;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
