//
//  HsWebViewController.m
//  CmAppCustXs
//
//  Created by chyo on 15/9/24.
//  Copyright (c) 2015年 chyo. All rights reserved.
//

#import "HsWebViewController.h"
#import "NJKWebViewProgress.h"
#import "NJKWebViewProgressView.h"
#import "AppDelegate.h"
#import "CTScannerController.h"
#import "AFNetworking.h"
#import "NcWebSerVice.h"

#define HTTP_REQUEST_UPLOAD_IMAGE 0          // 上传图片

@interface HsWebViewController () <NJKWebViewProgressDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate>

// 加载失败容器view
@property (nonatomic, strong) IBOutlet UIView *viewFailed;
// 重载按钮
@property (nonatomic, strong) IBOutlet UIButton *btnRefresh;
// 导航栏左按钮
@property (nonatomic, strong) UIBarButtonItem *leftBarItem;
// 进度view
@property (nonatomic, strong) NJKWebViewProgressView *progressView;
// 进度解析
@property (nonatomic, strong) NJKWebViewProgress *progressProxy;

// 是否加载成功
@property (nonatomic, assign) BOOL isUrlLoadOk;
// 是否任一请求加载失败
@property (nonatomic, assign) BOOL isAnyRequestFailed;
// 本界面是否push动画结束了
@property (nonatomic, assign) BOOL isPushAnimateEnd;

- (void)back;

@end

@implementation HsWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.curJsInteract = [HsJsInteract new];
    self.curJsInteract.controller = self;
    self.curJsInteract.delegate = self;
    
    // 往js注入模型
    JSContext *context = [self.webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    context[@"marketing4js"] = self.curJsInteract;
    
    context.exceptionHandler = ^(JSContext *con, JSValue *exception) {
        NSLog(@"网页有报错：%@", exception);
        con.exception = exception;
    };
    
    // 进度解析
    _progressProxy = [[NJKWebViewProgress alloc] init];
    _webView.delegate = _progressProxy;
    _progressProxy.webViewProxyDelegate = self;
    _progressProxy.progressDelegate = self;
    // 进度view
    CGFloat progressBarHeight = 2.f;
    CGRect navigationBarBounds = self.navigationController.navigationBar.bounds;
    CGRect barFrame = CGRectMake(0, navigationBarBounds.size.height - progressBarHeight, navigationBarBounds.size.width, progressBarHeight);
    _progressView = [[NJKWebViewProgressView alloc] initWithFrame:barFrame];
    _progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    // 加载失败按钮边框
    self.btnRefresh.layer.borderWidth = 0.8;
    self.btnRefresh.layer.borderColor = UIColorFromHexString(@"128f5e").CGColor;
    self.btnRefresh.layer.cornerRadius = 5;
    
    if (![self isNavRoot] && !self.isShowNav) {
        self.title = @"加载中...";
    }
    self.automaticallyAdjustsScrollViewInsets = NO;
    // 重载按钮默认不显示
    self.viewFailed.hidden = YES;
    self.btnRefresh.hidden = YES;
    self.navigationController.navigationBar.translucent = YES;
}

- (void)viewWillAppearOnce
{
    [super viewWillAppearOnce];
    
    // 返回按钮
    self.leftBarItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStyleBordered target:self action:@selector(leftBarItemOnClick:)];
    //    self.leftBarItem.tintColor = [UIColor colorWithRed:41/255.0 green:41/255.0 blue:41/255.0 alpha:1.0];
    self.leftBarItem.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = self.leftBarItem;
    
    // 网页控件
    self.webView.backgroundColor = [UIColor whiteColor];
    self.webView.scalesPageToFit = YES;
    self.webView.dataDetectorTypes = UIDataDetectorTypeNone;
    self.webView.mediaPlaybackRequiresUserAction = YES;
    self.webView.allowsInlineMediaPlayback = YES;
    self.webView.scrollView.bounces = NO;
    
    [self loadUrl];
    
    if (self.isShowNav) {
        return;
    }
    
    if ([self isNavRoot]) {
        // 一级界面，都不显示导航栏
        [self.navigationController setNavigationBarHidden:YES animated:NO];
        return;
    }
    self.navigationController.delegate = self;
    // 二级以上界面先显示导航栏
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    // 添加进度view
    [self.navigationController.navigationBar addSubview:_progressView];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // 加载完毕，回复导航栏显示
    NSString *readyState = [self.webView stringByEvaluatingJavaScriptFromString:@"document.readyState"];
    BOOL complete = [readyState isEqualToString:@"complete"];
    if (!self.isAnyRequestFailed && (_progressView.progress >= 1.0 || (_progressView.progress >= 0.1 && complete))) {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        self.navigationController.navigationBar.translucent = YES;
        
        // 告知js界面显示时机
        NSString *jsStr = [NSString stringWithFormat:@"viewWillAppear()"];
        [self.webView stringByEvaluatingJavaScriptFromString:jsStr];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.navigationController.delegate = nil;
    // Remove progress view
    // because UINavigationBar is shared with other ViewControllers
    [_progressView removeFromSuperview];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    return;
}

- (void)back
{
    if ([self.webView canGoBack]) {
        [self.webView goBack];
    }
}

// 判断是否nav的首界面
- (BOOL)isNavRoot {
    UITabBarController *tab = self.tabBarController;
    return tab != nil;
}

#pragma mark - 加载数据
- (void)loadUrl {
    // 重载按钮默认不显示
    self.viewFailed.hidden = YES;
    self.btnRefresh.hidden = YES;
    
    NSString *tagUrl = [NcWebSerVice getHtmlFullUrl:self.url];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:tagUrl] cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:60];
    [self.webView loadRequest:request];
    //    NSLog(@"webView loadUrl tagUrl=%@", tagUrl);
}

#pragma mark - 点击事件
- (void)leftBarItemOnClick:(id)sender {
    [super leftBarItemOnClick:sender];
    
    if (self.isShowNav) {
        return;
    }
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (IBAction)btnRefreshOnClick:(id)sender {
    self.viewFailed.hidden = YES;
    self.btnRefresh.hidden = YES;
    self.isAnyRequestFailed = NO;
    
    [self loadUrl];
    return;
}

- (NSString *)formatUrl:(NSString *)pramas
{
    NSString * str = [pramas stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([str hasPrefix:@"("]) {
        str = [str substringFromIndex:1];
    }
    if ([str hasSuffix:@")"]) {
        str = [str substringToIndex:str.length - 1];
    }
    return [[[str stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"'" withString:@""] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
}

#pragma mark - UIWebView Delegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    //    NSString *xx = request.URL.description;
    NSString *lastCompent = request.URL.description;
    lastCompent = [lastCompent stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //    DLog(@"URL.description：%@", xx);
    //    DLog(@"lastCompent：%@", lastCompent);
    
    __weak typeof(self) weakSelf = self;
    // html与原生交互头
    NSRange range = [lastCompent rangeOfString:@"window.marketing4js."];
    if(range.location != NSNotFound) {
        // 函数及参数信息
        lastCompent = [lastCompent substringFromIndex:(range.location + range.length)];;
        
        // 返回上一页
        if ([lastCompent hasPrefix:@"back"]){
            // 返回通知上一页刷新
            if (self.backBlock) {
                self.backBlock();
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
        // 显示Toast提醒
        else if ([lastCompent hasPrefix:@"showToast"]) {
            lastCompent = [lastCompent stringByReplacingOccurrencesOfString:@"showToast" withString:@""];
            lastCompent = [self formatUrl:lastCompent];
            NSArray *array = [lastCompent componentsSeparatedByString:@","];
            if (array.count > 0) {
                NSString *msg = [array objectAtIndex:0];
                [MBProgressHUD showTextHudInView:self.view withText:msg lastTime:1.0 completeBlock:nil];
            }
        }
        // 跳转新页面，返回执行js
        else if ([lastCompent hasPrefix:@"openWebViewBackJs"]) {
            lastCompent = [lastCompent stringByReplacingOccurrencesOfString:@"openWebViewBackJs" withString:@""];
            lastCompent = [self formatUrl:lastCompent];
            NSArray *array = [lastCompent componentsSeparatedByString:@","];
            if (array.count >= 1) {
                NSString *targetUlr = [array objectAtIndex:0];
                // 返回时执行的js字符串
                NSString *jsStr = @"";
                if (array.count >= 2) {
                    jsStr = [array objectAtIndex:1];
                }
                // 要关闭窗口的个数
                NSInteger closeCnt = 0;
                if (array.count >= 3) {
                    closeCnt = ((NSString *)[array objectAtIndex:2]).integerValue;
                }
                
                __weak typeof(self) weakSelf = self;
                if (closeCnt <= 0) {
                    // 直接打开新界面模式
                    HsWebViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
                    controller.url = targetUlr;
                    if (jsStr.length > 0) {
                        controller.backBlock = ^(void) {
                            [weakSelf.webView stringByEvaluatingJavaScriptFromString:jsStr];
                        };
                    }
                    [self.navigationController pushViewController:controller animated:YES];
                } else {
                    // 先删除旧界面，再打开新界面模式
                    
                    NSMutableArray *vcArray = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
                    UIViewController *firstDelVc = nil;
                    // 删除层级大于拥有层级
                    if (closeCnt >= vcArray.count) {
                        // 直接删除至根界面
                        if (vcArray.count > 1) {
                            firstDelVc = vcArray[1];
                        }
                        vcArray = [NSMutableArray arrayWithObjects:vcArray[0], nil];
                    } else {
                        firstDelVc = vcArray[vcArray.count - closeCnt];
                        vcArray = [NSMutableArray arrayWithArray:[vcArray subarrayWithRange:NSMakeRange(0, vcArray.count - closeCnt)]];
                    }
                    
                    // 添加新界面
                    HsWebViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
                    controller.url = targetUlr;
                    if (firstDelVc) {
                        // 将要删除的第一个界面的返回执行函数，让新界面返回时执行
                        if ([firstDelVc isKindOfClass:[HsWebViewController class]]) {
                            HsWebViewController *firstDelWebVc = (HsWebViewController *)firstDelVc;
                            controller.backBlock = firstDelWebVc.backBlock;
                        }
                    }
                    [vcArray addObject:controller];
                    [self.navigationController setViewControllers:vcArray animated:YES];
                }
            }
        }
        // 跳转新页面
        else if ([lastCompent hasPrefix:@"openWebView"]) {
            lastCompent = [lastCompent stringByReplacingOccurrencesOfString:@"openWebView" withString:@""];
            lastCompent = [self formatUrl:lastCompent];
            NSArray *array = [lastCompent componentsSeparatedByString:@","];
            if (array.count >= 1) {
                NSString *targetUlr = [array objectAtIndex:0];
                BOOL isRefreshOnBack = NO;
                if (array.count >= 2) {
                    NSString *refreshOnResult = [array objectAtIndex:1];
                    isRefreshOnBack = [refreshOnResult isEqualToString:@"true"] ? YES : NO;
                }
                
                __weak typeof(self) weakSelf = self;
                // 新页面
                HsWebViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
                controller.url = targetUlr;
                if (isRefreshOnBack) {
                    controller.backBlock = ^(void) {
                        [weakSelf loadUrl];
                        
                        weakSelf.navigationController.delegate = self;
                        if ([weakSelf isNavRoot]) {
                            // 一级界面，都不显示导航栏
                            [weakSelf.navigationController setNavigationBarHidden:YES animated:NO];
                            return;
                        }
                        // 二级以上界面先显示导航栏
                        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
                        [weakSelf.navigationController setNavigationBarHidden:NO animated:NO];
                    };
                }
                [self.navigationController pushViewController:controller animated:YES];
            }
        }
        // 获取二维码
        // 回调getBarCodeResult(code)
        else if ([lastCompent hasPrefix:@"getBarCode"]) {
            CTScannerController *scanController = [[CTScannerController alloc] initWithNibName:nil bundle:nil];
            scanController.scanResultBlock = ^(NSString *barCode) {
                // 扫描结果
                [weakSelf.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"onBarCodeResult('%@')", barCode]];
                [self.navigationController popViewControllerAnimated:NO];
            };
            // 跳转扫码
            [self.navigationController pushViewController:scanController animated:YES];
        }
        // 检查版本更新
        else if ([lastCompent hasPrefix:@"checkNewVersion"]) {
        }
        // 拍照
        else if ([lastCompent hasPrefix:@"getImage"]) {
            
            UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照", @"从相册选择", nil];
            [action showInView:self.navigationController.view];
        }
        // 退出登录
        else if ([lastCompent hasPrefix:@"logOut"]) {
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate logOut];
        }
        // 分享
        else if ([lastCompent hasPrefix:@"share"]) {
            //            lastCompent = [lastCompent stringByReplacingOccurrencesOfString:@"share" withString:@""];
            //            lastCompent = [self formatUrl:lastCompent];
            //            NSArray *array = [lastCompent componentsSeparatedByString:@","];
            //            if (array.count > 0) {
            //                NSString  *platform = [array objectAtIndex:0];
            //                NSString *title = [array objectAtIndex:1];
            //                NSString *titleUrl = [array objectAtIndex:2];
            //                NSString  *text = [array objectAtIndex:3];
            //                NSString *imageUrl = [array objectAtIndex:4];
            //                [self shareClick:platform title:title titleUrl:titleUrl text:text imgaeUrl:imageUrl];
            //            }
        }
        // 获取定位信息
        else if ([lastCompent hasPrefix:@"getLocation"]) {
            BMKUserLocation *loc = [[LocationServiceManager shareManager] getCurLocation];
            if (loc) {
                NSString *js = [NSString stringWithFormat:@"onGetLocationResult(%@, %@)", [NSNumber numberWithDouble:loc.location.coordinate.longitude], [NSNumber numberWithDouble:loc.location.coordinate.latitude]];
                [weakSelf.webView stringByEvaluatingJavaScriptFromString:js];
            } else {
                NSString *js = [NSString stringWithFormat:@"onGetLocationResult(0, 0)"];
                [weakSelf.webView stringByEvaluatingJavaScriptFromString:js];
            }
        }
        // 显示导航栏
        else if ([lastCompent hasPrefix:@"showNav"]) {
            self.navigationController.navigationBar.translucent = NO;
            [self.navigationController setNavigationBarHidden:NO animated:NO];
            [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
            self.isShowNav = YES;
            self.viewColor.hidden = YES;
            self.alcWebViewTop.constant = 0;
            self.title = [self.webView stringByEvaluatingJavaScriptFromString:@"document.title"];
        }
        return NO;
    } else {
        return YES;
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    if (self.isShowNav) {
        return;
    }
    // 只要有一个失败就显示失败重刷view
    self.isAnyRequestFailed = YES;
    NSLog(@"didFailLoadWithError=%@", error);
    self.viewFailed.hidden = NO;
    self.btnRefresh.hidden = NO;
    
    //    self.hud.labelText = @"加载失败";
    //    [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
    //    NSLog(@"%@", error);
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if (self.isShowNav) {
        self.title = [self.webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    }
    
    NSString *lJs = @"document.documentElement.innerHTML";
    NSString *lHtml1 = [self.webView stringByEvaluatingJavaScriptFromString:lJs];
    if ([lHtml1 rangeOfString:@"HTTP Status 404"].location != NSNotFound) {
        // 404
        self.title = @"404";
        self.isAnyRequestFailed = YES;
        self.viewFailed.hidden = NO;
        self.btnRefresh.hidden = NO;
    }
    
    // 之前就加载过，然后重刷了
    if (self.isUrlLoadOk) {
        // 再次往js注入模型
        JSContext *context=[self.webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
        context[@"marketing4js"] = self.curJsInteract;
    }
    self.isUrlLoadOk = YES;
    
    //    // 测试代码
    //    __weak typeof(self) weakSelf = self;
    //    if (![self.url containsString:@"home.jsp"]) {
    //        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //            NSString *testJsStr = [NSString stringWithFormat:@"location.href='window.marketing4js.showNav()'"] ;
    //            [weakSelf.webView stringByEvaluatingJavaScriptFromString:testJsStr];
    //        });
    //    }
}

#pragma mark - UIWebView 进度Delegate
- (void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress {
    [_progressView setProgress:progress animated:YES];
    
    if (self.isShowNav) {
        return;
    }
    //    NSLog(@"updateProgress() progress=%f", progress);
    if (progress >= 1.0) {
        // 显示顶部色块
        self.viewColor.hidden = NO;
        if (!self.isAnyRequestFailed && self.isPushAnimateEnd) {
            // 加载成功关闭导航栏
            //            NSLog(@"网页加载结束时，push动画已结束，直接关闭导航栏");
            [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
            [self.navigationController setNavigationBarHidden:YES animated:NO];
        }
    }
    return;
}

#pragma mark - UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    UIImagePickerController *imgPicker = [[UIImagePickerController alloc] init];
    imgPicker.delegate = self;
    if (buttonIndex == 0) {
        // 拍照
        imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else {
        // 相册选择
        imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    [self.view.window.rootViewController presentViewController:imgPicker animated:YES completion:nil];
    return;
}

#pragma mark - UIImagePickerController delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.hud.labelText = @"努力提交中..";
    [self.hud show:NO];
    [picker dismissViewControllerAnimated:YES completion:^() {
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        CGSize imgSize = portraitImg.size;
        CGSize tagSize = imgSize;
        CGFloat rate = imgSize.width / imgSize.height;
        if (imgSize.width > 1000) {
            tagSize = CGSizeMake(1000, 1000 / rate);
        } else if (imgSize.height > 1000) {
            tagSize = CGSizeMake(1000 * rate, 1000);
        }
        // 去除图片旋转属性
        UIGraphicsBeginImageContext(tagSize);
        [portraitImg drawInRect:CGRectMake(0, 0, tagSize.width, tagSize.height)];
        portraitImg = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        // 上传请求
        NSString *url = [NcWebSerVice uploadImageUrl];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [manager POST:url parameters:@{} constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            // 图片压缩
            NSData* imageData = UIImageJPEGRepresentation(portraitImg, 0.5);
            NSDate *nowDate = [NSDate date];
            NSString *fileName = [NSString stringWithFormat:@"iOS-%ld.jpg", (long)[nowDate timeIntervalSince1970]];
            // 图片文件流
            [formData appendPartWithFileData:imageData name:fileName fileName:fileName mimeType:@"image/jpeg"];
        } progress:^(NSProgress *progress) {
            
        } success:^(NSURLSessionDataTask *task, id responseObject) {
            NSData *doubi = responseObject;
            NSString *shabi =  [[NSString alloc] initWithData:doubi encoding:NSUTF8StringEncoding];
            NSError *error;
            NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:[shabi dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
            //            NSDictionary *resultDic = (NSDictionary *)responseObject;
            NSString *successful = resultDic[@"successful"];
            
            // 加载失败
            if (!successful.boolValue) {
                self.hud.labelText = @"提交失败，请稍后重试";
                [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
                return;
            }
            
            NSString *value = resultDic[@"data"];
            [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"onImageResult('%@')", value]];
            self.hud.labelText = @"上传成功";
            [self.hud autoHideWithType:Zb_Indicator_type_Done delay:1.0 complete:^(void) {
            }];
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            self.hud.labelText = @"提交失败，请稍后重试";
            [self.hud autoHideWithType:Zb_Indicator_type_Warning delay:1.0 complete:nil];
        }];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    return;
}

#pragma mark - 导航栏delegate
- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    // push动画结束
    self.isPushAnimateEnd = YES;
    // 如果push的动画还没完，http请求就结束了，那么等push动画完后再隐藏导航栏
    if (!self.isAnyRequestFailed && _progressView.progress >= 1.0) {
        //        NSLog(@"push动画结束了，再关闭导航栏");
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
            [self.navigationController setNavigationBarHidden:YES animated:NO];
        });
    }
    self.navigationController.delegate = nil;
}

#pragma mark - js交互
- (void)h5_login:(NSDictionary *)infoDict {
    NSMutableDictionary *userDict = [NSMutableDictionary dictionaryWithDictionary:infoDict];
    [NcWebSerVice setUser:userDict];
    
    //    // 保存用户名
    //    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    //    [userDefaults setValue:self.userCode.text forKey:SAVE_KEY_ACCOUNT];
    //    [userDefaults synchronize];
    
    // 友盟推送设置别名，进行单个推送
    [UMessage setAlias:userDict[@"USER_ID"] type:kUMessageAliasTypeSina response:^(id responseObject, NSError *error) {
        //                DLog(@"%@ - %@", responseObject, error);
    }];
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate loginSuccess];
    return;
}

- (void)h5_getImage {
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照", @"从相册选择", nil];
    [action showInView:self.navigationController.view];
    return;
}

// 跳转新页面，返回执行js
- (void)h5_open:(NSString *)targetUlr WebView:(NSString *)jsStr BackJs:(NSInteger)closeCnt {
    __weak typeof(self) weakSelf = self;
    if (closeCnt <= 0) {
        // 直接打开新界面模式
        HsWebViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
        controller.url = targetUlr;
        if (jsStr.length > 0) {
            controller.backBlock = ^(void) {
                [weakSelf.webView stringByEvaluatingJavaScriptFromString:jsStr];
            };
        }
        [self.navigationController pushViewController:controller animated:YES];
    } else {
        // 先删除旧界面，再打开新界面模式
        
        NSMutableArray *vcArray = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
        UIViewController *firstDelVc = nil;
        // 删除层级大于拥有层级
        if (closeCnt >= vcArray.count) {
            // 直接删除至根界面
            if (vcArray.count > 1) {
                firstDelVc = vcArray[1];
            }
            vcArray = [NSMutableArray arrayWithObjects:vcArray[0], nil];
        } else {
            firstDelVc = vcArray[vcArray.count - closeCnt];
            vcArray = [NSMutableArray arrayWithArray:[vcArray subarrayWithRange:NSMakeRange(0, vcArray.count - closeCnt)]];
        }
        
        // 添加新界面
        HsWebViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
        controller.url = targetUlr;
        if (firstDelVc) {
            // 将要删除的第一个界面的返回执行函数，让新界面返回时执行
            if ([firstDelVc isKindOfClass:[HsWebViewController class]]) {
                HsWebViewController *firstDelWebVc = (HsWebViewController *)firstDelVc;
                controller.backBlock = firstDelWebVc.backBlock;
            }
        }
        [vcArray addObject:controller];
        [self.navigationController setViewControllers:vcArray animated:YES];
    }
    return;
}

// 打开新页面
- (void)h5_open:(NSString *)targetUlr WebView:(NSString *)isRefreshOnBack {
    __weak typeof(self) weakSelf = self;
    // 新页面
    HsWebViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    controller.url = targetUlr;
    if (isRefreshOnBack) {
        controller.backBlock = ^(void) {
            [weakSelf loadUrl];
            
            weakSelf.navigationController.delegate = self;
            if ([weakSelf isNavRoot]) {
                // 一级界面，都不显示导航栏
                [weakSelf.navigationController setNavigationBarHidden:YES animated:NO];
                return;
            }
            // 二级以上界面先显示导航栏
            [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
            [weakSelf.navigationController setNavigationBarHidden:NO animated:NO];
        };
    }
    [self.navigationController pushViewController:controller animated:YES];
}

@end
