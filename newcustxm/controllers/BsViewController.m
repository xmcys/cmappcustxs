//
//  BsController.m
//  newcustxm
//
//  Created by chyo on 16/11/8.
//  Copyright © 2016年 chyo. All rights reserved.
//

#import "BsViewController.h"

@interface BsViewController()

@end

@implementation BsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = UIColorFromHexString(@"#F5F8F8");
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // 友盟统计
    //    [MobClick beginLogPageView:[self getUmentClassName]];
    
    if (!self.isInit) {
        self.isInit = YES;
        [self viewWillAppearOnce];
    }
}

// 初始化UI时机，只执行一次
- (void)viewWillAppearOnce {
    
    // 初始化统一的文字提示器
    self.hud = [[MBProgressHUD alloc] initWithView:self.view];
    self.hud.mode = MBProgressHUDModeIndeterminate;
    self.hud.removeFromSuperViewOnHide = NO;
    [self.view addSubview:self.hud];
    return;
}

#pragma mark - 点击事件
// 返回点击
- (IBAction)leftBarItemOnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    return;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
