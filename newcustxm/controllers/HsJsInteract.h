//
//  HsJsInteract.h
//  newconsumerxm
//
//  Created by 张斌 on 17/2/10.
//  Copyright © 2017年 chyo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HsJsProtocol.h"

#pragma mark - HsJsInteract delegate
@protocol HsJsInteractDelegate <NSObject>

@optional

// h5拍照
- (void)h5_getImage;
// 跳转新页面，返回执行js
- (void)h5_open:(NSString *)targetUlr WebView:(NSString *)jsStr BackJs:(NSInteger)closeCnt;
// 跳转新页面
- (void)h5_open:(NSString *)targetUlr WebView:(NSString *)isRefreshOnBack;
// 注册页面自动登录成功
- (void)h5_login:(NSDictionary *)infoDict;

@end

@class HsWebViewController;

#pragma mark - 与js交互的处理类
@interface HsJsInteract : NSObject <HsJsProtocol>

@property (nonatomic, weak) HsWebViewController *controller;
@property (nonatomic, weak) id<HsJsInteractDelegate> delegate;

@end
