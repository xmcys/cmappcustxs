//
//  HsWebViewController.h
//  CmAppCustXs
//
//  Created by chyo on 15/9/24.
//  Copyright (c) 2015年 chyo. All rights reserved.
//

#import "BsViewController.h"
#import "HsJsInteract.h"

@interface HsWebViewController : BsViewController <UIWebViewDelegate, HsJsInteractDelegate>

// 网页控件
@property (nonatomic, strong) IBOutlet UIWebView *webView;

// 目标url
@property (nonatomic, strong) NSString *url;

// 注入js的对象
@property (nonatomic, strong) HsJsInteract *curJsInteract;

// 后退block
@property (nonatomic, copy) void (^backBlock)();

// 是否显示导航栏
@property (nonatomic, assign) BOOL isShowNav;

// webview的top约束
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcWebViewTop;
// 色块view
@property (nonatomic, strong) IBOutlet UIView *viewColor;

// 加载数据
- (void)loadUrl;

@end
