//
//  ConsScannerController.h
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-8.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//



@interface CTScannerController : UIViewController

// 扫码结果block
@property (nonatomic, copy) void (^scanResultBlock)(NSString *barCode);

@end
