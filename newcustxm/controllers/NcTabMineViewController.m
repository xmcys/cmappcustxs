//
//  NcTabMineViewController.m
//  newcustxm
//
//  Created by chyo on 16/11/8.
//  Copyright © 2016年 chyo. All rights reserved.
//

#import "NcTabMineViewController.h"
#import "HsWebViewController.h"
#import "UIButton+Badge.h"

#define TOP_BACKIMAGE_HEIGHT  0.38

@interface NcTabMineViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UIImageView *ivHead;
@property (nonatomic, strong) IBOutlet UITableView *tv;
@property (nonatomic, strong) IBOutlet UIView *tvHeader;
@property (nonatomic, strong) IBOutlet UILabel *labName;
@property (nonatomic, strong) IBOutlet UIButton *btnPoint;
// 红点
@property (nonatomic, strong) IBOutlet UIButton *btnBadgeWaitPay;
@property (nonatomic, strong) IBOutlet UIButton *btnBadgeWaitDelivery;
@property (nonatomic, strong) IBOutlet UIButton *btnBadgeWaitComment;
@property (nonatomic, strong) IBOutlet UIButton *btnBadgeWaitTuikuan;
@property (nonatomic, strong) IBOutlet UIButton *btnBadgeWaitRecv;

// 设置按钮
@property (nonatomic, strong) IBOutlet UIBarButtonItem *btnSetting;

@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation NcTabMineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.array = [NSMutableArray new];
    
    self.view.backgroundColor = UIColorFromHexString(@"ebebec");
    
    // 用户头像
    self.ivHead.contentMode = UIViewContentModeScaleAspectFill;
    self.ivHead.layer.cornerRadius = self.ivHead.frame.size.width / 2;
    self.ivHead.layer.masksToBounds = YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppearOnce {
    [super viewWillAppearOnce];
    
    self.btnPoint.hidden = YES;
    // 加载首页菜单
    [self loadAppEasyMenuList];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.navigationItem.title = @"我的";
    self.tabBarController.navigationItem.rightBarButtonItem = self.btnSetting;
    
//    CGFloat height = DeviceWidth * TOP_BACKIMAGE_HEIGHT;
//    CGRect tvHeaderFrame = self.tvHeader.frame;
//    tvHeaderFrame.size.height = height;
//    self.tvHeader.frame = tvHeaderFrame;
//    [self.tv setTableHeaderView:self.tvHeader];
    
    self.labName.text = [NcWebSerVice user][@"USER_NAME"];
    // 查询我的积分
    [self loadMyPointInfo];
    [self loadOrderStatusNumInfo];
    // 加载用户头像信息
    [self loadUserInfo];
    
    // 恢复导航栏显示
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

#pragma mark - 接口调用
// 查询我的积分
- (void)loadMyPointInfo {
    NSDictionary *parameters = @{@"userId":[NcWebSerVice user][@"USER_ID"]};
    [HttpRequest postWithURLString:[NcWebSerVice queryIntegralByUserIdToAppUrl] parameters:parameters success:^(id responseObject) {
        NSString *result = responseObject[@"successful"];
        if (result.boolValue) {
            [self.btnPoint setTitle:[NSString stringWithFormat:@"%@积分", responseObject[@"data"]] forState:UIControlStateNormal];
        }
    } failure:^(NSError *error) {
        
    }];
    return;
}

- (void)loadUserInfo {
    NSString *userId = [NcWebSerVice user][@"USER_ID"];
    [HttpRequest postWithURLString:[NcWebSerVice getUserInfo] parameters:@{@"userId":userId} success:^(id responseObject) {
        NSString *result = responseObject[@"statusCode"];
        if ([result isEqualToString:@"200"]) {
            NSString *HEAD_PICTURE = responseObject[@"data"][@"HEAD_PICTURE"];
            if (!HEAD_PICTURE || (![HEAD_PICTURE isKindOfClass:[NSString class]]) || HEAD_PICTURE.length <= 0) {
                self.ivHead.image = [UIImage imageNamed:@"mine_head"];
            } else {
                NSString *urlString = [NcWebSerVice getImgFullUrl:HEAD_PICTURE];
                [self.ivHead sd_setImageWithURL:[NSURL URLWithString:urlString]];
            }
        } else {
            self.ivHead.image = [UIImage imageNamed:@"mine_head"];
        }
        
    } failure:^(NSError *error) {
        return;
    }];
}

// 查询订单状态数量
- (void)loadOrderStatusNumInfo {
    [HttpRequest getWithURLString:[NcWebSerVice getStautsNumUrl] parameters:nil
                          success:^(id responseObject) {
                              
                              NSString *result = responseObject[@"successful"];
                              if (result.boolValue) {
                                  NSDictionary *data = responseObject[@"data"];
                                  if (data && ![data isKindOfClass:[NSNull class]]) {
                                      NSNumber *num1 = data[@"num1"];
                                      NSNumber *num2 = data[@"num2"];
                                      NSNumber *num3 = data[@"num3"];
                                      NSNumber *num4 = data[@"num4"];
                                      NSNumber *num5 = data[@"num5"];
                                      if (num1 && ![num1 isKindOfClass:[NSNull class]]) {
                                          NSString *num1Str = [NSString stringWithFormat:@"%@", num1];
                                          num1Str = [num1Str isEqualToString:@"0"] ? @"":num1Str;
                                          self.btnBadgeWaitPay.badgeValue = num1Str;
                                      }
                                      if (num2 && ![num2 isKindOfClass:[NSNull class]]) {
                                          NSString *num2Str = [NSString stringWithFormat:@"%@", num2];
                                          num2Str = [num2Str isEqualToString:@"0"] ? @"":num2Str;
                                          self.btnBadgeWaitRecv.badgeValue = num2Str;
                                      }
                                      if (num3 && ![num3 isKindOfClass:[NSNull class]]) {
                                          NSString *num3Str = [NSString stringWithFormat:@"%@", num3];
                                          num3Str = [num3Str isEqualToString:@"0"] ? @"":num3Str;
                                          self.btnBadgeWaitComment.badgeValue = num3Str;
                                      }
                                      if (num4 && ![num4 isKindOfClass:[NSNull class]]) {
                                          NSString *num4Str = [NSString stringWithFormat:@"%@", num4];
                                          num4Str = [num4Str isEqualToString:@"0"] ? @"":num4Str;
                                          self.btnBadgeWaitTuikuan.badgeValue = num4Str;
                                      }
                                      if (num5 && ![num5 isKindOfClass:[NSNull class]]) {
                                          NSString *num5Str = [NSString stringWithFormat:@"%@", num5];
                                          num5Str = [num5Str isEqualToString:@"0"] ? @"":num5Str;
                                          self.btnBadgeWaitDelivery.badgeValue = num5Str;
                                      }
                                  }
                              }
                          } failure:^(NSError *error) {
                          }];
    return;
}

// 加载首页菜单
- (void)loadAppEasyMenuList {
    NSDictionary *parameters = @{};
    NSString *url = [NcWebSerVice getAppMenuNavListUrl:@"NAV_MY"];
    [HttpRequest getWithURLString:url parameters:parameters success:^(id responseObject) {
        NSString *result = responseObject[@"status"];
        if ([result isEqualToString:@"success"]) {
            NSArray *tagArray = responseObject[@"data"];
            // 无数据
            if (tagArray == nil || ![tagArray isKindOfClass:[NSArray class]]) {
                return;
            }
            // 菜单数据
            [self.array removeAllObjects];
            [self.array addObjectsFromArray:tagArray];
            [self.tv reloadData];
        }
    } failure:^(NSError *error) {
    }];
    return;
}

#pragma mark - 点击事件
- (void)itemOnClick:(NcControl *)control {
    NSString *TARGET_URL = control.userInfo[@"TARGET_URL"];
    HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    web.url = TARGET_URL;
    [self.navigationController pushViewController:web animated:YES];
    return;
}

// 设置点击
- (IBAction)btnSettingOnClick:(id)sender {
    HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    web.url = [NcWebSerVice retailerSettingsUrl];
    [self.navigationController pushViewController:web animated:YES];
    return;
}

#pragma mark - 点击事件
// 待付款点击
- (IBAction)btnWaitPayOnClick:(id)sender {
    HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    web.url = [NcWebSerVice getNotPaidOrderUrl];
    [self.navigationController pushViewController:web animated:YES];
    return;
}

// 待送货点击
- (IBAction)btnWaitDeliveryOnClick:(id)sender {
    HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    web.url = [NcWebSerVice getDfhPaidOrder];
    [self.navigationController pushViewController:web animated:YES];
    return;
}

// 待收货点击
- (IBAction)btnWaitRectOnClick:(id)sender {
    HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    web.url = [NcWebSerVice getNotReceivingOrderUrl];
    [self.navigationController pushViewController:web animated:YES];
    return;
}

// 待评价点击
- (IBAction)btnWaitCommentOnClick:(id)sender {
    HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    web.url = [NcWebSerVice getNotAppraiseOrderUrl];
    [self.navigationController pushViewController:web animated:YES];
    return;
}

// 待退款点击
- (IBAction)btnWaitTuikuanOnClick:(id)sender {
    HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    web.url = [NcWebSerVice getMerchantNotRefundOrderUrl];
    [self.navigationController pushViewController:web animated:YES];
    return;
}

// 全部订单点击
- (IBAction)btnAllOnClick:(id)sender {
    HsWebViewController *web = [self.storyboard instantiateViewControllerWithIdentifier:@"HsWebViewController"];
    web.url = [NcWebSerVice getAllOrderUrl];
    [self.navigationController pushViewController:web animated:YES];
    return;
}

#pragma mark - UITableView delegate && DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSArray *rowArray = self.array;
    if (rowArray.count % 2 == 0) {
        return rowArray.count / 2;
    } else {
        return rowArray.count / 2 + 1;
    };
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 75;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"CELL";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.backgroundColor = [UIColor clearColor];
    CGFloat width = DeviceWidth / 2;
    
    NSArray *rowArray = self.array;
    
    for (int i = 0; i < 2; i ++) {
        if (indexPath.section * 2 + i >= rowArray.count) {
            break;
        }
        
        NSDictionary * item = [rowArray objectAtIndex:(indexPath.section * 2 + i)];
        
        NcControl *itemControl = [[NcControl alloc] initWithFrame:CGRectMake(width * i, 0, width, 75)];
        itemControl.backgroundColor = [UIColor whiteColor];
        itemControl.layer.borderWidth = 0.5;
        itemControl.layer.borderColor = [UIColorFromHexString(@"E8E8E8") CGColor];
        itemControl.userInfo = item;
        [itemControl addTarget:self action:@selector(itemOnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        UIImageView *iconImg = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 25, 25)];
        NSString *imageUrl = [NcWebSerVice getImgFullUrl:item[@"ITEM_ICON"]];
        [iconImg sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
        [itemControl addSubview:iconImg];
        
        UILabel *itemMenuLab = [[UILabel alloc] initWithFrame:CGRectMake(23 + CGRectGetWidth(iconImg.frame), 12, CGRectGetWidth(itemControl.frame) - 20 - CGRectGetWidth(iconImg.frame), 21)];
        itemMenuLab.textAlignment = NSTextAlignmentLeft;
        [itemMenuLab setFont:[UIFont systemFontOfSize:16.0f]];
        [itemControl addSubview:itemMenuLab];
        itemMenuLab.text = [NSString stringWithFormat:@"%@",item[@"ITEM_NAME"]];
        [cell addSubview:itemControl];
        
        
        UILabel *itemSubLab = [[UILabel alloc] initWithFrame:CGRectMake(23 + CGRectGetWidth(iconImg.frame), 20 + CGRectGetHeight(itemMenuLab.frame), CGRectGetWidth(itemControl.frame) - 20 - CGRectGetWidth(iconImg.frame), 21)];
        itemSubLab.textAlignment = NSTextAlignmentLeft;
        itemSubLab.textColor = UIColorFromHexString(@"787878");
        [itemSubLab setFont:[UIFont systemFontOfSize:13.0f]];
        [itemControl addSubview:itemSubLab];
        itemSubLab.text = [NSString stringWithFormat:@"%@", item[@"ITEM_DESC"]];
        [cell addSubview:itemControl];
    }
    return cell;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
